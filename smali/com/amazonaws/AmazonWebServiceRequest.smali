.class public abstract Lcom/amazonaws/AmazonWebServiceRequest;
.super Ljava/lang/Object;
.source "AmazonWebServiceRequest.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private cloneSource:Lcom/amazonaws/AmazonWebServiceRequest;

.field private credentials:Lcom/amazonaws/auth/AWSCredentials;

.field private generalProgressListener:Lcom/amazonaws/event/ProgressListener;

.field private final requestClientOptions:Lcom/amazonaws/RequestClientOptions;

.field private requestMetricCollector:Lcom/amazonaws/metrics/RequestMetricCollector;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/amazonaws/RequestClientOptions;

    invoke-direct {v0}, Lcom/amazonaws/RequestClientOptions;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/AmazonWebServiceRequest;->requestClientOptions:Lcom/amazonaws/RequestClientOptions;

    return-void
.end method

.method private setCloneSource(Lcom/amazonaws/AmazonWebServiceRequest;)V
    .locals 0
    .param p1, "cloneSource"    # Lcom/amazonaws/AmazonWebServiceRequest;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/amazonaws/AmazonWebServiceRequest;->cloneSource:Lcom/amazonaws/AmazonWebServiceRequest;

    .line 198
    return-void
.end method


# virtual methods
.method public clone()Lcom/amazonaws/AmazonWebServiceRequest;
    .locals 4

    .prologue
    .line 209
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amazonaws/AmazonWebServiceRequest;

    .line 210
    .local v0, "cloned":Lcom/amazonaws/AmazonWebServiceRequest;
    invoke-direct {v0, p0}, Lcom/amazonaws/AmazonWebServiceRequest;->setCloneSource(Lcom/amazonaws/AmazonWebServiceRequest;)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    return-object v0

    .line 212
    .end local v0    # "cloned":Lcom/amazonaws/AmazonWebServiceRequest;
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Got a CloneNotSupportedException from Object.clone() even though we\'re Cloneable!"

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/amazonaws/AmazonWebServiceRequest;->clone()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v0

    return-object v0
.end method

.method protected final copyBaseTo(Lcom/amazonaws/AmazonWebServiceRequest;)Lcom/amazonaws/AmazonWebServiceRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/amazonaws/AmazonWebServiceRequest;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "target":Lcom/amazonaws/AmazonWebServiceRequest;, "TT;"
    iget-object v0, p0, Lcom/amazonaws/AmazonWebServiceRequest;->generalProgressListener:Lcom/amazonaws/event/ProgressListener;

    invoke-virtual {p1, v0}, Lcom/amazonaws/AmazonWebServiceRequest;->setGeneralProgressListener(Lcom/amazonaws/event/ProgressListener;)V

    .line 164
    iget-object v0, p0, Lcom/amazonaws/AmazonWebServiceRequest;->requestMetricCollector:Lcom/amazonaws/metrics/RequestMetricCollector;

    invoke-virtual {p1, v0}, Lcom/amazonaws/AmazonWebServiceRequest;->setRequestMetricCollector(Lcom/amazonaws/metrics/RequestMetricCollector;)V

    .line 165
    return-object p1
.end method

.method public getCloneRoot()Lcom/amazonaws/AmazonWebServiceRequest;
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/amazonaws/AmazonWebServiceRequest;->cloneSource:Lcom/amazonaws/AmazonWebServiceRequest;

    .line 188
    .local v0, "cloneRoot":Lcom/amazonaws/AmazonWebServiceRequest;
    if-eqz v0, :cond_0

    .line 189
    :goto_0
    invoke-virtual {v0}, Lcom/amazonaws/AmazonWebServiceRequest;->getCloneSource()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 190
    invoke-virtual {v0}, Lcom/amazonaws/AmazonWebServiceRequest;->getCloneSource()Lcom/amazonaws/AmazonWebServiceRequest;

    move-result-object v0

    goto :goto_0

    .line 193
    :cond_0
    return-object v0
.end method

.method public getCloneSource()Lcom/amazonaws/AmazonWebServiceRequest;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/amazonaws/AmazonWebServiceRequest;->cloneSource:Lcom/amazonaws/AmazonWebServiceRequest;

    return-object v0
.end method

.method public getGeneralProgressListener()Lcom/amazonaws/event/ProgressListener;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/amazonaws/AmazonWebServiceRequest;->generalProgressListener:Lcom/amazonaws/event/ProgressListener;

    return-object v0
.end method

.method public getRequestClientOptions()Lcom/amazonaws/RequestClientOptions;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/amazonaws/AmazonWebServiceRequest;->requestClientOptions:Lcom/amazonaws/RequestClientOptions;

    return-object v0
.end method

.method public getRequestCredentials()Lcom/amazonaws/auth/AWSCredentials;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/amazonaws/AmazonWebServiceRequest;->credentials:Lcom/amazonaws/auth/AWSCredentials;

    return-object v0
.end method

.method public getRequestMetricCollector()Lcom/amazonaws/metrics/RequestMetricCollector;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/amazonaws/AmazonWebServiceRequest;->requestMetricCollector:Lcom/amazonaws/metrics/RequestMetricCollector;

    return-object v0
.end method

.method public setGeneralProgressListener(Lcom/amazonaws/event/ProgressListener;)V
    .locals 0
    .param p1, "generalProgressListener"    # Lcom/amazonaws/event/ProgressListener;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/amazonaws/AmazonWebServiceRequest;->generalProgressListener:Lcom/amazonaws/event/ProgressListener;

    .line 128
    return-void
.end method

.method public setRequestCredentials(Lcom/amazonaws/auth/AWSCredentials;)V
    .locals 0
    .param p1, "credentials"    # Lcom/amazonaws/auth/AWSCredentials;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/amazonaws/AmazonWebServiceRequest;->credentials:Lcom/amazonaws/auth/AWSCredentials;

    .line 63
    return-void
.end method

.method public setRequestMetricCollector(Lcom/amazonaws/metrics/RequestMetricCollector;)V
    .locals 0
    .param p1, "requestMetricCollector"    # Lcom/amazonaws/metrics/RequestMetricCollector;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 102
    iput-object p1, p0, Lcom/amazonaws/AmazonWebServiceRequest;->requestMetricCollector:Lcom/amazonaws/metrics/RequestMetricCollector;

    .line 103
    return-void
.end method

.method public withGeneralProgressListener(Lcom/amazonaws/event/ProgressListener;)Lcom/amazonaws/AmazonWebServiceRequest;
    .locals 1
    .param p1, "generalProgressListener"    # Lcom/amazonaws/event/ProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/amazonaws/AmazonWebServiceRequest;",
            ">(",
            "Lcom/amazonaws/event/ProgressListener;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lcom/amazonaws/AmazonWebServiceRequest;->setGeneralProgressListener(Lcom/amazonaws/event/ProgressListener;)V

    .line 152
    move-object v0, p0

    .line 153
    .local v0, "t":Lcom/amazonaws/AmazonWebServiceRequest;, "TT;"
    return-object v0
.end method

.method public withRequestMetricCollector(Lcom/amazonaws/metrics/RequestMetricCollector;)Lcom/amazonaws/AmazonWebServiceRequest;
    .locals 1
    .param p1, "metricCollector"    # Lcom/amazonaws/metrics/RequestMetricCollector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/amazonaws/AmazonWebServiceRequest;",
            ">(",
            "Lcom/amazonaws/metrics/RequestMetricCollector;",
            ")TT;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lcom/amazonaws/AmazonWebServiceRequest;->setRequestMetricCollector(Lcom/amazonaws/metrics/RequestMetricCollector;)V

    .line 116
    move-object v0, p0

    .line 117
    .local v0, "t":Lcom/amazonaws/AmazonWebServiceRequest;, "TT;"
    return-object v0
.end method
