.class public final Lcom/navdy/service/library/events/settings/DateTimeConfiguration;
.super Lcom/squareup/wire/Message;
.source "DateTimeConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;,
        Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_FORMAT:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

.field public static final DEFAULT_TIMESTAMP:Ljava/lang/Long;

.field public static final DEFAULT_TIMEZONE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final timezone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->DEFAULT_TIMESTAMP:Ljava/lang/Long;

    .line 19
    sget-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_24_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    sput-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->DEFAULT_FORMAT:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;

    .prologue
    .line 47
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->timestamp:Ljava/lang/Long;

    iget-object v1, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->timezone:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;-><init>(Ljava/lang/Long;Ljava/lang/String;Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V

    .line 48
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 49
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;Lcom/navdy/service/library/events/settings/DateTimeConfiguration$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;-><init>(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/Long;
    .param p2, "timezone"    # Ljava/lang/String;
    .param p3, "format"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    .line 42
    iput-object p2, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 44
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    if-ne p1, p0, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 55
    check-cast v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    .line 56
    .local v0, "o":Lcom/navdy/service/library/events/settings/DateTimeConfiguration;
    iget-object v3, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    .line 57
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 58
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    iget v0, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->hashCode:I

    .line 64
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 65
    iget-object v2, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 66
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 67
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 68
    iput v0, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->hashCode:I

    .line 70
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 65
    goto :goto_0

    :cond_3
    move v2, v1

    .line 66
    goto :goto_1
.end method
