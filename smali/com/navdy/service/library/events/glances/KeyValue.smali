.class public final Lcom/navdy/service/library/events/glances/KeyValue;
.super Lcom/squareup/wire/Message;
.source "KeyValue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/glances/KeyValue$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/navdy/service/library/events/glances/KeyValue$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/glances/KeyValue$Builder;

    .prologue
    .line 31
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/KeyValue$Builder;->key:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/glances/KeyValue$Builder;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/glances/KeyValue;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 33
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/glances/KeyValue$Builder;Lcom/navdy/service/library/events/glances/KeyValue$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/glances/KeyValue$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/glances/KeyValue$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Lcom/navdy/service/library/events/glances/KeyValue$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    if-ne p1, p0, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v1

    .line 38
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/glances/KeyValue;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 39
    check-cast v0, Lcom/navdy/service/library/events/glances/KeyValue;

    .line 40
    .local v0, "o":Lcom/navdy/service/library/events/glances/KeyValue;
    iget-object v3, p0, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/KeyValue;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    .line 41
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/KeyValue;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 46
    iget v0, p0, Lcom/navdy/service/library/events/glances/KeyValue;->hashCode:I

    .line 47
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 48
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 49
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 50
    iput v0, p0, Lcom/navdy/service/library/events/glances/KeyValue;->hashCode:I

    .line 52
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 48
    goto :goto_0
.end method
