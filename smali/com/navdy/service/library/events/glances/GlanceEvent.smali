.class public final Lcom/navdy/service/library/events/glances/GlanceEvent;
.super Lcom/squareup/wire/Message;
.source "GlanceEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;,
        Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;,
        Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GLANCEDATA:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GLANCETYPE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_POSTTIME:Ljava/lang/Long;

.field public static final DEFAULT_PROVIDER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final actions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        enumType = Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;",
            ">;"
        }
    .end annotation
.end field

.field public final glanceData:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/glances/KeyValue;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;"
        }
    .end annotation
.end field

.field public final glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final postTime:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final provider:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->DEFAULT_GLANCETYPE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 25
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->DEFAULT_POSTTIME:Ljava/lang/Long;

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->DEFAULT_GLANCEDATA:Ljava/util/List;

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->DEFAULT_ACTIONS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;)V
    .locals 7
    .param p1, "builder"    # Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    .prologue
    .line 80
    iget-object v1, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    iget-object v2, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime:Ljava/lang/Long;

    iget-object v5, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData:Ljava/util/List;

    iget-object v6, p1, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions:Ljava/util/List;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/glances/GlanceEvent;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;)V

    .line 81
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/glances/GlanceEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 82
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;Lcom/navdy/service/library/events/glances/GlanceEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/glances/GlanceEvent$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/glances/GlanceEvent;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "glanceType"    # Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;
    .param p2, "provider"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "postTime"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p5, "glanceData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .local p6, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 72
    iput-object p2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    .line 74
    iput-object p4, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    .line 75
    invoke-static {p5}, Lcom/navdy/service/library/events/glances/GlanceEvent;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    .line 76
    invoke-static {p6}, Lcom/navdy/service/library/events/glances/GlanceEvent;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    .line 77
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 19
    invoke-static {p0}, Lcom/navdy/service/library/events/glances/GlanceEvent;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 19
    invoke-static {p0}, Lcom/navdy/service/library/events/glances/GlanceEvent;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    if-ne p1, p0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v1

    .line 87
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/glances/GlanceEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 88
    check-cast v0, Lcom/navdy/service/library/events/glances/GlanceEvent;

    .line 89
    .local v0, "o":Lcom/navdy/service/library/events/glances/GlanceEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    .line 90
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    .line 91
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    .line 92
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    .line 93
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    .line 94
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 99
    iget v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->hashCode:I

    .line 100
    .local v0, "result":I
    if-nez v0, :cond_2

    .line 101
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->hashCode()I

    move-result v0

    .line 102
    :goto_0
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 103
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v4, v2

    .line 104
    mul-int/lit8 v2, v0, 0x25

    iget-object v4, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    if-eqz v4, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 105
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_3
    add-int v0, v2, v1

    .line 106
    mul-int/lit8 v1, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v3

    :cond_1
    add-int v0, v1, v3

    .line 107
    iput v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->hashCode:I

    .line 109
    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 101
    goto :goto_0

    :cond_4
    move v2, v1

    .line 102
    goto :goto_1

    :cond_5
    move v2, v1

    .line 103
    goto :goto_2

    :cond_6
    move v1, v3

    .line 105
    goto :goto_3
.end method
