.class public Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;
.super Lcom/navdy/client/app/ui/base/BaseEditActivity;
.source "AudioSettingsActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;
    }
.end annotation


# static fields
.field public static final DIALOG_VOICE_SELECTION:I = 0x1

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field audioOutput:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

.field audioTestIndex:I

.field audioTests:[Ljava/lang/String;

.field mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mBluetooth:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100374
    .end annotation
.end field

.field mCameraWarnings:Landroid/widget/Switch;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100382
    .end annotation
.end field

.field mCameraWarningsIsOn:Z

.field mInitialVoice:Landroid/speech/tts/Voice;

.field mMainDescription:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100370
    .end annotation
.end field

.field mSelectedVoice:Ljava/lang/String;

.field mSharedPrefs:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mSmartBluetooth:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100373
    .end annotation
.end field

.field mSpeaker:Landroid/widget/RadioButton;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100375
    .end annotation
.end field

.field mSpeechDelayPreference:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10037b
    .end annotation
.end field

.field mSpeedWarningsIsOn:Z

.field mSppedLimitWarnings:Landroid/widget/Switch;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100381
    .end annotation
.end field

.field mTbtInstructionsIsOn:Z

.field mTtsLevel:F

.field mTurnByTurnNavigation:Landroid/widget/Switch;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10037f
    .end annotation
.end field

.field mTxtVoiceName:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10037a
    .end annotation
.end field

.field mVoicePreference:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100378
    .end annotation
.end field

.field mWelcomeMessage:Z

.field mWelcomeMessageSwitch:Landroid/widget/Switch;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100380
    .end annotation
.end field

.field speechLevelSettings:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100377
    .end annotation
.end field

.field tvSpeechLevel:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10037d
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;-><init>()V

    .line 121
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTestIndex:I

    .line 123
    sget-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->BEST_AVAILABLE:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioOutput:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->playTestAudio()V

    return-void
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method private static getDisplayName(Ljava/lang/Object;)Ljava/lang/String;
    .locals 7
    .param p0, "voiceObj"    # Ljava/lang/Object;

    .prologue
    .line 522
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_2

    move-object v3, p0

    .line 523
    check-cast v3, Landroid/speech/tts/Voice;

    .line 524
    .local v3, "voice":Landroid/speech/tts/Voice;
    if-eqz v3, :cond_1

    .line 525
    invoke-virtual {v3}, Landroid/speech/tts/Voice;->getLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 526
    .local v0, "displayName":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/speech/tts/Voice;->getName()Ljava/lang/String;

    move-result-object v2

    .line 527
    .local v2, "uniqueName":Ljava/lang/String;
    const-string v4, "#"

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 528
    .local v1, "index":I
    if-lez v1, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    if-ge v1, v4, :cond_0

    .line 529
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 536
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v1    # "index":I
    .end local v2    # "uniqueName":Ljava/lang/String;
    .end local v3    # "voice":Landroid/speech/tts/Voice;
    :goto_0
    return-object v4

    .line 531
    .restart local v0    # "displayName":Ljava/lang/String;
    .restart local v1    # "index":I
    .restart local v2    # "uniqueName":Ljava/lang/String;
    .restart local v3    # "voice":Landroid/speech/tts/Voice;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 534
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v1    # "index":I
    .end local v2    # "uniqueName":Ljava/lang/String;
    :cond_1
    const-string v4, ""

    goto :goto_0

    .line 536
    .end local v3    # "voice":Landroid/speech/tts/Voice;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private playTestAudio()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 418
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isTTSAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTests:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTests:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTests:[Ljava/lang/String;

    iget v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTestIndex:I

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 420
    iget v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTestIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTestIndex:I

    .line 421
    iget v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTestIndex:I

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTests:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 422
    iput v3, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTestIndex:I

    .line 425
    :cond_0
    invoke-static {p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->startAudioStatusActivity(Landroid/content/Context;)V

    .line 426
    return-void
.end method

.method private sendPreferencesToDisplay()V
    .locals 15

    .prologue
    .line 310
    iget-object v10, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_route_calculation"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 311
    .local v2, "calculateShortestRouteIsOn":Z
    iget-object v10, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_auto_recalc"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 312
    .local v3, "autoRecalcIsOn":Z
    iget-object v10, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_highways"

    const/4 v12, 0x1

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 313
    .local v4, "highwaysIsOn":Z
    iget-object v10, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_toll_roads"

    const/4 v12, 0x1

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 314
    .local v5, "tollRoadsIsOn":Z
    iget-object v10, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_ferries"

    const/4 v12, 0x1

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 315
    .local v6, "ferriesIsOn":Z
    iget-object v10, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_tunnels"

    const/4 v12, 0x1

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 316
    .local v7, "tunnelsIsOn":Z
    iget-object v10, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_unpaved_roads"

    const/4 v12, 0x1

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 317
    .local v8, "unpavedRoadsIsOn":Z
    iget-object v10, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_auto_trains"

    const/4 v12, 0x1

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 320
    .local v9, "autoTrainsIsOn":Z
    const-string v10, "nav_serial_number"

    invoke-static {v10}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->incrementSerialNumber(Ljava/lang/String;)J

    move-result-wide v0

    .line 323
    .local v0, "navSerialNumber":J
    iget-object v10, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSppedLimitWarnings:Landroid/widget/Switch;

    .line 332
    invoke-virtual {v10}, Landroid/widget/Switch;->isChecked()Z

    move-result v10

    iget-object v11, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarnings:Landroid/widget/Switch;

    .line 333
    invoke-virtual {v11}, Landroid/widget/Switch;->isChecked()Z

    move-result v11

    iget-object v12, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTurnByTurnNavigation:Landroid/widget/Switch;

    .line 334
    invoke-virtual {v12}, Landroid/widget/Switch;->isChecked()Z

    move-result v12

    .line 323
    invoke-static/range {v0 .. v12}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->buildNavigationPreferences(JZZZZZZZZZZZ)Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v13

    .line 338
    .local v13, "navPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    invoke-static {v13}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendNavSettingsToTheHud(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)Z

    move-result v14

    .line 339
    .local v14, "success":Z
    if-eqz v14, :cond_0

    .line 340
    const-string v10, "Audio_Settings_Changed"

    invoke-static {v10}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 344
    :goto_0
    return-void

    .line 342
    :cond_0
    const v10, 0x7f080455

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v10, v11}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->showShortToast(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private setAudioOutputAndUpdateUI(Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;)V
    .locals 4
    .param p1, "audioOutput"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 429
    sget-object v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$3;->$SwitchMap$com$navdy$client$app$framework$util$TTSAudioRouter$AudioOutput:[I

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 446
    :goto_0
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioOutput:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    .line 447
    iput-boolean v3, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->somethingChanged:Z

    .line 448
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v0, p1, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setAudioOutput(Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;Z)V

    .line 449
    return-void

    .line 431
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSmartBluetooth:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 432
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mBluetooth:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 433
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeaker:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 436
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mBluetooth:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 437
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSmartBluetooth:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 438
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeaker:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 441
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeaker:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 442
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mBluetooth:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 443
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSmartBluetooth:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 17
    .param p1, "id"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 460
    packed-switch p1, :pswitch_data_0

    .line 509
    :cond_0
    invoke-super/range {p0 .. p2}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v2

    :goto_0
    return-object v2

    .line 462
    :pswitch_0
    sget v14, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v15, 0x15

    if-lt v14, v15, :cond_0

    .line 463
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v14}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getTextToSpeech()Landroid/speech/tts/TextToSpeech;

    move-result-object v14

    invoke-virtual {v14}, Landroid/speech/tts/TextToSpeech;->getVoices()Ljava/util/Set;

    move-result-object v12

    .line 464
    .local v12, "voices":Ljava/util/Set;, "Ljava/util/Set<Landroid/speech/tts/Voice;>;"
    if-eqz v12, :cond_0

    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v14

    if-lez v14, :cond_0

    .line 465
    invoke-interface {v12}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v13

    .line 466
    .local v13, "voicesArray":[Ljava/lang/Object;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 467
    .local v10, "voiceLabelsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;>;"
    array-length v15, v13

    const/4 v14, 0x0

    :goto_1
    if-ge v14, v15, :cond_1

    aget-object v8, v13, v14

    .local v8, "voice":Ljava/lang/Object;
    move-object v11, v8

    .line 468
    check-cast v11, Landroid/speech/tts/Voice;

    .line 469
    .local v11, "voiceObj":Landroid/speech/tts/Voice;
    new-instance v9, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;

    invoke-direct {v9}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;-><init>()V

    .line 470
    .local v9, "voiceLabel":Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;
    iput-object v11, v9, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;->voice:Landroid/speech/tts/Voice;

    .line 472
    invoke-static {v11}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->getDisplayName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v9, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;->label:Ljava/lang/String;

    .line 473
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 475
    .end local v8    # "voice":Ljava/lang/Object;
    .end local v9    # "voiceLabel":Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;
    .end local v11    # "voiceObj":Landroid/speech/tts/Voice;
    :cond_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v14

    new-array v7, v14, [Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;

    .line 476
    .local v7, "resultantArray":[Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 477
    array-length v14, v7

    new-array v6, v14, [Ljava/lang/CharSequence;

    .line 478
    .local v6, "labels":[Ljava/lang/CharSequence;
    const/4 v3, 0x0

    .line 479
    .local v3, "i":I
    array-length v15, v7

    const/4 v14, 0x0

    move v4, v3

    .end local v3    # "i":I
    .local v4, "i":I
    :goto_2
    if-ge v14, v15, :cond_2

    aget-object v5, v7, v14

    .line 480
    .local v5, "label":Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    iget-object v0, v5, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;->label:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v6, v4

    .line 479
    add-int/lit8 v14, v14, 0x1

    move v4, v3

    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto :goto_2

    .line 482
    .end local v5    # "label":Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;
    :cond_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 483
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v14, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v7}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;-><init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;[Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;)V

    invoke-virtual {v1, v6, v14}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 501
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 502
    .local v2, "dialog":Landroid/app/AlertDialog;
    const v14, 0x7f0803fa

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 460
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected discardChanges()V
    .locals 2

    .prologue
    .line 542
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->discardChanges()V

    .line 543
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 544
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getTextToSpeech()Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setVoice(Landroid/speech/tts/Voice;)I

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->resetToUserPreference()V

    .line 547
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 348
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 364
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->somethingChanged:Z

    .line 365
    return-void

    .line 350
    :pswitch_0
    iput-boolean p2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mWelcomeMessage:Z

    goto :goto_0

    .line 353
    :pswitch_1
    iput-boolean p2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeedWarningsIsOn:Z

    goto :goto_0

    .line 356
    :pswitch_2
    iput-boolean p2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarningsIsOn:Z

    goto :goto_0

    .line 359
    :pswitch_3
    iput-boolean p2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTbtInstructionsIsOn:Z

    goto :goto_0

    .line 348
    nop

    :pswitch_data_0
    .packed-switch 0x7f10037f
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100372,
            0x7f100374,
            0x7f100375,
            0x7f100371,
            0x7f100378,
            0x7f10037b
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 383
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 415
    :goto_0
    :pswitch_0
    return-void

    .line 385
    :pswitch_1
    sget-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->BEST_AVAILABLE:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->setAudioOutputAndUpdateUI(Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;)V

    .line 386
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeechDelayPreference:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 387
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->playTestAudio()V

    goto :goto_0

    .line 390
    :pswitch_2
    sget-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->BLUETOOTH_MEDIA:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->setAudioOutputAndUpdateUI(Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;)V

    .line 391
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeechDelayPreference:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 392
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->playTestAudio()V

    goto :goto_0

    .line 395
    :pswitch_3
    sget-object v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->PHONE_SPEAKER:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->setAudioOutputAndUpdateUI(Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;)V

    .line 396
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeechDelayPreference:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 397
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->playTestAudio()V

    goto :goto_0

    .line 400
    :pswitch_4
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->playTestAudio()V

    goto :goto_0

    .line 403
    :pswitch_5
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2, v2}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 406
    :pswitch_6
    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->isHFPSpeakerConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    invoke-static {p0}, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->start(Landroid/content/Context;)V

    goto :goto_0

    .line 409
    :cond_0
    invoke-static {p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->showHFPNotConnected(Landroid/content/Context;)V

    goto :goto_0

    .line 383
    nop

    :pswitch_data_0
    .packed-switch 0x7f100371
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 19
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 128
    invoke-super/range {p0 .. p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 129
    const v16, 0x7f0300ed

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->setContentView(I)V

    .line 130
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 132
    new-instance v16, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v17, 0x7f0802b1

    .line 133
    invoke-virtual/range {v16 .. v17}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v16

    .line 134
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 137
    invoke-static/range {p0 .. p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f09000a

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTests:[Ljava/lang/String;

    .line 141
    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    .line 142
    .local v7, "random":Ljava/util/Random;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTests:[Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioTestIndex:I

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_phone_volume"

    invoke-interface/range {v16 .. v17}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_phone_volume"

    const/16 v18, 0x4b

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 148
    .local v6, "phoneVolumeLevel":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    const-string v17, "audio_phone_volume"

    invoke-interface/range {v16 .. v17}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_tts_volume"

    invoke-interface/range {v16 .. v17}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_0

    .line 150
    const/16 v16, 0x32

    move/from16 v0, v16

    if-gt v6, v0, :cond_4

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    const-string v17, "audio_tts_volume"

    const v18, 0x3e99999a    # 0.3f

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 161
    .end local v6    # "phoneVolumeLevel":I
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_tts_volume"

    const v18, 0x3f19999a    # 0.6f

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTtsLevel:F

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_play_welcome"

    const/16 v18, 0x0

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mWelcomeMessage:Z

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_turn_by_turn_instructions"

    const/16 v18, 0x1

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTbtInstructionsIsOn:Z

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_speed_warnings"

    const/16 v18, 0x0

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeedWarningsIsOn:Z

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_camera_warnings"

    const/16 v18, 0x1

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarningsIsOn:Z

    .line 167
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_voice"

    const/16 v18, 0x0

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    const-string v17, "audio_output_preference"

    sget v18, Lcom/navdy/client/app/ui/settings/SettingsConstants;->AUDIO_OUTPUT_PREFERENCE_DEFAULT:I

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 175
    .local v2, "audioOutPreference":I
    invoke-static {}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->values()[Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    move-result-object v16

    aget-object v16, v16, v2

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioOutput:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    .line 177
    const/4 v13, 0x0

    .line 178
    .local v13, "voiceAvailable":Z
    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v17, 0x15

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_2

    .line 180
    const/4 v13, 0x1

    .line 182
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getTextToSpeech()Landroid/speech/tts/TextToSpeech;

    move-result-object v9

    .line 183
    .local v9, "textToSpeech":Landroid/speech/tts/TextToSpeech;
    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getVoice()Landroid/speech/tts/Voice;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v16, v0

    if-nez v16, :cond_1

    .line 185
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "TTS getVoice() returned null, trying to select default voice"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 186
    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getDefaultVoice()Landroid/speech/tts/Voice;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v16, v0

    if-eqz v16, :cond_6

    .line 188
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Default voice : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/speech/tts/TextToSpeech;->setVoice(Landroid/speech/tts/Voice;)I

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/speech/tts/Voice;->getName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    .line 195
    :cond_1
    :goto_2
    invoke-virtual {v9}, Landroid/speech/tts/TextToSpeech;->getVoices()Ljava/util/Set;

    move-result-object v15

    .line 196
    .local v15, "voicesSet":Ljava/util/Set;, "Ljava/util/Set<Landroid/speech/tts/Voice;>;"
    if-nez v15, :cond_7

    .line 197
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "List of voices available is null. Not giving an option to select the voice"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 198
    const/4 v13, 0x0

    .line 239
    .end local v9    # "textToSpeech":Landroid/speech/tts/TextToSpeech;
    .end local v15    # "voicesSet":Ljava/util/Set;, "Ljava/util/Set<Landroid/speech/tts/Voice;>;"
    :cond_2
    :goto_3
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "Initializing the Audio preferences"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 240
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Welcome Message: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mWelcomeMessage:Z

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 241
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "TTS Volume : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTtsLevel:F

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 242
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "TBT : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTbtInstructionsIsOn:Z

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 243
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Speed warning : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeedWarningsIsOn:Z

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 244
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Camera warning : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarningsIsOn:Z

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 245
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Output Preference : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioOutput:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->name()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 246
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Saved Voice : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mMainDescription:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const v17, 0x7f08041f

    invoke-static/range {v17 .. v17}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTurnByTurnNavigation:Landroid/widget/Switch;

    move-object/from16 v16, v0

    if-eqz v16, :cond_3

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTurnByTurnNavigation:Landroid/widget/Switch;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTbtInstructionsIsOn:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/Switch;->setChecked(Z)V

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTurnByTurnNavigation:Landroid/widget/Switch;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 255
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTtsLevel:F

    move/from16 v16, v0

    const v17, 0x3e99999a    # 0.3f

    cmpg-float v16, v16, v17

    if-gtz v16, :cond_d

    const/4 v11, 0x1

    .line 256
    .local v11, "ttsIsLow":Z
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTtsLevel:F

    move/from16 v16, v0

    const/high16 v17, 0x3f800000    # 1.0f

    cmpl-float v16, v16, v17

    if-ltz v16, :cond_e

    const/4 v10, 0x1

    .line 257
    .local v10, "ttsIsLoud":Z
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->speechLevelSettings:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    move-object/from16 v17, v0

    if-eqz v11, :cond_f

    const/16 v16, 0x0

    :goto_6
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->setSelectedIndex(I)V

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->speechLevelSettings:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    move-object/from16 v16, v0

    new-instance v17, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->setChoiceListener(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;)V

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mWelcomeMessageSwitch:Landroid/widget/Switch;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mWelcomeMessage:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/Switch;->setChecked(Z)V

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mWelcomeMessageSwitch:Landroid/widget/Switch;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSppedLimitWarnings:Landroid/widget/Switch;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeedWarningsIsOn:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/Switch;->setChecked(Z)V

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSppedLimitWarnings:Landroid/widget/Switch;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarnings:Landroid/widget/Switch;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarningsIsOn:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/Switch;->setChecked(Z)V

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarnings:Landroid/widget/Switch;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 285
    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v17, 0x15

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_11

    if-eqz v13, :cond_11

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getTextToSpeech()Landroid/speech/tts/TextToSpeech;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/speech/tts/TextToSpeech;->getVoice()Landroid/speech/tts/Voice;

    move-result-object v8

    .line 287
    .local v8, "selectedVoice":Landroid/speech/tts/Voice;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTxtVoiceName:Landroid/widget/TextView;

    move-object/from16 v16, v0

    invoke-static {v8}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->getDisplayName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    .end local v8    # "selectedVoice":Landroid/speech/tts/Voice;
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioOutput:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    move-object/from16 v16, v0

    sget-object v17, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->BEST_AVAILABLE:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_12

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeechDelayPreference:Landroid/view/View;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setVisibility(I)V

    .line 296
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioOutput:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->setAudioOutputAndUpdateUI(Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;)V

    .line 297
    return-void

    .line 152
    .end local v2    # "audioOutPreference":I
    .end local v10    # "ttsIsLoud":Z
    .end local v11    # "ttsIsLow":Z
    .end local v13    # "voiceAvailable":Z
    .restart local v6    # "phoneVolumeLevel":I
    :cond_4
    const/16 v16, 0x4b

    move/from16 v0, v16

    if-gt v6, v0, :cond_5

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    const-string v17, "audio_tts_volume"

    const v18, 0x3f19999a    # 0.6f

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 155
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    const-string v17, "audio_tts_volume"

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    .line 156
    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 168
    .end local v6    # "phoneVolumeLevel":I
    :catch_0
    move-exception v3

    .line 170
    .local v3, "ce":Ljava/lang/ClassCastException;
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "Class cast exception while getting the saved voice"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    const-string v17, "audio_voice"

    invoke-interface/range {v16 .. v17}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_1

    .line 192
    .end local v3    # "ce":Ljava/lang/ClassCastException;
    .restart local v2    # "audioOutPreference":I
    .restart local v9    # "textToSpeech":Landroid/speech/tts/TextToSpeech;
    .restart local v13    # "voiceAvailable":Z
    :cond_6
    :try_start_2
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "Default voice is also null"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    .line 232
    .end local v9    # "textToSpeech":Landroid/speech/tts/TextToSpeech;
    :catch_1
    move-exception v4

    .line 233
    .local v4, "e":Ljava/lang/Throwable;
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "Exception while getting the voice "

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 234
    const/4 v13, 0x0

    goto/16 :goto_3

    .line 199
    .end local v4    # "e":Ljava/lang/Throwable;
    .restart local v9    # "textToSpeech":Landroid/speech/tts/TextToSpeech;
    .restart local v15    # "voicesSet":Ljava/util/Set;, "Ljava/util/Set<Landroid/speech/tts/Voice;>;"
    :cond_7
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_c

    .line 200
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "User selected voice "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v16, v0

    if-eqz v16, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/speech/tts/Voice;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 202
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "User selected voice is same as TTS voice currently configured"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 205
    :cond_8
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 206
    .local v14, "voicesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/speech/tts/Voice;>;"
    const/4 v5, 0x0

    .line 207
    .local v5, "found":Z
    :cond_9
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_a

    .line 208
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/speech/tts/Voice;

    .line 209
    .local v12, "voice":Landroid/speech/tts/Voice;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual {v12}, Landroid/speech/tts/Voice;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    .line 210
    invoke-virtual {v9, v12}, Landroid/speech/tts/TextToSpeech;->setVoice(Landroid/speech/tts/Voice;)I

    .line 211
    const/4 v5, 0x1

    .line 215
    .end local v12    # "voice":Landroid/speech/tts/Voice;
    :cond_a
    if-nez v5, :cond_2

    .line 216
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "Could not find the voice that user selected in the list of voices"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v16, v0

    if-eqz v16, :cond_b

    .line 218
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "Falling back to the voice currently configured"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/speech/tts/Voice;->getName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    goto/16 :goto_3

    .line 221
    :cond_b
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "No voice currently configured on the TTS"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 222
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    goto/16 :goto_3

    .line 227
    .end local v5    # "found":Z
    .end local v14    # "voicesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/speech/tts/Voice;>;"
    :cond_c
    sget-object v16, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "User selected voice is empty"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v16, v0

    if-eqz v16, :cond_2

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mInitialVoice:Landroid/speech/tts/Voice;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/speech/tts/Voice;->getName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_3

    .line 255
    .end local v9    # "textToSpeech":Landroid/speech/tts/TextToSpeech;
    .end local v15    # "voicesSet":Ljava/util/Set;, "Ljava/util/Set<Landroid/speech/tts/Voice;>;"
    :cond_d
    const/4 v11, 0x0

    goto/16 :goto_4

    .line 256
    .restart local v11    # "ttsIsLow":Z
    :cond_e
    const/4 v10, 0x0

    goto/16 :goto_5

    .line 257
    .restart local v10    # "ttsIsLoud":Z
    :cond_f
    if-eqz v10, :cond_10

    const/16 v16, 0x2

    goto/16 :goto_6

    :cond_10
    const/16 v16, 0x1

    goto/16 :goto_6

    .line 289
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mVoicePreference:Landroid/view/View;

    move-object/from16 v16, v0

    const/16 v17, 0x8

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    .line 294
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeechDelayPreference:Landroid/view/View;

    move-object/from16 v16, v0

    const/16 v17, 0x8

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8
.end method

.method public onDescriptionClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 513
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 514
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 301
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onResume()V

    .line 302
    const-string v1, "Settings_Audio"

    invoke-static {v1}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 303
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v2, "audio_hfp_delay_level"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 304
    .local v0, "level":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->tvSpeechLevel:Landroid/widget/TextView;

    add-int/lit8 v2, v0, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    return-void
.end method

.method protected saveChanges()V
    .locals 3

    .prologue
    .line 369
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio_tts_volume"

    iget v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTtsLevel:F

    .line 370
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio_play_welcome"

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mWelcomeMessage:Z

    .line 371
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio_turn_by_turn_instructions"

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTbtInstructionsIsOn:Z

    .line 372
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio_speed_warnings"

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeedWarningsIsOn:Z

    .line 373
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio_camera_warnings"

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarningsIsOn:Z

    .line 374
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio_voice"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    .line 375
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio_output_preference"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->audioOutput:Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;

    .line 376
    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$AudioOutput;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 377
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 378
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sendPreferencesToDisplay()V

    .line 379
    return-void
.end method

.method public setTtsLevel(F)V
    .locals 2
    .param p1, "ttsLevel"    # F

    .prologue
    .line 452
    iput p1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTtsLevel:F

    .line 453
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    iget v1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTtsLevel:F

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setTtsVolume(F)V

    .line 454
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->somethingChanged:Z

    .line 455
    return-void
.end method
