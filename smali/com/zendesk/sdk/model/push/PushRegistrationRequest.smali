.class public abstract Lcom/zendesk/sdk/model/push/PushRegistrationRequest;
.super Ljava/lang/Object;
.source "PushRegistrationRequest.java"


# static fields
.field private static final DEVICE_TYPE:Ljava/lang/String; = "android"
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "device_type"
    .end annotation
.end field


# instance fields
.field private identifier:Ljava/lang/String;

.field private locale:Ljava/lang/String;

.field private tokenType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;->identifier:Ljava/lang/String;

    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;->locale:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;->tokenType:Ljava/lang/String;

    return-object v0
.end method

.method public setIdentifier(Ljava/lang/String;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;->identifier:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setLocale(Ljava/lang/String;)V
    .locals 0
    .param p1, "locale"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;->locale:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setTokenType(Ljava/lang/String;)V
    .locals 0
    .param p1, "tokenType"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;->tokenType:Ljava/lang/String;

    .line 49
    return-void
.end method
