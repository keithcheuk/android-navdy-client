.class public Lnet/minidev/json/parser/JSONParser;
.super Ljava/lang/Object;
.source "JSONParser.java"


# static fields
.field public static final ACCEPT_LEADING_ZERO:I = 0x20

.field public static final ACCEPT_NAN:I = 0x4

.field public static final ACCEPT_NON_QUOTE:I = 0x2

.field public static final ACCEPT_SIMPLE_QUOTE:I = 0x1

.field public static final ACCEPT_TAILLING_DATA:I = 0x100

.field public static final ACCEPT_TAILLING_SPACE:I = 0x200

.field public static final ACCEPT_USELESS_COMMA:I = 0x40

.field public static DEFAULT_PERMISSIVE_MODE:I = 0x0

.field public static final IGNORE_CONTROL_CHAR:I = 0x8

.field public static final MODE_JSON_SIMPLE:I = 0x3c0

.field public static final MODE_PERMISSIVE:I = -0x1

.field public static final MODE_RFC4627:I = 0x190

.field public static final MODE_STRICTEST:I = 0x90

.field public static final USE_HI_PRECISION_FLOAT:I = 0x80

.field public static final USE_INTEGER_STORAGE:I = 0x10


# instance fields
.field private mode:I

.field private pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

.field private pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

.field private pStream:Lnet/minidev/json/parser/JSONParserReader;

.field private pString:Lnet/minidev/json/parser/JSONParserString;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const-string v0, "JSON_SMART_SIMPLE"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3c0

    :goto_0
    sput v0, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    sget v0, Lnet/minidev/json/parser/JSONParser;->DEFAULT_PERMISSIVE_MODE:I

    iput v0, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    .line 127
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "permissifMode"    # I

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput p1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    .line 131
    return-void
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 239
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

    if-nez v0, :cond_0

    .line 240
    new-instance v0, Lnet/minidev/json/parser/JSONParserInputStream;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserInputStream;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

    .line 241
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

    invoke-virtual {v0, p1}, Lnet/minidev/json/parser/JSONParserInputStream;->parse(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/io/InputStream;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

    if-nez v0, :cond_0

    .line 250
    new-instance v0, Lnet/minidev/json/parser/JSONParserInputStream;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserInputStream;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

    .line 251
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

    invoke-virtual {v0, p1, p2}, Lnet/minidev/json/parser/JSONParserInputStream;->parse(Ljava/io/InputStream;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/io/InputStream;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .param p3, "handler"    # Lnet/minidev/json/parser/ContentHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

    if-nez v0, :cond_0

    .line 261
    new-instance v0, Lnet/minidev/json/parser/JSONParserInputStream;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserInputStream;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

    .line 262
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pSBintream:Lnet/minidev/json/parser/JSONParserInputStream;

    invoke-virtual {v0, p1, p2, p3}, Lnet/minidev/json/parser/JSONParserInputStream;->parse(Ljava/io/InputStream;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/io/Reader;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pStream:Lnet/minidev/json/parser/JSONParserReader;

    if-nez v0, :cond_0

    .line 210
    new-instance v0, Lnet/minidev/json/parser/JSONParserReader;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserReader;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pStream:Lnet/minidev/json/parser/JSONParserReader;

    .line 211
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pStream:Lnet/minidev/json/parser/JSONParserReader;

    invoke-virtual {v0, p1}, Lnet/minidev/json/parser/JSONParserReader;->parse(Ljava/io/Reader;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Ljava/io/Reader;
    .param p2, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pStream:Lnet/minidev/json/parser/JSONParserReader;

    if-nez v0, :cond_0

    .line 220
    new-instance v0, Lnet/minidev/json/parser/JSONParserReader;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserReader;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pStream:Lnet/minidev/json/parser/JSONParserReader;

    .line 221
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pStream:Lnet/minidev/json/parser/JSONParserReader;

    invoke-virtual {v0, p1, p2}, Lnet/minidev/json/parser/JSONParserReader;->parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Ljava/io/Reader;
    .param p2, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .param p3, "handler"    # Lnet/minidev/json/parser/ContentHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pStream:Lnet/minidev/json/parser/JSONParserReader;

    if-nez v0, :cond_0

    .line 230
    new-instance v0, Lnet/minidev/json/parser/JSONParserReader;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserReader;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pStream:Lnet/minidev/json/parser/JSONParserReader;

    .line 231
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pStream:Lnet/minidev/json/parser/JSONParserReader;

    invoke-virtual {v0, p1, p2, p3}, Lnet/minidev/json/parser/JSONParserReader;->parse(Ljava/io/Reader;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pString:Lnet/minidev/json/parser/JSONParserString;

    if-nez v0, :cond_0

    .line 139
    new-instance v0, Lnet/minidev/json/parser/JSONParserString;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserString;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pString:Lnet/minidev/json/parser/JSONParserString;

    .line 140
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pString:Lnet/minidev/json/parser/JSONParserString;

    invoke-virtual {v0, p1}, Lnet/minidev/json/parser/JSONParserString;->parse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Ljava/lang/String;
    .param p2, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pString:Lnet/minidev/json/parser/JSONParserString;

    if-nez v0, :cond_0

    .line 149
    new-instance v0, Lnet/minidev/json/parser/JSONParserString;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserString;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pString:Lnet/minidev/json/parser/JSONParserString;

    .line 150
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pString:Lnet/minidev/json/parser/JSONParserString;

    invoke-virtual {v0, p1, p2}, Lnet/minidev/json/parser/JSONParserString;->parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # Ljava/lang/String;
    .param p2, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .param p3, "handler"    # Lnet/minidev/json/parser/ContentHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pString:Lnet/minidev/json/parser/JSONParserString;

    if-nez v0, :cond_0

    .line 155
    new-instance v0, Lnet/minidev/json/parser/JSONParserString;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserString;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pString:Lnet/minidev/json/parser/JSONParserString;

    .line 156
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pString:Lnet/minidev/json/parser/JSONParserString;

    invoke-virtual {v0, p1, p2, p3}, Lnet/minidev/json/parser/JSONParserString;->parse(Ljava/lang/String;Lnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse([B)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    if-nez v0, :cond_0

    .line 165
    new-instance v0, Lnet/minidev/json/parser/JSONParserByteArray;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserByteArray;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    .line 166
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    invoke-virtual {v0, p1}, Lnet/minidev/json/parser/JSONParserByteArray;->parse([B)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse([BII)Ljava/lang/Object;
    .locals 6
    .param p1, "in"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Lnet/minidev/json/parser/JSONParserByteArray;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserByteArray;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    .line 188
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    sget-object v4, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    sget-object v5, Lnet/minidev/json/parser/ContentHandlerDumy;->HANDLER:Lnet/minidev/json/parser/ContentHandlerDumy;

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lnet/minidev/json/parser/JSONParserByteArray;->parse([BIILnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse([BIILnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    .locals 6
    .param p1, "in"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    if-nez v0, :cond_0

    .line 193
    new-instance v0, Lnet/minidev/json/parser/JSONParserByteArray;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserByteArray;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    .line 194
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    sget-object v5, Lnet/minidev/json/parser/ContentHandlerDumy;->HANDLER:Lnet/minidev/json/parser/ContentHandlerDumy;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lnet/minidev/json/parser/JSONParserByteArray;->parse([BIILnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse([BIILnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;
    .locals 6
    .param p1, "in"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .param p5, "handler"    # Lnet/minidev/json/parser/ContentHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Lnet/minidev/json/parser/JSONParserByteArray;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserByteArray;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    .line 201
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lnet/minidev/json/parser/JSONParserByteArray;->parse([BIILnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse([BLnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # [B
    .param p2, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    if-nez v0, :cond_0

    .line 175
    new-instance v0, Lnet/minidev/json/parser/JSONParserByteArray;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserByteArray;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    .line 176
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    invoke-virtual {v0, p1, p2}, Lnet/minidev/json/parser/JSONParserByteArray;->parse([BLnet/minidev/json/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public parse([BLnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;
    .locals 2
    .param p1, "in"    # [B
    .param p2, "containerFactory"    # Lnet/minidev/json/parser/ContainerFactory;
    .param p3, "handler"    # Lnet/minidev/json/parser/ContentHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/minidev/json/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    if-nez v0, :cond_0

    .line 181
    new-instance v0, Lnet/minidev/json/parser/JSONParserByteArray;

    iget v1, p0, Lnet/minidev/json/parser/JSONParser;->mode:I

    invoke-direct {v0, v1}, Lnet/minidev/json/parser/JSONParserByteArray;-><init>(I)V

    iput-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    .line 182
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/parser/JSONParser;->pBytes:Lnet/minidev/json/parser/JSONParserByteArray;

    invoke-virtual {v0, p1, p2, p3}, Lnet/minidev/json/parser/JSONParserByteArray;->parse([BLnet/minidev/json/parser/ContainerFactory;Lnet/minidev/json/parser/ContentHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
