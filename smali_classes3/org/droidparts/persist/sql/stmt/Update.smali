.class public Lorg/droidparts/persist/sql/stmt/Update;
.super Lorg/droidparts/persist/sql/stmt/Statement;
.source "Update.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<EntityType:",
        "Lorg/droidparts/model/Entity;",
        ">",
        "Lorg/droidparts/persist/sql/stmt/Statement",
        "<TEntityType;>;"
    }
.end annotation


# instance fields
.field private contentValues:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    .line 31
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-direct {p0, p1, p2}, Lorg/droidparts/persist/sql/stmt/Statement;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/droidparts/persist/sql/stmt/Update;->contentValues:Landroid/content/ContentValues;

    .line 32
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 8

    .prologue
    .line 62
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-virtual {p0}, Lorg/droidparts/persist/sql/stmt/Update;->getSelection()Landroid/util/Pair;

    move-result-object v2

    .line 63
    .local v2, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/droidparts/persist/sql/stmt/Update;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 64
    const/4 v1, 0x0

    .line 66
    .local v1, "rowCount":I
    :try_start_0
    iget-object v5, p0, Lorg/droidparts/persist/sql/stmt/Update;->db:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v6, p0, Lorg/droidparts/persist/sql/stmt/Update;->tableName:Ljava/lang/String;

    iget-object v7, p0, Lorg/droidparts/persist/sql/stmt/Update;->contentValues:Landroid/content/ContentValues;

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 72
    :goto_0
    return v1

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/droidparts/util/L;->e(Ljava/lang/Object;)V

    .line 70
    invoke-static {v0}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setValues(Landroid/content/ContentValues;)Lorg/droidparts/persist/sql/stmt/Update;
    .locals 0
    .param p1, "contentValues"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentValues;",
            ")",
            "Lorg/droidparts/persist/sql/stmt/Update",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    iput-object p1, p0, Lorg/droidparts/persist/sql/stmt/Update;->contentValues:Landroid/content/ContentValues;

    .line 58
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lorg/droidparts/persist/sql/stmt/Statement;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contentValues: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/droidparts/persist/sql/stmt/Update;->contentValues:Landroid/content/ContentValues;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic where(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Statement;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lorg/droidparts/persist/sql/stmt/Is;
    .param p3, "x2"    # [Ljava/lang/Object;

    .prologue
    .line 26
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-virtual {p0, p1, p2, p3}, Lorg/droidparts/persist/sql/stmt/Update;->where(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Update;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic where(Ljava/lang/String;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Statement;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 26
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-virtual {p0, p1, p2}, Lorg/droidparts/persist/sql/stmt/Update;->where(Ljava/lang/String;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Update;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic where(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Statement;
    .locals 1
    .param p1, "x0"    # Lorg/droidparts/persist/sql/stmt/Where;

    .prologue
    .line 26
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/sql/stmt/Update;->where(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Update;

    move-result-object v0

    return-object v0
.end method

.method public varargs where(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Update;
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "operator"    # Lorg/droidparts/persist/sql/stmt/Is;
    .param p3, "columnValue"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/droidparts/persist/sql/stmt/Is;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Lorg/droidparts/persist/sql/stmt/Update",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-super {p0, p1, p2, p3}, Lorg/droidparts/persist/sql/stmt/Statement;->where(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Statement;

    move-result-object v0

    check-cast v0, Lorg/droidparts/persist/sql/stmt/Update;

    return-object v0
.end method

.method public varargs where(Ljava/lang/String;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Update;
    .locals 1
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Lorg/droidparts/persist/sql/stmt/Update",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-super {p0, p1, p2}, Lorg/droidparts/persist/sql/stmt/Statement;->where(Ljava/lang/String;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Statement;

    move-result-object v0

    check-cast v0, Lorg/droidparts/persist/sql/stmt/Update;

    return-object v0
.end method

.method protected where(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Update;
    .locals 1
    .param p1, "where"    # Lorg/droidparts/persist/sql/stmt/Where;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/persist/sql/stmt/Where;",
            ")",
            "Lorg/droidparts/persist/sql/stmt/Update",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-super {p0, p1}, Lorg/droidparts/persist/sql/stmt/Statement;->where(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Statement;

    move-result-object v0

    check-cast v0, Lorg/droidparts/persist/sql/stmt/Update;

    return-object v0
.end method

.method public bridge synthetic whereId([J)Lorg/droidparts/persist/sql/stmt/Statement;
    .locals 1
    .param p1, "x0"    # [J

    .prologue
    .line 26
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/sql/stmt/Update;->whereId([J)Lorg/droidparts/persist/sql/stmt/Update;

    move-result-object v0

    return-object v0
.end method

.method public varargs whereId([J)Lorg/droidparts/persist/sql/stmt/Update;
    .locals 1
    .param p1, "oneOrMore"    # [J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)",
            "Lorg/droidparts/persist/sql/stmt/Update",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Update;, "Lorg/droidparts/persist/sql/stmt/Update<TEntityType;>;"
    invoke-super {p0, p1}, Lorg/droidparts/persist/sql/stmt/Statement;->whereId([J)Lorg/droidparts/persist/sql/stmt/Statement;

    move-result-object v0

    check-cast v0, Lorg/droidparts/persist/sql/stmt/Update;

    return-object v0
.end method
