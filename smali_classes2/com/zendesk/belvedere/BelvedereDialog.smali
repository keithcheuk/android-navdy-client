.class public Lcom/zendesk/belvedere/BelvedereDialog;
.super Landroid/support/v7/app/AppCompatDialogFragment;
.source "BelvedereDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;,
        Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;,
        Lcom/zendesk/belvedere/BelvedereDialog$Adapter;
    }
.end annotation


# static fields
.field private static final EXTRA_INTENT:Ljava/lang/String; = "extra_intent"

.field private static final FRAGMENT_TAG:Ljava/lang/String; = "BelvedereDialog"

.field private static final LOG_TAG:Ljava/lang/String; = "BelvedereDialog"


# instance fields
.field private listView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDialogFragment;-><init>()V

    .line 183
    return-void
.end method

.method private fillListView(Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;Ljava/util/List;)V
    .locals 4
    .param p1, "activity"    # Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereIntent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p2, "intents":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereIntent;>;"
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereDialog;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/zendesk/belvedere/BelvedereDialog$Adapter;

    invoke-interface {p1}, Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/zendesk/belvedere/R$layout;->belvedere_dialog_row:I

    invoke-direct {v1, p0, v2, v3, p2}, Lcom/zendesk/belvedere/BelvedereDialog$Adapter;-><init>(Lcom/zendesk/belvedere/BelvedereDialog;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 116
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereDialog;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/zendesk/belvedere/BelvedereDialog$3;

    invoke-direct {v1, p0, p1}, Lcom/zendesk/belvedere/BelvedereDialog$3;-><init>(Lcom/zendesk/belvedere/BelvedereDialog;Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 125
    return-void
.end method

.method public static showDialog(Landroid/support/v4/app/FragmentManager;Ljava/util/List;)V
    .locals 4
    .param p0, "fm"    # Landroid/support/v4/app/FragmentManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentManager;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereIntent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "belvedereIntent":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereIntent;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    new-instance v0, Lcom/zendesk/belvedere/BelvedereDialog;

    invoke-direct {v0}, Lcom/zendesk/belvedere/BelvedereDialog;-><init>()V

    .line 51
    .local v0, "attachmentSourceSelectorDialog":Lcom/zendesk/belvedere/BelvedereDialog;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 53
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "extra_intent"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 54
    invoke-virtual {v0, v1}, Lcom/zendesk/belvedere/BelvedereDialog;->setArguments(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "BelvedereDialog"

    invoke-virtual {v0, v2, v3}, Lcom/zendesk/belvedere/BelvedereDialog;->show(Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/zendesk/belvedere/BelvedereDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "extra_intent"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 77
    .local v1, "intents":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereIntent;>;"
    invoke-virtual {p0}, Lcom/zendesk/belvedere/BelvedereDialog;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/zendesk/belvedere/BelvedereDialog;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 80
    .local v2, "parentFragment":Landroid/support/v4/app/Fragment;
    new-instance v3, Lcom/zendesk/belvedere/BelvedereDialog$1;

    invoke-direct {v3, p0, v2}, Lcom/zendesk/belvedere/BelvedereDialog$1;-><init>(Lcom/zendesk/belvedere/BelvedereDialog;Landroid/support/v4/app/Fragment;)V

    invoke-direct {p0, v3, v1}, Lcom/zendesk/belvedere/BelvedereDialog;->fillListView(Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;Ljava/util/List;)V

    .line 112
    .end local v2    # "parentFragment":Landroid/support/v4/app/Fragment;
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/zendesk/belvedere/BelvedereDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 94
    invoke-virtual {p0}, Lcom/zendesk/belvedere/BelvedereDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 95
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    new-instance v3, Lcom/zendesk/belvedere/BelvedereDialog$2;

    invoke-direct {v3, p0, v0}, Lcom/zendesk/belvedere/BelvedereDialog$2;-><init>(Lcom/zendesk/belvedere/BelvedereDialog;Landroid/support/v4/app/FragmentActivity;)V

    invoke-direct {p0, v3, v1}, Lcom/zendesk/belvedere/BelvedereDialog;->fillListView(Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;Ljava/util/List;)V

    goto :goto_0

    .line 108
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    :cond_1
    const-string v3, "BelvedereDialog"

    const-string v4, "Not able to find a valid context for starting an BelvedereIntent"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {p0}, Lcom/zendesk/belvedere/BelvedereDialog;->dismiss()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/zendesk/belvedere/BelvedereDialog;->getTheme()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/zendesk/belvedere/BelvedereDialog;->setStyle(II)V

    .line 70
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 61
    sget v1, Lcom/zendesk/belvedere/R$layout;->belvedere_dialog:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 62
    .local v0, "view":Landroid/view/View;
    sget v1, Lcom/zendesk/belvedere/R$id;->belvedere_dialog_listview:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/zendesk/belvedere/BelvedereDialog;->listView:Landroid/widget/ListView;

    .line 63
    return-object v0
.end method
