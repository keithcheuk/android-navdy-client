.class Lcom/zendesk/belvedere/BelvedereDialog$2;
.super Ljava/lang/Object;
.source "BelvedereDialog.java"

# interfaces
.implements Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/belvedere/BelvedereDialog;->onActivityCreated(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/belvedere/BelvedereDialog;

.field final synthetic val$activity:Landroid/support/v4/app/FragmentActivity;


# direct methods
.method constructor <init>(Lcom/zendesk/belvedere/BelvedereDialog;Landroid/support/v4/app/FragmentActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/belvedere/BelvedereDialog;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/zendesk/belvedere/BelvedereDialog$2;->this$0:Lcom/zendesk/belvedere/BelvedereDialog;

    iput-object p2, p0, Lcom/zendesk/belvedere/BelvedereDialog$2;->val$activity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereDialog$2;->val$activity:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method public startActivity(Lcom/zendesk/belvedere/BelvedereIntent;)V
    .locals 1
    .param p1, "belvedereIntent"    # Lcom/zendesk/belvedere/BelvedereIntent;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereDialog$2;->val$activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1, v0}, Lcom/zendesk/belvedere/BelvedereIntent;->open(Landroid/app/Activity;)V

    .line 99
    return-void
.end method
