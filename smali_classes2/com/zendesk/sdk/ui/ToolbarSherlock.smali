.class public Lcom/zendesk/sdk/ui/ToolbarSherlock;
.super Ljava/lang/Object;
.source "ToolbarSherlock.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ToolbarSherlock"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static installToolBar(Landroid/support/v7/app/AppCompatActivity;)V
    .locals 8
    .param p0, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .prologue
    const/4 v7, 0x0

    .line 37
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 39
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 40
    const-string v4, "ToolbarSherlock"

    const-string v5, "ActionBar available."

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    :goto_0
    return-void

    .line 44
    :cond_0
    const-string v4, "ToolbarSherlock"

    const-string v5, "No Actionbar detected. Installing Toolbar"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    sget v4, Lcom/zendesk/sdk/R$id;->zd_toolbar_container:I

    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 48
    .local v1, "toolBarContainer":Landroid/view/View;
    if-nez v1, :cond_1

    .line 49
    const-string v4, "ToolbarSherlock"

    const-string v5, "Unable to find toolbar in Activity."

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :cond_1
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 55
    sget v4, Lcom/zendesk/sdk/R$id;->zd_toolbar:I

    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/Toolbar;

    .line 56
    .local v3, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v3}, Landroid/support/v7/app/AppCompatActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 58
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_2

    .line 59
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/zendesk/sdk/R$dimen;->zd_toolbar_elevation:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/support/v7/app/ActionBar;->setElevation(F)V

    goto :goto_0

    .line 62
    :cond_2
    const-string v4, "ToolbarSherlock"

    const-string v5, "Device pre-Lollipop. Enable Toolbar shadow workaround."

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    sget v4, Lcom/zendesk/sdk/R$id;->zd_toolbar_shadow:I

    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 65
    .local v2, "toolBarShadow":Landroid/view/View;
    if-eqz v2, :cond_3

    .line 66
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 68
    :cond_3
    const-string v4, "ToolbarSherlock"

    const-string v5, "Toolbar shadow is missing"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
