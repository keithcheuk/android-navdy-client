.class public interface abstract Lcom/zendesk/sdk/storage/StorageStore;
.super Ljava/lang/Object;
.source "StorageStore.java"


# virtual methods
.method public abstract helpCenterSessionCache()Lcom/zendesk/sdk/storage/HelpCenterSessionCache;
.end method

.method public abstract identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;
.end method

.method public abstract requestStorage()Lcom/zendesk/sdk/storage/RequestStorage;
.end method

.method public abstract sdkSettingsStorage()Lcom/zendesk/sdk/storage/SdkSettingsStorage;
.end method

.method public abstract sdkStorage()Lcom/zendesk/sdk/storage/SdkStorage;
.end method
