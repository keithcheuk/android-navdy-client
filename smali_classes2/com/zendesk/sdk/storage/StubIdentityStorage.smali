.class Lcom/zendesk/sdk/storage/StubIdentityStorage;
.super Ljava/lang/Object;
.source "StubIdentityStorage.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/IdentityStorage;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "StubIdentityStorage"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public anonymiseIdentity()Lcom/zendesk/sdk/model/access/Identity;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 56
    const-string v0, "StubIdentityStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    new-instance v0, Lcom/zendesk/sdk/storage/StubIdentity;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/StubIdentity;-><init>()V

    return-object v0
.end method

.method public clearUserData()V
    .locals 3

    .prologue
    .line 62
    const-string v0, "StubIdentityStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    return-void
.end method

.method public getCacheKey()Ljava/lang/String;
    .locals 3

    .prologue
    .line 67
    const-string v0, "StubIdentityStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    const-string v0, ""

    return-object v0
.end method

.method public getIdentity()Lcom/zendesk/sdk/model/access/Identity;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 49
    const-string v0, "StubIdentityStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    new-instance v0, Lcom/zendesk/sdk/storage/StubIdentity;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/StubIdentity;-><init>()V

    return-object v0
.end method

.method public getStoredAccessToken()Lcom/zendesk/sdk/model/access/AccessToken;
    .locals 3

    .prologue
    .line 25
    const-string v0, "StubIdentityStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    new-instance v0, Lcom/zendesk/sdk/model/access/AccessToken;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/access/AccessToken;-><init>()V

    return-object v0
.end method

.method public getStoredAccessTokenAsBearerToken()Ljava/lang/String;
    .locals 3

    .prologue
    .line 31
    const-string v0, "StubIdentityStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    const-string v0, ""

    return-object v0
.end method

.method public getUUID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 37
    const-string v0, "StubIdentityStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    const-string v0, ""

    return-object v0
.end method

.method public storeAccessToken(Lcom/zendesk/sdk/model/access/AccessToken;)V
    .locals 3
    .param p1, "accessToken"    # Lcom/zendesk/sdk/model/access/AccessToken;

    .prologue
    .line 20
    const-string v0, "StubIdentityStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    return-void
.end method

.method public storeIdentity(Lcom/zendesk/sdk/model/access/Identity;)V
    .locals 3
    .param p1, "identity"    # Lcom/zendesk/sdk/model/access/Identity;

    .prologue
    .line 43
    const-string v0, "StubIdentityStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    return-void
.end method
