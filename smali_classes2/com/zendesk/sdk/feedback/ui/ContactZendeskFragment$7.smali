.class Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ContactZendeskFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->setUpCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 524
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 3
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 548
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$700(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 550
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 551
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->networkNotAvailable()V

    .line 552
    invoke-static {}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Preload settings: Network not available."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 554
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1300(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/Retryable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1300(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/Retryable;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    sget v2, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_send_error_toast:I

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7$1;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7$1;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;)V

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/network/Retryable;->onRetryAvailable(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 4
    .param p1, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 527
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0, p1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$402(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .line 529
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    iget-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v3, p1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->isEmailFieldEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v3

    invoke-static {v0, v3}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$502(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;Z)Z

    .line 530
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$600(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    move-result-object v3

    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$500(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$700(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 533
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$800(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 535
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$900(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1000(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1100(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    .line 537
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1200(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 541
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->networkNotAvailable()V

    .line 542
    invoke-static {}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Preload settings: Network not available."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 544
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 530
    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 524
    check-cast p1, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;->onSuccess(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    return-void
.end method
