.class Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;
.super Landroid/widget/FrameLayout;
.source "AttachmentContainerHost.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AttachmentContainer"
.end annotation


# instance fields
.field private mAttachmentState:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

.field private mDeleteButton:Landroid/widget/Button;

.field private mFile:Ljava/io/File;

.field private mImageView:Landroid/widget/ImageView;

.field private mOrientation:I

.field private mProgressBar:Landroid/widget/ProgressBar;

.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;Landroid/content/Context;Ljava/io/File;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;I)V
    .locals 4
    .param p1, "this$0"    # Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "file"    # Ljava/io/File;
    .param p4, "parent"    # Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;
    .param p5, "orientation"    # I

    .prologue
    .line 276
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->this$0:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    .line 277
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 279
    iput-object p3, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mFile:Ljava/io/File;

    .line 280
    sget-object v1, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADING:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    iput-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mAttachmentState:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    .line 281
    iput p5, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mOrientation:I

    .line 283
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$layout;->view_attachment_container_item:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 284
    .local v0, "v":Landroid/view/View;
    sget v1, Lcom/zendesk/sdk/R$id;->attachment_image:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mImageView:Landroid/widget/ImageView;

    .line 285
    sget v1, Lcom/zendesk/sdk/R$id;->attachment_delete:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mDeleteButton:Landroid/widget/Button;

    .line 286
    sget v1, Lcom/zendesk/sdk/R$id;->attachment_progress:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mProgressBar:Landroid/widget/ProgressBar;

    .line 287
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 289
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mDeleteButton:Landroid/widget/Button;

    new-instance v2, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer$1;

    invoke-direct {v2, p0, p1, p4, p3}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer$1;-><init>(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;Ljava/io/File;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mFile:Ljava/io/File;

    iget-object v2, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mImageView:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v2, p4, p2}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->attachImage(Ljava/io/File;Landroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/content/Context;)V

    .line 297
    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->addView(Landroid/view/View;)V

    .line 298
    return-void
.end method

.method private attachImage(Ljava/io/File;Landroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/content/Context;)V
    .locals 6
    .param p1, "file"    # Ljava/io/File;
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 337
    iget v4, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mOrientation:I

    packed-switch v4, :pswitch_data_0

    .line 359
    :goto_0
    iget-object v4, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mAttachmentState:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {p0, v4}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->setState(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;)V

    .line 360
    return-void

    .line 340
    :pswitch_0
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/zendesk/sdk/R$dimen;->attachment_container_host_horizontal_height:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 342
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/zendesk/sdk/R$dimen;->attachment_container_image_margin:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    mul-int/lit8 v2, v4, 0x2

    .line 343
    .local v2, "marginVertical":I
    sget-object v4, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->INSTANCE:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    sub-int v5, v0, v2

    invoke-virtual {v4, v5}, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->getResizeTransformationHeight(I)Lcom/squareup/picasso/Transformation;

    move-result-object v4

    invoke-direct {p0, p2, v4, p1, p4}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->loadFileIntoImageView(Landroid/widget/ImageView;Lcom/squareup/picasso/Transformation;Ljava/io/File;Landroid/content/Context;)V

    goto :goto_0

    .line 347
    .end local v0    # "height":I
    .end local v2    # "marginVertical":I
    :pswitch_1
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v3

    .line 349
    .local v3, "widthPixels":I
    if-nez v3, :cond_0

    .line 350
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v3, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 353
    :cond_0
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/zendesk/sdk/R$dimen;->attachment_container_image_margin:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    mul-int/lit8 v1, v4, 0x2

    .line 354
    .local v1, "marginHorizontal":I
    sget-object v4, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->INSTANCE:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    sub-int v5, v3, v1

    invoke-virtual {v4, v5}, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->getResizeTransformationWidth(I)Lcom/squareup/picasso/Transformation;

    move-result-object v4

    invoke-direct {p0, p2, v4, p1, p4}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->loadFileIntoImageView(Landroid/widget/ImageView;Lcom/squareup/picasso/Transformation;Ljava/io/File;Landroid/content/Context;)V

    goto :goto_0

    .line 337
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private loadFileIntoImageView(Landroid/widget/ImageView;Lcom/squareup/picasso/Transformation;Ljava/io/File;Landroid/content/Context;)V
    .locals 1
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "transformation"    # Lcom/squareup/picasso/Transformation;
    .param p3, "file"    # Ljava/io/File;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 363
    invoke-static {p4}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    .line 364
    invoke-virtual {v0, p3}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 365
    invoke-virtual {v0, p2}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 366
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 367
    invoke-virtual {v0, p1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 368
    return-void
.end method


# virtual methods
.method public getAttachmentState()Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mAttachmentState:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mFile:Ljava/io/File;

    return-object v0
.end method

.method public setState(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;)V
    .locals 4
    .param p1, "attachmentState"    # Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 306
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mAttachmentState:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    .line 308
    sget-object v0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$1;->$SwitchMap$com$zendesk$sdk$feedback$ui$AttachmentContainerHost$AttachmentState:[I

    invoke-virtual {p1}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 325
    :goto_0
    return-void

    .line 311
    :pswitch_0
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mDeleteButton:Landroid/widget/Button;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 312
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-static {v0, v3}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    goto :goto_0

    .line 316
    :pswitch_1
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mDeleteButton:Landroid/widget/Button;

    invoke-static {v0, v3}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 317
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    goto :goto_0

    .line 321
    :pswitch_2
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mDeleteButton:Landroid/widget/Button;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 322
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainer;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    goto :goto_0

    .line 308
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
