.class public Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;
.super Landroid/support/v4/app/Fragment;
.source "ContactZendeskFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;,
        Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;
    }
.end annotation


# static fields
.field public static final EXTRA_CONFIGURATION:Ljava/lang/String; = "EXTRA_CONFIGURATION"

.field public static final EXTRA_CONFIGURATION_ADDITIONAL_INFO:Ljava/lang/String; = "EXTRA_CONFIGURATION_ADDITIONAL"

.field public static final EXTRA_CONFIGURATION_REQUEST_SUBJECT:Ljava/lang/String; = "EXTRA_CONFIGURATION_SUBJECT"

.field public static final EXTRA_CONFIGURATION_TAGS:Ljava/lang/String; = "EXTRA_CONFIGURATION_TAGS"

.field private static final FOURTY_PERCENT_OPACITY:I = 0x66

.field private static final FULL_OPACTITY:I = 0xff

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

.field private attachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

.field private attachmentItem:Landroid/view/MenuItem;

.field private contactZendeskFeedbackConfiguration:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;

.field private container:Landroid/view/ViewGroup;

.field private descriptionEditText:Landroid/widget/EditText;

.field private doneMenuItem:Landroid/view/MenuItem;

.field private emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

.field private feedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

.field private imageUploadProgressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

.field private imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

.field private imagesExtracted:Lcom/zendesk/belvedere/BelvedereCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/belvedere/BelvedereCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;>;"
        }
    .end annotation
.end field

.field private isEmailFieldVisible:Z

.field private mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

.field private progressBar:Landroid/widget/ProgressBar;

.field private requestCallback:Lcom/zendesk/service/SafeZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/SafeZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            ">;"
        }
    .end annotation
.end field

.field private retryable:Lcom/zendesk/sdk/network/Retryable;

.field private settingsCallback:Lcom/zendesk/service/SafeZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/SafeZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            ">;"
        }
    .end annotation
.end field

.field private settingsHelper:Lcom/zendesk/sdk/network/SettingsHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->checkSendButtonState()V

    return-void
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/attachment/ImageUploadHelper;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->disableSendButton()V

    return-void
.end method

.method static synthetic access$1200(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->displayAttachmentButton()V

    return-void
.end method

.method static synthetic access$1300(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/Retryable;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->retryable:Lcom/zendesk/sdk/network/Retryable;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->preloadSettingsAndInit()V

    return-void
.end method

.method static synthetic access$1500(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/SubmissionListener;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->feedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->reinitializeFragment()V

    return-void
.end method

.method static synthetic access$1700(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->enableSendButton()V

    return-void
.end method

.method static synthetic access$1800(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->descriptionEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->sendFeedback()V

    return-void
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;
    .param p1, "x1"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    return-object p1
.end method

.method static synthetic access$500(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->isEmailFieldVisible:Z

    return v0
.end method

.method static synthetic access$502(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->isEmailFieldVisible:Z

    return p1
.end method

.method static synthetic access$600(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->progressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->container:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$900(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->doneMenuItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method private static bundleToZendeskFeedbackConfiguration(Landroid/os/Bundle;)Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    .locals 1
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 144
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$1;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$1;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private checkSendButtonState()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 307
    iget-object v5, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->descriptionEditText:Landroid/widget/EditText;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->descriptionEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    .line 308
    .local v0, "descriptionPresent":Z
    :goto_0
    iget-object v5, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v5}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->isImageUploadCompleted()Z

    move-result v2

    .line 312
    .local v2, "imagesUploaded":Z
    iget-boolean v5, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->isEmailFieldVisible:Z

    if-eqz v5, :cond_2

    .line 313
    iget-object v5, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    invoke-virtual {v5}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->isInputValid()Z

    move-result v5

    if-eqz v5, :cond_1

    move v1, v3

    .line 318
    .local v1, "emailFieldValidOrHidden":Z
    :goto_1
    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    .line 319
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->enableSendButton()V

    .line 323
    :goto_2
    return-void

    .end local v0    # "descriptionPresent":Z
    .end local v1    # "emailFieldValidOrHidden":Z
    .end local v2    # "imagesUploaded":Z
    :cond_0
    move v0, v4

    .line 307
    goto :goto_0

    .restart local v0    # "descriptionPresent":Z
    .restart local v2    # "imagesUploaded":Z
    :cond_1
    move v1, v4

    .line 313
    goto :goto_1

    .line 315
    :cond_2
    const/4 v1, 0x1

    .restart local v1    # "emailFieldValidOrHidden":Z
    goto :goto_1

    .line 321
    :cond_3
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->disableSendButton()V

    goto :goto_2
.end method

.method private static configurationToBundle(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "additionalInfo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 136
    .local p0, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 137
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "EXTRA_CONFIGURATION_SUBJECT"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v1, "EXTRA_CONFIGURATION_ADDITIONAL"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v2, "EXTRA_CONFIGURATION_TAGS"

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 140
    return-object v0

    .line 139
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private disableAttachmentButton()V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 328
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 330
    :cond_0
    return-void
.end method

.method private disableSendButton()V
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->doneMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->doneMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 433
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->doneMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 435
    :cond_0
    return-void
.end method

.method private displayAttachmentButton()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 334
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 335
    sget-object v1, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Ignoring displayAttachmentButton() because there is no network connection"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v0

    .line 340
    .local v0, "storedSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    if-eqz v0, :cond_0

    .line 345
    invoke-static {v0}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->isAttachmentSupportEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 346
    sget-object v1, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Ignoring displayAttachmentButton() because attachment support is disabled"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 350
    :cond_2
    sget-object v1, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->getBelvedere(Landroid/content/Context;)Lcom/zendesk/belvedere/Belvedere;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/belvedere/Belvedere;->oneOrMoreSourceAvailable()Z

    move-result v1

    if-nez v1, :cond_3

    .line 351
    sget-object v1, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Ignoring displayAttachmentButton() because we don\'t have permissions for the camera or gallery"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 355
    :cond_3
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentItem:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    .line 356
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentItem:Landroid/view/MenuItem;

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 357
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentItem:Landroid/view/MenuItem;

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 358
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentItem:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method

.method private enableSendButton()V
    .locals 3

    .prologue
    .line 439
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 440
    sget-object v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Ignoring enableSendButton() because there is no network connection"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->doneMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->doneMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 446
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->doneMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method

.method public static newInstance(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;
    .locals 6
    .param p0, "configuration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .prologue
    .line 115
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 117
    .local v1, "fragmentArgs":Landroid/os/Bundle;
    if-nez p0, :cond_0

    .line 118
    sget-object v2, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Cannot instantiate a ContactZendeskFragment with no configuration"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    const/4 v0, 0x0

    .line 132
    :goto_0
    return-object v0

    .line 122
    :cond_0
    const-string v2, "EXTRA_CONFIGURATION"

    .line 125
    invoke-interface {p0}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getTags()Ljava/util/List;

    move-result-object v3

    invoke-interface {p0}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getRequestSubject()Ljava/lang/String;

    move-result-object v4

    .line 126
    invoke-interface {p0}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v5

    .line 124
    invoke-static {v3, v4, v5}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->configurationToBundle(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 122
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 128
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-direct {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;-><init>()V

    .line 129
    .local v0, "contactZendeskFragment":Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;
    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->setArguments(Landroid/os/Bundle;)V

    .line 130
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->setRetainInstance(Z)V

    goto :goto_0
.end method

.method private preloadSettingsAndInit()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->settingsHelper:Lcom/zendesk/sdk/network/SettingsHelper;

    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->settingsCallback:Lcom/zendesk/service/SafeZendeskCallback;

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/SettingsHelper;->loadSetting(Lcom/zendesk/service/ZendeskCallback;)V

    .line 242
    return-void
.end method

.method private reinitializeFragment()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 628
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 631
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->reset()V

    .line 632
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->reset()V

    .line 635
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->isEmailFieldEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->isEmailFieldVisible:Z

    .line 638
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->descriptionEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 639
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->descriptionEditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 641
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->setEnabled(Z)V

    .line 642
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 645
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->checkSendButtonState()V

    .line 646
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->displayAttachmentButton()V

    .line 647
    return-void
.end method

.method private sendFeedback()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 373
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->feedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    if-eqz v1, :cond_0

    .line 374
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->feedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    invoke-interface {v1}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionStarted()V

    .line 376
    :cond_0
    iget-boolean v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->isEmailFieldVisible:Z

    if-eqz v1, :cond_1

    .line 377
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->updateIdentity()V

    .line 380
    :cond_1
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->contactZendeskFeedbackConfiguration:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;

    if-nez v1, :cond_3

    .line 381
    sget-object v1, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Configuration is null, cannot send feedback..."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 383
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->feedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    if-eqz v1, :cond_2

    .line 384
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->feedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    new-instance v2, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v3, "Configuration is null, cannot send feedback..."

    invoke-direct {v2, v3}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionError(Lcom/zendesk/service/ErrorResponse;)V

    .line 402
    :cond_2
    :goto_0
    return-void

    .line 390
    :cond_3
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 391
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->descriptionEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 392
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    invoke-virtual {v1, v3}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->setEnabled(Z)V

    .line 393
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->disableSendButton()V

    .line 394
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    invoke-virtual {v1, v3}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentsDeletable(Z)V

    .line 397
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->contactZendeskFeedbackConfiguration:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;

    sget-object v3, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 399
    invoke-virtual {v3}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getContactZendeskTags()Ljava/util/List;

    move-result-object v3

    .line 396
    invoke-static {v1, v2, v3}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->defaultConnector(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;Ljava/util/List;)Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;

    move-result-object v0

    .line 401
    .local v0, "feedbackConnector":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->descriptionEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v2}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->getUploadTokens()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->requestCallback:Lcom/zendesk/service/SafeZendeskCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->sendFeedback(Ljava/lang/String;Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method private setUpCallbacks()V
    .locals 1

    .prologue
    .line 487
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    invoke-static {v0}, Lcom/zendesk/service/SafeZendeskCallback;->from(Lcom/zendesk/service/ZendeskCallback;)Lcom/zendesk/service/SafeZendeskCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->requestCallback:Lcom/zendesk/service/SafeZendeskCallback;

    .line 488
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$4;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$4;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imagesExtracted:Lcom/zendesk/belvedere/BelvedereCallback;

    .line 498
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$5;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$5;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

    .line 505
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$6;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$6;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadProgressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    .line 524
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$7;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    invoke-static {v0}, Lcom/zendesk/service/SafeZendeskCallback;->from(Lcom/zendesk/service/ZendeskCallback;)Lcom/zendesk/service/SafeZendeskCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->settingsCallback:Lcom/zendesk/service/SafeZendeskCallback;

    .line 571
    return-void
.end method

.method private tearDownCallbacks()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 472
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->requestCallback:Lcom/zendesk/service/SafeZendeskCallback;

    invoke-virtual {v0}, Lcom/zendesk/service/SafeZendeskCallback;->cancel()V

    .line 473
    iput-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->requestCallback:Lcom/zendesk/service/SafeZendeskCallback;

    .line 475
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imagesExtracted:Lcom/zendesk/belvedere/BelvedereCallback;

    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereCallback;->cancel()V

    .line 476
    iput-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imagesExtracted:Lcom/zendesk/belvedere/BelvedereCallback;

    .line 478
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentContainerListener(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;)V

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->settingsCallback:Lcom/zendesk/service/SafeZendeskCallback;

    invoke-virtual {v0}, Lcom/zendesk/service/SafeZendeskCallback;->cancel()V

    .line 483
    iput-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->settingsCallback:Lcom/zendesk/service/SafeZendeskCallback;

    .line 484
    return-void
.end method

.method private updateIdentity()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 405
    sget-object v4, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Updating existing anonymous identity with an email address"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 406
    sget-object v4, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v4

    invoke-interface {v4}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v4

    invoke-interface {v4}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v2

    .line 407
    .local v2, "identity":Lcom/zendesk/sdk/model/access/Identity;
    iget-object v4, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 409
    .local v1, "emailAddress":Ljava/lang/String;
    if-eqz v2, :cond_0

    instance-of v4, v2, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-nez v4, :cond_1

    .line 410
    :cond_0
    sget-object v4, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "No valid identity found "

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 428
    :goto_0
    return-void

    :cond_1
    move-object v0, v2

    .line 414
    check-cast v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 416
    .local v0, "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    new-instance v3, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    invoke-direct {v3}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;-><init>()V

    .line 417
    .local v3, "newIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;
    invoke-virtual {v3, v1}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->withEmailIdentifier(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    .line 419
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 420
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->withNameIdentifier(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    .line 423
    :cond_2
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getExternalId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 424
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getExternalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->withExternalIdentifier(Ljava/lang/String;)Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;

    .line 427
    :cond_3
    sget-object v4, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/access/AnonymousIdentity$Builder;->build()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->setIdentity(Lcom/zendesk/sdk/model/access/Identity;)V

    goto :goto_0
.end method


# virtual methods
.method public deleteUnusedAttachmentsBeforeShutdown()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->deleteAllAttachmentsBeforeShutdown()V

    .line 302
    :cond_0
    return-void
.end method

.method isEmailFieldEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z
    .locals 8
    .param p1, "safeSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    const/4 v4, 0x0

    .line 592
    if-nez p1, :cond_1

    .line 623
    :cond_0
    :goto_0
    return v4

    .line 597
    :cond_1
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isConversationsEnabled()Z

    move-result v1

    .line 598
    .local v1, "conversationsEnabled":Z
    if-nez v1, :cond_0

    .line 603
    sget-object v5, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v5}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v5

    invoke-interface {v5}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v5

    invoke-interface {v5}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v3

    .line 605
    .local v3, "identity":Lcom/zendesk/sdk/model/access/Identity;
    if-eqz v3, :cond_0

    instance-of v5, v3, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-eqz v5, :cond_0

    move-object v0, v3

    .line 610
    check-cast v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 611
    .local v0, "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getEmail()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 616
    sget-object v5, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v5}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isCoppaEnabled()Z

    move-result v2

    .line 617
    .local v2, "coppaEnabled":Z
    if-eqz v2, :cond_2

    .line 618
    sget-object v5, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v6, "This ticket will not be viewable by the user after submission. Conversations are not supported and COPPA removes any personal information associated with the ticket"

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 623
    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method

.method networkNotAvailable()V
    .locals 2

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 452
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    if-eqz v1, :cond_0

    .line 453
    check-cast v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->onNetworkUnavailable()V

    .line 455
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 288
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 289
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->disableSendButton()V

    .line 290
    sget-object v1, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->getBelvedere(Landroid/content/Context;)Lcom/zendesk/belvedere/Belvedere;

    move-result-object v0

    .line 291
    .local v0, "belvedere":Lcom/zendesk/belvedere/Belvedere;
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imagesExtracted:Lcom/zendesk/belvedere/BelvedereCallback;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/zendesk/belvedere/Belvedere;->getFilesFromActivityOnResult(IILandroid/content/Intent;Lcom/zendesk/belvedere/BelvedereCallback;)V

    .line 292
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 166
    instance-of v0, p1, Lcom/zendesk/sdk/network/Retryable;

    if-eqz v0, :cond_0

    .line 167
    check-cast p1, Lcom/zendesk/sdk/network/Retryable;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->retryable:Lcom/zendesk/sdk/network/Retryable;

    .line 170
    :cond_0
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->setUpCallbacks()V

    .line 171
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 176
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->setHasOptionsMenu(Z)V

    .line 177
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/impl/ProviderStore;->uiSettingsHelper()Lcom/zendesk/sdk/network/SettingsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->settingsHelper:Lcom/zendesk/sdk/network/SettingsHelper;

    .line 179
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "EXTRA_CONFIGURATION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;

    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "EXTRA_CONFIGURATION"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->bundleToZendeskFeedbackConfiguration(Landroid/os/Bundle;)Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->contactZendeskFeedbackConfiguration:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$ContactZendeskFeedbackConfiguration;

    .line 185
    :goto_0
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    if-nez v0, :cond_1

    .line 186
    new-instance v0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadProgressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/zendesk/sdk/network/impl/ProviderStore;->uploadProvider()Lcom/zendesk/sdk/network/UploadProvider;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;-><init>(Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;Lcom/zendesk/sdk/network/UploadProvider;)V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .line 190
    :goto_1
    return-void

    .line 182
    :cond_0
    sget-object v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "No configuration passed to the fragment, this will result in no feedback being sent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadProgressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->setImageUploadProgressListener(Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 246
    sget v0, Lcom/zendesk/sdk/R$menu;->fragment_contact_zendesk_menu:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 248
    sget v0, Lcom/zendesk/sdk/R$id;->fragment_contact_zendesk_menu_done:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->doneMenuItem:Landroid/view/MenuItem;

    .line 249
    sget v0, Lcom/zendesk/sdk/R$id;->fragment_contact_zendesk_attachment:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentItem:Landroid/view/MenuItem;

    .line 251
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 252
    :cond_0
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->disableSendButton()V

    .line 253
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->disableAttachmentButton()V

    .line 255
    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 196
    sget-object v4, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v4

    invoke-interface {v4}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v4

    invoke-interface {v4}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    .line 200
    .local v2, "hasIdentity":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 202
    sget v4, Lcom/zendesk/sdk/R$layout;->fragment_contact_zendesk:I

    invoke-virtual {p1, v4, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 204
    .local v1, "fragmentView":Landroid/view/View;
    sget v3, Lcom/zendesk/sdk/R$id;->contact_fragment_attachments:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    iput-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    .line 205
    iget-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    iget-object v4, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v3, v4}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setState(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)V

    .line 206
    iget-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    iget-object v4, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->attachmentContainerListener:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;

    invoke-virtual {v3, v4}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentContainerListener(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;)V

    .line 208
    sget v3, Lcom/zendesk/sdk/R$id;->contact_fragment_email:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    iput-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    .line 209
    iget-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->emailEditText:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    new-instance v4, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$2;

    invoke-direct {v4, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$2;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    invoke-virtual {v3, v4}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 216
    sget v3, Lcom/zendesk/sdk/R$id;->contact_fragment_description:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->descriptionEditText:Landroid/widget/EditText;

    .line 217
    iget-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->descriptionEditText:Landroid/widget/EditText;

    new-instance v4, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$3;

    invoke-direct {v4, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$3;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 224
    sget v3, Lcom/zendesk/sdk/R$id;->contact_fragment_progress:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 225
    sget v3, Lcom/zendesk/sdk/R$id;->contact_fragment_container:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->container:Landroid/view/ViewGroup;

    .line 227
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->preloadSettingsAndInit()V

    .line 236
    :goto_1
    return-object v1

    .end local v1    # "fragmentView":Landroid/view/View;
    .end local v2    # "hasIdentity":Z
    :cond_0
    move v2, v3

    .line 196
    goto :goto_0

    .line 230
    .restart local v2    # "hasIdentity":Z
    :cond_1
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 231
    .local v0, "errorView":Landroid/widget/TextView;
    const-string v3, "Error, please check that an identity or email address has been set"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    move-object v1, v0

    .restart local v1    # "fragmentView":Landroid/view/View;
    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 575
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 576
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->imageUploadService:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->setImageUploadProgressListener(Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;)V

    .line 577
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 281
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->retryable:Lcom/zendesk/sdk/network/Retryable;

    .line 283
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->tearDownCallbacks()V

    .line 284
    return-void
.end method

.method onNetworkAvailable()V
    .locals 1

    .prologue
    .line 458
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->checkSendButtonState()V

    .line 459
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->displayAttachmentButton()V

    .line 461
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    if-nez v0, :cond_0

    .line 462
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->preloadSettingsAndInit()V

    .line 464
    :cond_0
    return-void
.end method

.method onNetworkUnavailable()V
    .locals 0

    .prologue
    .line 467
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->disableAttachmentButton()V

    .line 468
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->disableSendButton()V

    .line 469
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 266
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/zendesk/sdk/R$id;->fragment_contact_zendesk_menu_done:I

    if-ne v2, v3, :cond_0

    .line 267
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->sendFeedback()V

    .line 276
    :goto_0
    return v1

    .line 270
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/zendesk/sdk/R$id;->fragment_contact_zendesk_attachment:I

    if-ne v2, v3, :cond_1

    .line 271
    sget-object v2, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->getBelvedere(Landroid/content/Context;)Lcom/zendesk/belvedere/Belvedere;

    move-result-object v0

    .line 272
    .local v0, "belvedere":Lcom/zendesk/belvedere/Belvedere;
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/zendesk/belvedere/Belvedere;->showDialog(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    .line 276
    .end local v0    # "belvedere":Lcom/zendesk/belvedere/Belvedere;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 259
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 260
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->checkSendButtonState()V

    .line 261
    return-void
.end method

.method public setFeedbackListener(Lcom/zendesk/sdk/network/SubmissionListener;)V
    .locals 0
    .param p1, "feedbackListener"    # Lcom/zendesk/sdk/network/SubmissionListener;

    .prologue
    .line 368
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->feedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    .line 369
    return-void
.end method
