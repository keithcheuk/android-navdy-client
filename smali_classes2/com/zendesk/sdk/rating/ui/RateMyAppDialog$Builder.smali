.class public Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
.super Ljava/lang/Object;
.source "RateMyAppDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mActivity:Landroid/support/v4/app/FragmentActivity;

.field private final mButtons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/zendesk/sdk/rating/RateMyAppButton;",
            ">;"
        }
    .end annotation
.end field

.field private mOnShowListener:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

.field private mRules:[Lcom/zendesk/sdk/rating/RateMyAppRule;

.field private mSelectionListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;)V
    .locals 3
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mButtons:Ljava/util/ArrayList;

    .line 372
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    .line 374
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    if-nez v0, :cond_0

    .line 375
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Activity is null, many things will not work"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 378
    :cond_0
    return-void
.end method

.method private ensureSaneRules(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;)V
    .locals 4
    .param p1, "dialog"    # Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;

    .prologue
    .line 580
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mRules:[Lcom/zendesk/sdk/rating/RateMyAppRule;

    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty([Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581
    new-instance v1, Lcom/zendesk/sdk/rating/impl/RateMyAppRules;

    iget-object v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mRules:[Lcom/zendesk/sdk/rating/RateMyAppRule;

    invoke-direct {v1, v2}, Lcom/zendesk/sdk/rating/impl/RateMyAppRules;-><init>([Lcom/zendesk/sdk/rating/RateMyAppRule;)V

    invoke-virtual {p1, v1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->setRateMyAppRules(Lcom/zendesk/sdk/rating/impl/RateMyAppRules;)V

    .line 593
    :goto_0
    return-void

    .line 585
    :cond_0
    const/4 v1, 0x4

    new-array v0, v1, [Lcom/zendesk/sdk/rating/RateMyAppRule;

    const/4 v1, 0x0

    new-instance v2, Lcom/zendesk/sdk/rating/impl/SettingsRateMyAppRule;

    iget-object v3, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v2, v3}, Lcom/zendesk/sdk/rating/impl/SettingsRateMyAppRule;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/zendesk/sdk/rating/impl/MetricsRateMyAppRule;

    iget-object v3, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v2, v3}, Lcom/zendesk/sdk/rating/impl/MetricsRateMyAppRule;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/zendesk/sdk/rating/impl/NetworkRateMyAppRule;

    iget-object v3, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v2, v3}, Lcom/zendesk/sdk/rating/impl/NetworkRateMyAppRule;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/zendesk/sdk/rating/impl/IdentityRateMyAppRule;

    iget-object v3, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v2, v3}, Lcom/zendesk/sdk/rating/impl/IdentityRateMyAppRule;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, v1

    .line 592
    .local v0, "defaultRules":[Lcom/zendesk/sdk/rating/RateMyAppRule;
    new-instance v1, Lcom/zendesk/sdk/rating/impl/RateMyAppRules;

    invoke-direct {v1, v0}, Lcom/zendesk/sdk/rating/impl/RateMyAppRules;-><init>([Lcom/zendesk/sdk/rating/RateMyAppRule;)V

    invoke-virtual {p1, v1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->setRateMyAppRules(Lcom/zendesk/sdk/rating/impl/RateMyAppRules;)V

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;
    .locals 2

    .prologue
    .line 550
    new-instance v0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;

    invoke-direct {v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;-><init>()V

    .line 552
    .local v0, "dialog":Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mOnShowListener:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->setRateMyAppOnShowListener(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;)V

    .line 553
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mSelectionListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->setRateMyAppOnSelectionListener(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;)V

    .line 554
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mButtons:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->setRateMyAppButtonList(Ljava/util/List;)V

    .line 556
    invoke-direct {p0, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->ensureSaneRules(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;)V

    .line 558
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->access$302(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;Z)Z

    .line 559
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->setRetainInstance(Z)V

    .line 561
    return-object v0
.end method

.method public withAndroidStoreRatingButton()Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
    .locals 3

    .prologue
    .line 415
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mButtons:Ljava/util/ArrayList;

    new-instance v1, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;

    iget-object v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v1, v2}, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    :cond_0
    return-object p0
.end method

.method public withButton(Lcom/zendesk/sdk/rating/RateMyAppButton;)Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
    .locals 1
    .param p1, "button"    # Lcom/zendesk/sdk/rating/RateMyAppButton;

    .prologue
    .line 392
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 393
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mButtons:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    :cond_0
    return-object p0
.end method

.method public withDontRemindMeAgainButton()Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
    .locals 3

    .prologue
    .line 433
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mButtons:Ljava/util/ArrayList;

    new-instance v1, Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;

    iget-object v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v1, v2}, Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 437
    :cond_0
    return-object p0
.end method

.method public withOnShowListener(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;)Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
    .locals 0
    .param p1, "onShowListener"    # Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

    .prologue
    .line 524
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mOnShowListener:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

    .line 525
    return-object p0
.end method

.method public varargs withRules([Lcom/zendesk/sdk/rating/RateMyAppRule;)Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
    .locals 0
    .param p1, "rules"    # [Lcom/zendesk/sdk/rating/RateMyAppRule;

    .prologue
    .line 539
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mRules:[Lcom/zendesk/sdk/rating/RateMyAppRule;

    .line 540
    return-object p0
.end method

.method public withSelectionListener(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;)Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
    .locals 0
    .param p1, "selectionListener"    # Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    .prologue
    .line 512
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mSelectionListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    .line 513
    return-object p0
.end method

.method public withSendFeedbackButton()Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
    .locals 2

    .prologue
    .line 446
    new-instance v0, Lcom/zendesk/sdk/rating/ui/DefaultContactConfiguration;

    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/rating/ui/DefaultContactConfiguration;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/zendesk/sdk/network/SubmissionListenerAdapter;

    invoke-direct {v1}, Lcom/zendesk/sdk/network/SubmissionListenerAdapter;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->withSendFeedbackButton(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;Lcom/zendesk/sdk/network/SubmissionListener;)Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public withSendFeedbackButton(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
    .locals 1
    .param p1, "feedbackConfiguration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .prologue
    .line 463
    new-instance v0, Lcom/zendesk/sdk/network/SubmissionListenerAdapter;

    invoke-direct {v0}, Lcom/zendesk/sdk/network/SubmissionListenerAdapter;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->withSendFeedbackButton(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;Lcom/zendesk/sdk/network/SubmissionListener;)Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public withSendFeedbackButton(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;Lcom/zendesk/sdk/network/SubmissionListener;)Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;
    .locals 5
    .param p1, "feedbackConfiguration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    .param p2, "listener"    # Lcom/zendesk/sdk/network/SubmissionListener;

    .prologue
    .line 488
    iget-object v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_0

    .line 490
    iget-object v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    new-instance v3, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;

    invoke-direct {v3, p1}, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;-><init>(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V

    sget-object v4, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 492
    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppTags()Ljava/util/List;

    move-result-object v4

    .line 490
    invoke-static {v2, v3, v4}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->defaultConnector(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;Ljava/util/List;)Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;

    move-result-object v0

    .line 494
    .local v0, "connector":Lcom/zendesk/sdk/feedback/FeedbackConnector;
    invoke-interface {v0}, Lcom/zendesk/sdk/feedback/FeedbackConnector;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 495
    new-instance v1, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;

    iget-object v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v1, v2, v0}, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/zendesk/sdk/feedback/FeedbackConnector;)V

    .line 496
    .local v1, "feedbackButton":Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;
    invoke-virtual {v1, p2}, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->setFeedbackListener(Lcom/zendesk/sdk/network/SubmissionListener;)V

    .line 497
    iget-object v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;->mButtons:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    .end local v0    # "connector":Lcom/zendesk/sdk/feedback/FeedbackConnector;
    .end local v1    # "feedbackButton":Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;
    :cond_0
    return-object p0
.end method
