.class Lcom/zendesk/sdk/rating/ui/FeedbackDialog$1;
.super Ljava/lang/Object;
.source "FeedbackDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$1;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 99
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cancel button was clicked..."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$1;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-virtual {v0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->dismiss()V

    .line 103
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$1;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-static {v0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$100(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Notifying feedback listener of submission cancellation"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$1;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-static {v0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$100(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionCancel()V

    .line 107
    :cond_0
    return-void
.end method
