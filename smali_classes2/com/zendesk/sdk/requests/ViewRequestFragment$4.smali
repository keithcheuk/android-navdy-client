.class Lcom/zendesk/sdk/requests/ViewRequestFragment$4;
.super Ljava/lang/Object;
.source "ViewRequestFragment.java"

# interfaces
.implements Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/ViewRequestFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$4;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public allImagesUploaded(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/io/File;",
            "Lcom/zendesk/sdk/model/request/UploadResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 221
    .local p1, "responses":Ljava/util/Map;, "Ljava/util/Map<Ljava/io/File;Lcom/zendesk/sdk/model/request/UploadResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$4;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$000(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V

    .line 222
    return-void
.end method

.method public imageUploadError(Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/belvedere/BelvedereResult;)V
    .locals 3
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;
    .param p2, "file"    # Lcom/zendesk/belvedere/BelvedereResult;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$4;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$4;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$300(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$4;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$200(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    move-result-object v2

    invoke-static {v0, p2, p1, v1, v2}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->showAttachmentTryAgainDialog(Landroid/content/Context;Lcom/zendesk/belvedere/BelvedereResult;Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/attachment/ImageUploadHelper;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;)V

    .line 232
    return-void
.end method

.method public imageUploaded(Lcom/zendesk/sdk/model/request/UploadResponse;Lcom/zendesk/belvedere/BelvedereResult;)V
    .locals 2
    .param p1, "uploadResponse"    # Lcom/zendesk/sdk/model/request/UploadResponse;
    .param p2, "file"    # Lcom/zendesk/belvedere/BelvedereResult;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$4;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$200(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    move-result-object v0

    invoke-virtual {p2}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentUploaded(Ljava/io/File;)V

    .line 227
    return-void
.end method
