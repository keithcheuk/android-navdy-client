.class Lcom/zendesk/sdk/requests/ViewRequestFragment$9;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ViewRequestFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/ViewRequestFragment;->setupCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 398
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 2
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$400(Lcom/zendesk/sdk/requests/ViewRequestFragment;Z)V

    .line 421
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    sget-object v1, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->LOAD_IMAGE:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    invoke-static {v0, p1, v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$700(Lcom/zendesk/sdk/requests/ViewRequestFragment;Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;)V

    .line 422
    return-void
.end method

.method public onSuccess(Landroid/net/Uri;)V
    .locals 5
    .param p1, "result"    # Landroid/net/Uri;

    .prologue
    .line 401
    iget-object v3, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$400(Lcom/zendesk/sdk/requests/ViewRequestFragment;Z)V

    .line 403
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 404
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    const-string v3, "image/*"

    invoke-virtual {v2, p1, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 407
    sget-object v3, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->INSTANCE:Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;

    iget-object v4, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-virtual {v4}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/zendesk/sdk/attachment/ZendeskBelvedereProvider;->getBelvedere(Landroid/content/Context;)Lcom/zendesk/belvedere/Belvedere;

    move-result-object v0

    .line 408
    .local v0, "belvedere":Lcom/zendesk/belvedere/Belvedere;
    invoke-virtual {v0, v2, p1}, Lcom/zendesk/belvedere/Belvedere;->grantPermissionsForUri(Landroid/content/Intent;Landroid/net/Uri;)V

    .line 411
    :try_start_0
    iget-object v3, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-virtual {v3, v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 416
    :goto_0
    return-void

    .line 412
    :catch_0
    move-exception v1

    .line 414
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v4, "Couldn\'t start activity to view attachment"

    invoke-direct {v3, v4}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;->onError(Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 398
    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/ViewRequestFragment$9;->onSuccess(Landroid/net/Uri;)V

    return-void
.end method
