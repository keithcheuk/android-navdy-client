.class public Lcom/zendesk/sdk/model/settings/AccountSettings;
.super Ljava/lang/Object;
.source "AccountSettings.java"


# instance fields
.field private attachments:Lcom/zendesk/sdk/model/settings/AttachmentSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAttachmentSettings()Lcom/zendesk/sdk/model/settings/AttachmentSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/AccountSettings;->attachments:Lcom/zendesk/sdk/model/settings/AttachmentSettings;

    return-object v0
.end method
