.class public Lcom/zendesk/sdk/model/helpcenter/LastSearch;
.super Ljava/lang/Object;
.source "LastSearch.java"


# instance fields
.field private final origin:Ljava/lang/String;

.field private final query:Ljava/lang/String;

.field private final resultsCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "resultCount"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "mobile_sdk"

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/LastSearch;->origin:Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/LastSearch;->query:Ljava/lang/String;

    .line 24
    iput p2, p0, Lcom/zendesk/sdk/model/helpcenter/LastSearch;->resultsCount:I

    .line 25
    return-void
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/LastSearch;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getResultsCount()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/zendesk/sdk/model/helpcenter/LastSearch;->resultsCount:I

    return v0
.end method
