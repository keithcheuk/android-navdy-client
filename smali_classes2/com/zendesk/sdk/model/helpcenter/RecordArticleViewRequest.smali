.class public Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;
.super Ljava/lang/Object;
.source "RecordArticleViewRequest.java"


# instance fields
.field private lastSearch:Lcom/zendesk/sdk/model/helpcenter/LastSearch;

.field private uniqueSearchResultClick:Z


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/model/helpcenter/LastSearch;Z)V
    .locals 0
    .param p1, "lastSearch"    # Lcom/zendesk/sdk/model/helpcenter/LastSearch;
    .param p2, "uniqueSearchResultClick"    # Z

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;->lastSearch:Lcom/zendesk/sdk/model/helpcenter/LastSearch;

    .line 33
    iput-boolean p2, p0, Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;->uniqueSearchResultClick:Z

    .line 34
    return-void
.end method
