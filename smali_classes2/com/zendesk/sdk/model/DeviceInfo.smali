.class public Lcom/zendesk/sdk/model/DeviceInfo;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# static fields
.field private static final DEVICE_INFO_API_VERSION:Ljava/lang/String; = "device_api"

.field private static final DEVICE_INFO_BATTERY_OK:Ljava/lang/String; = "device_battery_ok"

.field private static final DEVICE_INFO_DEVICE_NAME:Ljava/lang/String; = "device_name"

.field private static final DEVICE_INFO_LOW_MEMORY:Ljava/lang/String; = "device_low_memory"

.field private static final DEVICE_INFO_MANUFACTURER:Ljava/lang/String; = "device_manufacturer"

.field private static final DEVICE_INFO_MODEL_TYPE:Ljava/lang/String; = "device_model"

.field private static final DEVICE_INFO_OS_VERSION:Ljava/lang/String; = "device_os"

.field private static final DEVICE_INFO_TOTAL_MEMORY:Ljava/lang/String; = "device_total_memory"

.field private static final DEVICE_INFO_USED_MEMORY:Ljava/lang/String; = "device_used_memory"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mModelDeviceName:Ljava/lang/String;

.field private mModelManufacturer:Ljava/lang/String;

.field private mModelName:Ljava/lang/String;

.field private mVersionCode:I

.field private mVersionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mContext:Landroid/content/Context;

    .line 47
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mModelName:Ljava/lang/String;

    .line 48
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mVersionName:Ljava/lang/String;

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mVersionCode:I

    .line 50
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mModelManufacturer:Ljava/lang/String;

    .line 51
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mModelDeviceName:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public getDeviceInfoAsMap()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 152
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "device_os"

    invoke-virtual {p0}, Lcom/zendesk/sdk/model/DeviceInfo;->getVersionName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    const-string v3, "device_api"

    invoke-virtual {p0}, Lcom/zendesk/sdk/model/DeviceInfo;->getVersionCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    const-string v3, "device_model"

    invoke-virtual {p0}, Lcom/zendesk/sdk/model/DeviceInfo;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    const-string v3, "device_name"

    invoke-virtual {p0}, Lcom/zendesk/sdk/model/DeviceInfo;->getModelDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    const-string v3, "device_manufacturer"

    invoke-virtual {p0}, Lcom/zendesk/sdk/model/DeviceInfo;->getModelManufacturer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    new-instance v1, Lcom/zendesk/sdk/model/MemoryInformation;

    iget-object v3, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/zendesk/sdk/model/MemoryInformation;-><init>(Landroid/content/Context;)V

    .line 160
    .local v1, "memoryInformation":Lcom/zendesk/sdk/model/MemoryInformation;
    const-string v3, "device_total_memory"

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/MemoryInformation;->getTotalMemory()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    const-string v3, "device_used_memory"

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/MemoryInformation;->getUsedMemory()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    const-string v3, "device_low_memory"

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/MemoryInformation;->isLowMemory()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v3, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/zendesk/sdk/power/PowerConfig;->getInstance(Landroid/content/Context;)Lcom/zendesk/sdk/power/PowerConfig;

    move-result-object v2

    .line 165
    .local v2, "powerConfig":Lcom/zendesk/sdk/power/PowerConfig;
    const-string v3, "device_battery_ok"

    invoke-virtual {v2}, Lcom/zendesk/sdk/power/PowerConfig;->isBatteryOk()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    return-object v0
.end method

.method public getModelDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mModelDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getModelManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mModelManufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mModelName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mVersionCode:I

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mVersionName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 105
    .local v2, "sb":Ljava/lang/StringBuilder;
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v4, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mContext:Landroid/content/Context;

    sget v5, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_device_os_version:I

    .line 108
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    .line 109
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/DeviceInfo;->getVersionName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 106
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "currentLine":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v4, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mContext:Landroid/content/Context;

    sget v5, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_device_api_version:I

    .line 116
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    .line 117
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/DeviceInfo;->getVersionCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 114
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v4, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mContext:Landroid/content/Context;

    sget v5, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_device_model:I

    .line 124
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    .line 125
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/DeviceInfo;->getModelName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 122
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v4, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mContext:Landroid/content/Context;

    sget v5, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_device_name:I

    .line 132
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    .line 133
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/DeviceInfo;->getModelDeviceName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 130
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    new-instance v1, Lcom/zendesk/sdk/model/MemoryInformation;

    iget-object v3, p0, Lcom/zendesk/sdk/model/DeviceInfo;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/zendesk/sdk/model/MemoryInformation;-><init>(Landroid/content/Context;)V

    .line 139
    .local v1, "memoryInformation":Lcom/zendesk/sdk/model/MemoryInformation;
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/MemoryInformation;->formatMemoryUsage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
