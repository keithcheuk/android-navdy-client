.class public Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;
.super Ljava/lang/Object;
.source "ActionRefreshComments.java"

# interfaces
.implements Lcom/zendesk/sdk/deeplinking/actions/ActionData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActionRefreshCommentsData"
.end annotation


# instance fields
.field private final requestId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;->requestId:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public getRequestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/actions/ActionRefreshComments$ActionRefreshCommentsData;->requestId:Ljava/lang/String;

    return-object v0
.end method
