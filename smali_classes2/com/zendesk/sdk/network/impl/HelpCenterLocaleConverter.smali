.class Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;
.super Ljava/lang/Object;
.source "HelpCenterLocaleConverter.java"


# static fields
.field private static final forwardLookupMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;->forwardLookupMap:Ljava/util/Map;

    .line 34
    sget-object v0, Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;->forwardLookupMap:Ljava/util/Map;

    const-string v1, "iw"

    const-string v2, "he"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;->forwardLookupMap:Ljava/util/Map;

    const-string v1, "nb"

    const-string v2, "no"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;->forwardLookupMap:Ljava/util/Map;

    const-string v1, "in"

    const-string v2, "id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;->forwardLookupMap:Ljava/util/Map;

    const-string v1, "ji"

    const-string v2, "yi"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method toHelpCenterLocaleString(Ljava/util/Locale;)Ljava/lang/String;
    .locals 6
    .param p1, "locale"    # Ljava/util/Locale;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 49
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    .line 51
    .local v1, "isUsableLocale":Z
    :goto_0
    if-eqz v1, :cond_2

    move-object v0, p1

    .line 53
    .local v0, "deviceLocale":Ljava/util/Locale;
    :goto_1
    sget-object v4, Lcom/zendesk/sdk/network/impl/HelpCenterLocaleConverter;->forwardLookupMap:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 55
    .local v3, "remappedLanguage":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    .line 56
    invoke-static {v3}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 58
    .end local v3    # "remappedLanguage":Ljava/lang/String;
    :goto_2
    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 61
    .local v2, "localeBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 62
    const-string v4, "-"

    .line 63
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 64
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 49
    .end local v0    # "deviceLocale":Ljava/util/Locale;
    .end local v1    # "isUsableLocale":Z
    .end local v2    # "localeBuilder":Ljava/lang/StringBuilder;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 51
    .restart local v1    # "isUsableLocale":Z
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    goto :goto_1

    .line 58
    .restart local v0    # "deviceLocale":Ljava/util/Locale;
    .restart local v3    # "remappedLanguage":Ljava/lang/String;
    :cond_3
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method
