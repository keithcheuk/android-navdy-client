.class Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ZendeskNetworkInfoProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NetworkAvailabilityBroadcastReceiver"
.end annotation


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;)V
    .locals 1
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 234
    const-class v0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;->LOG_TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 239
    if-eqz p2, :cond_0

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 240
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onReceive: intent was null or getAction() was mismatched"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    :goto_0
    return-void

    .line 244
    :cond_1
    const-string v1, "noConnectivity"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 246
    .local v0, "noConnection":Z
    if-eqz v0, :cond_2

    .line 247
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->onNetworkUnavailable()V

    goto :goto_0

    .line 249
    :cond_2
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->onNetworkAvailable()V

    goto :goto_0
.end method
