.class Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;
.super Ljava/lang/Object;
.source "ZendeskPushRegistrationService.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskPushRegistrationService"


# instance fields
.field private final pushService:Lcom/zendesk/sdk/network/PushRegistrationService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/PushRegistrationService;)V
    .locals 0
    .param p1, "pushRegistrationService"    # Lcom/zendesk/sdk/network/PushRegistrationService;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;->pushService:Lcom/zendesk/sdk/network/PushRegistrationService;

    .line 26
    return-void
.end method


# virtual methods
.method public registerDevice(Ljava/lang/String;Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "authorization"    # Ljava/lang/String;
    .param p2, "registerPushResponseWrapper"    # Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;->pushService:Lcom/zendesk/sdk/network/PushRegistrationService;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/PushRegistrationService;->registerDevice(Ljava/lang/String;Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 37
    return-void
.end method

.method public unregisterDevice(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "authorization"    # Ljava/lang/String;
    .param p2, "identifier"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;->pushService:Lcom/zendesk/sdk/network/PushRegistrationService;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/PushRegistrationService;->unregisterDevice(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 48
    return-void
.end method
