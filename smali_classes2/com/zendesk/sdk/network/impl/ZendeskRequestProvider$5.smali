.class Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$5;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskRequestProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->addCommentInternal(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/EndUserComment;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/Request;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$5;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/request/Request;)V
    .locals 4
    .param p1, "result"    # Lcom/zendesk/sdk/model/request/Request;

    .prologue
    .line 237
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getCommentCount()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 238
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$5;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    invoke-static {v1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;)Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getCommentCount()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/zendesk/sdk/storage/RequestStorage;->setCommentCount(Ljava/lang/String;I)V

    .line 244
    :goto_0
    new-instance v0, Lcom/zendesk/sdk/model/request/Comment;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/request/Comment;-><init>()V

    .line 245
    .local v0, "comment":Lcom/zendesk/sdk/model/request/Comment;
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getRequesterId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/model/request/Comment;->setAuthorId(Ljava/lang/Long;)V

    .line 246
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/model/request/Comment;->setCreatedAt(Ljava/util/Date;)V

    .line 247
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v1, v0}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 250
    :cond_0
    return-void

    .line 240
    .end local v0    # "comment":Lcom/zendesk/sdk/model/request/Comment;
    :cond_1
    const-string v1, "ZendeskRequestProvider"

    const-string v2, "The ID and / or comment count was missing. Cannot store comment count."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 232
    check-cast p1, Lcom/zendesk/sdk/model/request/Request;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$5;->onSuccess(Lcom/zendesk/sdk/model/request/Request;)V

    return-void
.end method
