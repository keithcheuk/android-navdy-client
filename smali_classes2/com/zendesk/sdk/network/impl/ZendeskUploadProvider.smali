.class Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;
.super Ljava/lang/Object;
.source "ZendeskUploadProvider.java"

# interfaces
.implements Lcom/zendesk/sdk/network/UploadProvider;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskUploadProvider"


# instance fields
.field private final baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

.field private final uploadService:Lcom/zendesk/sdk/network/impl/ZendeskUploadService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskUploadService;)V
    .locals 0
    .param p1, "baseProvider"    # Lcom/zendesk/sdk/network/BaseProvider;
    .param p2, "uploadService"    # Lcom/zendesk/sdk/network/impl/ZendeskUploadService;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    .line 31
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;->uploadService:Lcom/zendesk/sdk/network/impl/ZendeskUploadService;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;)Lcom/zendesk/sdk/network/impl/ZendeskUploadService;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;->uploadService:Lcom/zendesk/sdk/network/impl/ZendeskUploadService;

    return-object v0
.end method


# virtual methods
.method public deleteAttachment(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "token"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;

    invoke-direct {v1, p0, p2, p1, p2}, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$2;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 84
    return-void
.end method

.method public uploadAttachment(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 8
    .param p1, "fileName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "file"    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "mimeType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/UploadResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/UploadResponse;>;"
    iget-object v7, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v7, v0}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 58
    return-void
.end method
