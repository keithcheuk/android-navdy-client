.class public Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;
.super Ljava/lang/Object;
.source "RectangleIntersects.java"


# instance fields
.field private rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

.field private rectangle:Lcom/vividsolutions/jts/geom/Polygon;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 1
    .param p1, "rectangle"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->rectangle:Lcom/vividsolutions/jts/geom/Polygon;

    .line 86
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 87
    return-void
.end method

.method public static intersects(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p0, "rectangle"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 69
    new-instance v0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;-><init>(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 70
    .local v0, "rp":Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 7
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 98
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v3

    .line 105
    :cond_1
    new-instance v2, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;

    iget-object v5, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v2, v5}, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 106
    .local v2, "visitor":Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;
    invoke-virtual {v2, p1}, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->applyTo(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 107
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/predicate/EnvelopeIntersectsVisitor;->intersects()Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    .line 108
    goto :goto_0

    .line 113
    :cond_2
    new-instance v0, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;

    iget-object v5, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->rectangle:Lcom/vividsolutions/jts/geom/Polygon;

    invoke-direct {v0, v5}, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;-><init>(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 114
    .local v0, "ecpVisitor":Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->applyTo(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 115
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->containsPoint()Z

    move-result v5

    if-eqz v5, :cond_3

    move v3, v4

    .line 116
    goto :goto_0

    .line 121
    :cond_3
    new-instance v1, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;

    iget-object v5, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->rectangle:Lcom/vividsolutions/jts/geom/Polygon;

    invoke-direct {v1, v5}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;-><init>(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 122
    .local v1, "riVisitor":Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;
    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->applyTo(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 123
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersectsSegmentVisitor;->intersects()Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    .line 124
    goto :goto_0
.end method
