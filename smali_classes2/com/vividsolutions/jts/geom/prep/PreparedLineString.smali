.class public Lcom/vividsolutions/jts/geom/prep/PreparedLineString;
.super Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;
.source "PreparedLineString.java"


# instance fields
.field private segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Lineal;)V
    .locals 1
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/Lineal;

    .prologue
    .line 52
    check-cast p1, Lcom/vividsolutions/jts/geom/Geometry;

    .end local p1    # "line":Lcom/vividsolutions/jts/geom/Lineal;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;->segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    .line 53
    return-void
.end method


# virtual methods
.method public declared-synchronized getIntersectionFinder()Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;
    .locals 2

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;->segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-static {v1}, Lcom/vividsolutions/jts/noding/SegmentStringUtil;->extractSegmentStrings(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;->segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;->segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;->envelopesIntersect(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 71
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;->intersects(Lcom/vividsolutions/jts/geom/prep/PreparedLineString;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0
.end method
