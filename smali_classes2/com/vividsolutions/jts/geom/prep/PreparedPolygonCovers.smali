.class Lcom/vividsolutions/jts/geom/prep/PreparedPolygonCovers;
.super Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;
.source "PreparedPolygonCovers.java"


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V
    .locals 1
    .param p1, "prepPoly"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/AbstractPreparedPolygonContains;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonCovers;->requireSomePointInInterior:Z

    .line 75
    return-void
.end method

.method public static covers(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p0, "prep"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 62
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonCovers;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonCovers;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V

    .line 63
    .local v0, "polyInt":Lcom/vividsolutions/jts/geom/prep/PreparedPolygonCovers;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonCovers;->covers(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public covers(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonCovers;->eval(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method protected fullTopologicalPredicate(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 97
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonCovers;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/Geometry;->covers(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    .line 98
    .local v0, "result":Z
    return v0
.end method
