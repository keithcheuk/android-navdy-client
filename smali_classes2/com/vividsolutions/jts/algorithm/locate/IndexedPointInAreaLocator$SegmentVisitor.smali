.class Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$SegmentVisitor;
.super Ljava/lang/Object;
.source "IndexedPointInAreaLocator.java"

# interfaces
.implements Lcom/vividsolutions/jts/index/ItemVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SegmentVisitor"
.end annotation


# instance fields
.field private counter:Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;)V
    .locals 0
    .param p1, "counter"    # Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$SegmentVisitor;->counter:Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;

    .line 103
    return-void
.end method


# virtual methods
.method public visitItem(Ljava/lang/Object;)V
    .locals 4
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 107
    move-object v0, p1

    check-cast v0, Lcom/vividsolutions/jts/geom/LineSegment;

    .line 108
    .local v0, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$SegmentVisitor;->counter:Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;->countSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 109
    return-void
.end method
