.class public Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;
.super Ljava/lang/Object;
.source "IndexedPointInAreaLocator.java"

# interfaces
.implements Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;,
        Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$SegmentVisitor;
    }
.end annotation


# instance fields
.field private index:Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygonal;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument must be Polygonal"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;->buildIndex(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 66
    return-void
.end method

.method private buildIndex(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 70
    new-instance v0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;->index:Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;

    .line 71
    return-void
.end method


# virtual methods
.method public locate(Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 7
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 81
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 83
    .local v0, "rcc":Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;
    new-instance v6, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$SegmentVisitor;

    invoke-direct {v6, v0}, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$SegmentVisitor;-><init>(Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;)V

    .line 84
    .local v6, "visitor":Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$SegmentVisitor;
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;->index:Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual/range {v1 .. v6}, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;->query(DDLcom/vividsolutions/jts/index/ItemVisitor;)V

    .line 92
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;->getLocation()I

    move-result v1

    return v1
.end method
