.class public Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;
.super Ljava/lang/Object;
.source "DiscreteHausdorffDistance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;,
        Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;
    }
.end annotation


# instance fields
.field private densifyFrac:D

.field private g0:Lcom/vividsolutions/jts/geom/Geometry;

.field private g1:Lcom/vividsolutions/jts/geom/Geometry;

.field private ptDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->ptDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .line 96
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->densifyFrac:D

    .line 100
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->g0:Lcom/vividsolutions/jts/geom/Geometry;

    .line 101
    iput-object p2, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->g1:Lcom/vividsolutions/jts/geom/Geometry;

    .line 102
    return-void
.end method

.method private compute(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->ptDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-direct {p0, p1, p2, v0}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->computeOrientedDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 138
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->ptDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-direct {p0, p2, p1, v0}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->computeOrientedDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 139
    return-void
.end method

.method private computeOrientedDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V
    .locals 6
    .param p1, "discreteGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "ptDist"    # Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .prologue
    .line 143
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;

    invoke-direct {v0, p2}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 144
    .local v0, "distFilter":Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    .line 145
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->getMaxPointDistance()Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 147
    iget-wide v2, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->densifyFrac:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 148
    new-instance v1, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;

    iget-wide v2, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->densifyFrac:D

    invoke-direct {v1, p2, v2, v3}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;-><init>(Lcom/vividsolutions/jts/geom/Geometry;D)V

    .line 149
    .local v1, "fracFilter":Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V

    .line 150
    invoke-virtual {v1}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->getMaxPointDistance()Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 153
    .end local v1    # "fracFilter":Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;
    :cond_0
    return-void
.end method

.method public static distance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 4
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 78
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 79
    .local v0, "dist":Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->distance()D

    move-result-wide v2

    return-wide v2
.end method

.method public static distance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)D
    .locals 4
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "densifyFrac"    # D

    .prologue
    .line 84
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 85
    .local v0, "dist":Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;
    invoke-virtual {v0, p2, p3}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->setDensifyFraction(D)V

    .line 86
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->distance()D

    move-result-wide v2

    return-wide v2
.end method


# virtual methods
.method public distance()D
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->g0:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->g1:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->compute(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 124
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->ptDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->getDistance()D

    move-result-wide v0

    return-wide v0
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->ptDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public orientedDistance()D
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->g0:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->g1:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->ptDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-direct {p0, v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->computeOrientedDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 130
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->ptDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->getDistance()D

    move-result-wide v0

    return-wide v0
.end method

.method public setDensifyFraction(D)V
    .locals 3
    .param p1, "densifyFrac"    # D

    .prologue
    .line 114
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_1

    .line 116
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Fraction is not in range (0.0 - 1.0]"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_1
    iput-wide p1, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;->densifyFrac:D

    .line 119
    return-void
.end method
