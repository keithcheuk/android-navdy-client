.class public Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;
.super Ljava/lang/Object;
.source "ConformingDelaunayTriangulationBuilder.java"


# instance fields
.field private constraintLines:Lcom/vividsolutions/jts/geom/Geometry;

.field private constraintVertexMap:Ljava/util/Map;

.field private siteCoords:Ljava/util/Collection;

.field private subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

.field private tolerance:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->tolerance:D

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .line 56
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->constraintVertexMap:Ljava/util/Map;

    .line 60
    return-void
.end method

.method private create()V
    .locals 6

    .prologue
    .line 105
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    if-eqz v4, :cond_0

    .line 124
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->siteCoords:Ljava/util/Collection;

    invoke-static {v4}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->envelope(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    .line 109
    .local v2, "siteEnv":Lcom/vividsolutions/jts/geom/Envelope;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v1, "segments":Ljava/util/List;
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->constraintLines:Lcom/vividsolutions/jts/geom/Geometry;

    if-eqz v4, :cond_1

    .line 111
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->constraintLines:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 112
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->constraintLines:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->createVertices(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 113
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->constraintLines:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v4}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->createConstraintSegments(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v1

    .line 115
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->siteCoords:Ljava/util/Collection;

    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->createSiteVertices(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v3

    .line 117
    .local v3, "sites":Ljava/util/List;
    new-instance v0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;

    iget-wide v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->tolerance:D

    invoke-direct {v0, v3, v4, v5}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;-><init>(Ljava/util/Collection;D)V

    .line 119
    .local v0, "cdt":Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->constraintVertexMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v4}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->setConstraints(Ljava/util/List;Ljava/util/List;)V

    .line 121
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->formInitialDelaunay()V

    .line 122
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->enforceConstraints()V

    .line 123
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->getSubdivision()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    move-result-object v4

    iput-object v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    goto :goto_0
.end method

.method private static createConstraintSegments(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
    .locals 5
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 149
    invoke-static {p0}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v3

    .line 150
    .local v3, "lines":Ljava/util/List;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v0, "constraintSegs":Ljava/util/List;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 152
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LineString;

    .line 153
    .local v2, "line":Lcom/vividsolutions/jts/geom/LineString;
    invoke-static {v2, v0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->createConstraintSegments(Lcom/vividsolutions/jts/geom/LineString;Ljava/util/List;)V

    goto :goto_0

    .line 155
    .end local v2    # "line":Lcom/vividsolutions/jts/geom/LineString;
    :cond_0
    return-object v0
.end method

.method private static createConstraintSegments(Lcom/vividsolutions/jts/geom/LineString;Ljava/util/List;)V
    .locals 5
    .param p0, "line"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p1, "constraintSegs"    # Ljava/util/List;

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 161
    .local v0, "coords":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 162
    new-instance v2, Lcom/vividsolutions/jts/triangulate/Segment;

    add-int/lit8 v3, v1, -0x1

    aget-object v3, v0, v3

    aget-object v4, v0, v1

    invoke-direct {v2, v3, v4}, Lcom/vividsolutions/jts/triangulate/Segment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164
    :cond_0
    return-void
.end method

.method private createSiteVertices(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .param p1, "coords"    # Ljava/util/Collection;

    .prologue
    .line 128
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v2, "verts":Ljava/util/List;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 130
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 131
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->constraintVertexMap:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 133
    new-instance v3, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    invoke-direct {v3, v0}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 135
    .end local v0    # "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    return-object v2
.end method

.method private createVertices(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 5
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 140
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 141
    .local v0, "coords":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 142
    new-instance v2, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    aget-object v3, v0, v1

    invoke-direct {v2, v3}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 143
    .local v2, "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->constraintVertexMap:Ljava/util/Map;

    aget-object v4, v0, v1

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 145
    .end local v2    # "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    :cond_0
    return-void
.end method


# virtual methods
.method public getEdges(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->create()V

    .line 186
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getEdges(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public getSubdivision()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->create()V

    .line 174
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    return-object v0
.end method

.method public getTriangles(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 198
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->create()V

    .line 199
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getTriangles(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public setConstraints(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "constraintLines"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->constraintLines:Lcom/vividsolutions/jts/geom/Geometry;

    .line 88
    return-void
.end method

.method public setSites(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 74
    invoke-static {p1}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->extractUniqueCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateList;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->siteCoords:Ljava/util/Collection;

    .line 75
    return-void
.end method

.method public setTolerance(D)V
    .locals 1
    .param p1, "tolerance"    # D

    .prologue
    .line 99
    iput-wide p1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulationBuilder;->tolerance:D

    .line 100
    return-void
.end method
