.class public Lcom/vividsolutions/jts/geomgraph/EdgeNodingValidator;
.super Ljava/lang/Object;
.source "EdgeNodingValidator.java"


# instance fields
.field private nv:Lcom/vividsolutions/jts/noding/FastNodingValidator;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 2
    .param p1, "edges"    # Ljava/util/Collection;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Lcom/vividsolutions/jts/noding/FastNodingValidator;

    invoke-static {p1}, Lcom/vividsolutions/jts/geomgraph/EdgeNodingValidator;->toSegmentStrings(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/noding/FastNodingValidator;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/EdgeNodingValidator;->nv:Lcom/vividsolutions/jts/noding/FastNodingValidator;

    .line 83
    return-void
.end method

.method public static checkValid(Ljava/util/Collection;)V
    .locals 1
    .param p0, "edges"    # Ljava/util/Collection;

    .prologue
    .line 58
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/EdgeNodingValidator;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/geomgraph/EdgeNodingValidator;-><init>(Ljava/util/Collection;)V

    .line 59
    .local v0, "validator":Lcom/vividsolutions/jts/geomgraph/EdgeNodingValidator;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/EdgeNodingValidator;->checkValid()V

    .line 60
    return-void
.end method

.method public static toSegmentStrings(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 5
    .param p0, "edges"    # Ljava/util/Collection;

    .prologue
    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v2, "segStrings":Ljava/util/Collection;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 67
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 68
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    new-instance v3, Lcom/vividsolutions/jts/noding/BasicSegmentString;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/vividsolutions/jts/noding/BasicSegmentString;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-object v2
.end method


# virtual methods
.method public checkValid()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/EdgeNodingValidator;->nv:Lcom/vividsolutions/jts/noding/FastNodingValidator;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/noding/FastNodingValidator;->checkValid()V

    .line 95
    return-void
.end method
