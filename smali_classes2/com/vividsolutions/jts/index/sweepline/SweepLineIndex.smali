.class public Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;
.super Ljava/lang/Object;
.source "SweepLineIndex.java"


# instance fields
.field events:Ljava/util/List;

.field private indexBuilt:Z

.field private nOverlaps:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->events:Ljava/util/List;

    .line 53
    return-void
.end method

.method private buildIndex()V
    .locals 3

    .prologue
    .line 69
    iget-boolean v2, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->indexBuilt:Z

    if-eqz v2, :cond_0

    .line 79
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->events:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 71
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->events:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 73
    iget-object v2, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->events:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;

    .line 74
    .local v0, "ev":Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;->isDelete()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;->getInsertEvent()Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;->setDeleteEventIndex(I)V

    .line 71
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 78
    .end local v0    # "ev":Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->indexBuilt:Z

    goto :goto_0
.end method

.method private processOverlaps(IILcom/vividsolutions/jts/index/sweepline/SweepLineInterval;Lcom/vividsolutions/jts/index/sweepline/SweepLineOverlapAction;)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "s0"    # Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;
    .param p4, "action"    # Lcom/vividsolutions/jts/index/sweepline/SweepLineOverlapAction;

    .prologue
    .line 102
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 103
    iget-object v3, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->events:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;

    .line 104
    .local v0, "ev":Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;->isInsert()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 105
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;->getInterval()Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;

    move-result-object v2

    .line 106
    .local v2, "s1":Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;
    invoke-interface {p4, p3, v2}, Lcom/vividsolutions/jts/index/sweepline/SweepLineOverlapAction;->overlap(Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;)V

    .line 107
    iget v3, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->nOverlaps:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->nOverlaps:I

    .line 102
    .end local v2    # "s1":Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 111
    .end local v0    # "ev":Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;
    :cond_1
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;)V
    .locals 6
    .param p1, "sweepInt"    # Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;

    .prologue
    .line 57
    new-instance v0, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;->getMin()D

    move-result-wide v2

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1, p1}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;-><init>(DLcom/vividsolutions/jts/index/sweepline/SweepLineEvent;Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;)V

    .line 58
    .local v0, "insertEvent":Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;
    iget-object v1, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->events:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v1, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->events:Ljava/util/List;

    new-instance v2, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;->getMax()D

    move-result-wide v4

    invoke-direct {v2, v4, v5, v0, p1}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;-><init>(DLcom/vividsolutions/jts/index/sweepline/SweepLineEvent;Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method public computeOverlaps(Lcom/vividsolutions/jts/index/sweepline/SweepLineOverlapAction;)V
    .locals 4
    .param p1, "action"    # Lcom/vividsolutions/jts/index/sweepline/SweepLineOverlapAction;

    .prologue
    .line 83
    const/4 v2, 0x0

    iput v2, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->nOverlaps:I

    .line 84
    invoke-direct {p0}, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->buildIndex()V

    .line 86
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->events:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 88
    iget-object v2, p0, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->events:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;

    .line 89
    .local v0, "ev":Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;->isInsert()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;->getDeleteEventIndex()I

    move-result v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;->getInterval()Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3, p1}, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->processOverlaps(IILcom/vividsolutions/jts/index/sweepline/SweepLineInterval;Lcom/vividsolutions/jts/index/sweepline/SweepLineOverlapAction;)V

    .line 86
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    .end local v0    # "ev":Lcom/vividsolutions/jts/index/sweepline/SweepLineEvent;
    :cond_1
    return-void
.end method
