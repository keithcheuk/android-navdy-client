.class public Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;
.super Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;
.source "PointShapeFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/awt/PointShapeFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Cross"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0}, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;-><init>()V

    .line 302
    return-void
.end method

.method public constructor <init>(D)V
    .locals 1
    .param p1, "size"    # D

    .prologue
    .line 311
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;-><init>(D)V

    .line 312
    return-void
.end method


# virtual methods
.method public createPoint(Ljava/awt/geom/Point2D;)Ljava/awt/Shape;
    .locals 18
    .param p1, "point"    # Ljava/awt/geom/Point2D;

    .prologue
    .line 323
    invoke-virtual/range {p1 .. p1}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;->size:D

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    sub-double/2addr v12, v14

    double-to-float v3, v12

    .line 324
    .local v3, "x1":F
    invoke-virtual/range {p1 .. p1}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;->size:D

    const-wide/high16 v16, 0x4010000000000000L    # 4.0

    div-double v14, v14, v16

    sub-double/2addr v12, v14

    double-to-float v4, v12

    .line 325
    .local v4, "x2":F
    invoke-virtual/range {p1 .. p1}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;->size:D

    const-wide/high16 v16, 0x4010000000000000L    # 4.0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-float v5, v12

    .line 326
    .local v5, "x3":F
    invoke-virtual/range {p1 .. p1}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;->size:D

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-float v6, v12

    .line 328
    .local v6, "x4":F
    invoke-virtual/range {p1 .. p1}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;->size:D

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    sub-double/2addr v12, v14

    double-to-float v7, v12

    .line 329
    .local v7, "y1":F
    invoke-virtual/range {p1 .. p1}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;->size:D

    const-wide/high16 v16, 0x4010000000000000L    # 4.0

    div-double v14, v14, v16

    sub-double/2addr v12, v14

    double-to-float v8, v12

    .line 330
    .local v8, "y2":F
    invoke-virtual/range {p1 .. p1}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;->size:D

    const-wide/high16 v16, 0x4010000000000000L    # 4.0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-float v9, v12

    .line 331
    .local v9, "y3":F
    invoke-virtual/range {p1 .. p1}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;->size:D

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    add-double/2addr v12, v14

    double-to-float v10, v12

    .line 333
    .local v10, "y4":F
    new-instance v2, Ljava/awt/geom/GeneralPath;

    invoke-direct {v2}, Ljava/awt/geom/GeneralPath;-><init>()V

    .line 334
    .local v2, "path":Ljava/awt/geom/GeneralPath;
    invoke-virtual {v2, v4, v7}, Ljava/awt/geom/GeneralPath;->moveTo(FF)V

    .line 335
    invoke-virtual {v2, v5, v7}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 336
    invoke-virtual {v2, v5, v8}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 337
    invoke-virtual {v2, v6, v8}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 338
    invoke-virtual {v2, v6, v9}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 339
    invoke-virtual {v2, v5, v9}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 340
    invoke-virtual {v2, v5, v10}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 341
    invoke-virtual {v2, v4, v10}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 342
    invoke-virtual {v2, v4, v9}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 343
    invoke-virtual {v2, v3, v9}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 344
    invoke-virtual {v2, v3, v8}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 345
    invoke-virtual {v2, v4, v8}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 346
    invoke-virtual {v2, v4, v7}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    .line 348
    return-object v2
.end method
