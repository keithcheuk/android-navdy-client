.class public Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;
.super Ljava/lang/Object;
.source "TopologyPreservingSimplifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringMapBuilderFilter;,
        Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringTransformer;
    }
.end annotation


# instance fields
.field private inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private lineSimplifier:Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;

.field private linestringMap:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "inputGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;

    invoke-direct {v0}, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->lineSimplifier:Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;

    .line 103
    iput-object p1, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 104
    return-void
.end method

.method static synthetic access$000(Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->linestringMap:Ljava/util/Map;

    return-object v0
.end method

.method public static simplify(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "distanceTolerance"    # D

    .prologue
    .line 92
    new-instance v0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 93
    .local v0, "tss":Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;
    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->setDistanceTolerance(D)V

    .line 94
    invoke-virtual {v0}, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->getResultGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getResultGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3

    .prologue
    .line 124
    iget-object v1, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Geometry;

    .line 130
    :goto_0
    return-object v1

    .line 126
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->linestringMap:Ljava/util/Map;

    .line 127
    iget-object v1, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    new-instance v2, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringMapBuilderFilter;

    invoke-direct {v2, p0}, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringMapBuilderFilter;-><init>(Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;)V

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 128
    iget-object v1, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->lineSimplifier:Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;

    iget-object v2, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->linestringMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->simplify(Ljava/util/Collection;)V

    .line 129
    new-instance v1, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringTransformer;

    invoke-direct {v1, p0}, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringTransformer;-><init>(Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;)V

    iget-object v2, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringTransformer;->transform(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .local v0, "result":Lcom/vividsolutions/jts/geom/Geometry;
    move-object v1, v0

    .line 130
    goto :goto_0
.end method

.method public setDistanceTolerance(D)V
    .locals 3
    .param p1, "distanceTolerance"    # D

    .prologue
    .line 116
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 117
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tolerance must be non-negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->lineSimplifier:Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;

    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/simplify/TaggedLinesSimplifier;->setDistanceTolerance(D)V

    .line 119
    return-void
.end method
