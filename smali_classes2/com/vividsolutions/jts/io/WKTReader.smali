.class public Lcom/vividsolutions/jts/io/WKTReader;
.super Ljava/lang/Object;
.source "WKTReader.java"


# static fields
.field private static final ALLOW_OLD_JTS_MULTIPOINT_SYNTAX:Z = true

.field private static final COMMA:Ljava/lang/String; = ","

.field private static final EMPTY:Ljava/lang/String; = "EMPTY"

.field private static final L_PAREN:Ljava/lang/String; = "("

.field private static final NAN_SYMBOL:Ljava/lang/String; = "NaN"

.field private static final R_PAREN:Ljava/lang/String; = ")"


# instance fields
.field private geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private precisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

.field private tokenizer:Ljava/io/StreamTokenizer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/io/WKTReader;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 147
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 1
    .param p1, "geometryFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput-object p1, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 157
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/io/WKTReader;->precisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 158
    return-void
.end method

.method private getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextEmptyOrOpener()Ljava/lang/String;

    move-result-object v2

    .line 226
    .local v2, "nextToken":Ljava/lang/String;
    const-string v3, "EMPTY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 227
    const/4 v3, 0x0

    new-array v3, v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 237
    :goto_0
    return-object v3

    .line 229
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 230
    .local v1, "coordinates":Ljava/util/ArrayList;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getPreciseCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v2

    .line 232
    :goto_1
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 233
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getPreciseCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 236
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 237
    .local v0, "array":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    check-cast v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0
.end method

.method private getCoordinatesNoLeftParen()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 241
    const/4 v2, 0x0

    .line 242
    .local v2, "nextToken":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 243
    .local v1, "coordinates":Ljava/util/ArrayList;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getPreciseCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v2

    .line 245
    :goto_0
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 246
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getPreciseCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 249
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 250
    .local v0, "array":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    check-cast v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v3
.end method

.method private getNextCloser()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 353
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextWord()Ljava/lang/String;

    move-result-object v0

    .line 354
    .local v0, "nextWord":Ljava/lang/String;
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 358
    .end local v0    # "nextWord":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 357
    .restart local v0    # "nextWord":Ljava/lang/String;
    :cond_0
    const-string v1, ")"

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/io/WKTReader;->parseErrorExpected(Ljava/lang/String;)V

    .line 358
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNextCloserOrComma()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextWord()Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "nextWord":Ljava/lang/String;
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 340
    .end local v0    # "nextWord":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 339
    .restart local v0    # "nextWord":Ljava/lang/String;
    :cond_1
    const-string v1, ", or )"

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/io/WKTReader;->parseErrorExpected(Ljava/lang/String;)V

    .line 340
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNextEmptyOrOpener()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextWord()Ljava/lang/String;

    move-result-object v0

    .line 318
    .local v0, "nextWord":Ljava/lang/String;
    const-string v1, "EMPTY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 322
    .end local v0    # "nextWord":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 321
    .restart local v0    # "nextWord":Ljava/lang/String;
    :cond_1
    const-string v1, "EMPTY or ("

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/io/WKTReader;->parseErrorExpected(Ljava/lang/String;)V

    .line 322
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNextNumber()D
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 286
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v2}, Ljava/io/StreamTokenizer;->nextToken()I

    move-result v1

    .line 287
    .local v1, "type":I
    packed-switch v1, :pswitch_data_0

    .line 303
    :goto_0
    const-string v2, "number"

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/io/WKTReader;->parseErrorExpected(Ljava/lang/String;)V

    .line 304
    const-wide/16 v2, 0x0

    :goto_1
    return-wide v2

    .line 290
    :pswitch_0
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    iget-object v2, v2, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    const-string v3, "NaN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    goto :goto_1

    .line 295
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    iget-object v2, v2, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_1

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "ex":Ljava/lang/NumberFormatException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid number: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    iget-object v3, v3, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/io/WKTReader;->parseErrorWithLine(Ljava/lang/String;)V

    goto :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
    .end packed-switch
.end method

.method private getNextWord()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 371
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v2}, Ljava/io/StreamTokenizer;->nextToken()I

    move-result v0

    .line 372
    .local v0, "type":I
    sparse-switch v0, :sswitch_data_0

    .line 384
    const-string v2, "word"

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/io/WKTReader;->parseErrorExpected(Ljava/lang/String;)V

    .line 385
    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    .line 375
    :sswitch_0
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    iget-object v1, v2, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    .line 376
    .local v1, "word":Ljava/lang/String;
    const-string v2, "EMPTY"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 377
    const-string v1, "EMPTY"

    goto :goto_0

    .line 380
    .end local v1    # "word":Ljava/lang/String;
    :sswitch_1
    const-string v1, "("

    goto :goto_0

    .line 381
    :sswitch_2
    const-string v1, ")"

    goto :goto_0

    .line 382
    :sswitch_3
    const-string v1, ","

    goto :goto_0

    .line 372
    :sswitch_data_0
    .sparse-switch
        -0x3 -> :sswitch_0
        0x28 -> :sswitch_1
        0x29 -> :sswitch_2
        0x2c -> :sswitch_3
    .end sparse-switch
.end method

.method private getPreciseCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 256
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 257
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextNumber()D

    move-result-wide v2

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 258
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextNumber()D

    move-result-wide v2

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 259
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->isNumberNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextNumber()D

    move-result-wide v2

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->precisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 263
    return-object v0
.end method

.method private isNumberNext()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 267
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v1}, Ljava/io/StreamTokenizer;->nextToken()I

    move-result v0

    .line 268
    .local v0, "type":I
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v1}, Ljava/io/StreamTokenizer;->pushBack()V

    .line 269
    const/4 v1, -0x3

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private lookaheadWord()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 398
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextWord()Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "nextWord":Ljava/lang/String;
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v1}, Ljava/io/StreamTokenizer;->pushBack()V

    .line 400
    return-object v0
.end method

.method private parseErrorExpected(Ljava/lang/String;)V
    .locals 3
    .param p1, "expected"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 415
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    iget v1, v1, Ljava/io/StreamTokenizer;->ttype:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_0

    .line 416
    const-string v1, "Unexpected NUMBER token"

    invoke-static {v1}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere(Ljava/lang/String;)V

    .line 417
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    iget v1, v1, Ljava/io/StreamTokenizer;->ttype:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_1

    .line 418
    const-string v1, "Unexpected EOL token"

    invoke-static {v1}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere(Ljava/lang/String;)V

    .line 420
    :cond_1
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->tokenString()Ljava/lang/String;

    move-result-object v0

    .line 421
    .local v0, "tokenStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/io/WKTReader;->parseErrorWithLine(Ljava/lang/String;)V

    .line 422
    return-void
.end method

.method private parseErrorWithLine(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 427
    new-instance v0, Lcom/vividsolutions/jts/io/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v2}, Ljava/io/StreamTokenizer;->lineno()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/io/ParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private readGeometryCollectionText()Lcom/vividsolutions/jts/geom/GeometryCollection;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 717
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextEmptyOrOpener()Ljava/lang/String;

    move-result-object v3

    .line 718
    .local v3, "nextToken":Ljava/lang/String;
    const-string v4, "EMPTY"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 719
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v4

    .line 731
    :goto_0
    return-object v4

    .line 721
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 722
    .local v1, "geometries":Ljava/util/ArrayList;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readGeometryTaggedText()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 723
    .local v2, "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 724
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v3

    .line 725
    :goto_1
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 726
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readGeometryTaggedText()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 727
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 728
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 730
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Lcom/vividsolutions/jts/geom/Geometry;

    .line 731
    .local v0, "array":[Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v5, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/vividsolutions/jts/geom/Geometry;

    check-cast v4, [Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v5, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v4

    goto :goto_0
.end method

.method private readGeometryTaggedText()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 461
    const/4 v1, 0x0

    .line 464
    .local v1, "type":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextWord()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/vividsolutions/jts/io/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 471
    const-string v3, "POINT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 472
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readPointText()Lcom/vividsolutions/jts/geom/Point;

    move-result-object v2

    .line 497
    :goto_0
    return-object v2

    .line 465
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0

    .line 467
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 468
    .local v0, "e":Lcom/vividsolutions/jts/io/ParseException;
    goto :goto_0

    .line 474
    .end local v0    # "e":Lcom/vividsolutions/jts/io/ParseException;
    :cond_0
    const-string v3, "LINESTRING"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 475
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readLineStringText()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    goto :goto_0

    .line 477
    :cond_1
    const-string v3, "LINEARRING"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 478
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readLinearRingText()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v2

    goto :goto_0

    .line 480
    :cond_2
    const-string v3, "POLYGON"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 481
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readPolygonText()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v2

    goto :goto_0

    .line 483
    :cond_3
    const-string v3, "MULTIPOINT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 484
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readMultiPointText()Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v2

    goto :goto_0

    .line 486
    :cond_4
    const-string v3, "MULTILINESTRING"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 487
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readMultiLineStringText()Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v2

    goto :goto_0

    .line 489
    :cond_5
    const-string v3, "MULTIPOLYGON"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 490
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readMultiPolygonText()Lcom/vividsolutions/jts/geom/MultiPolygon;

    move-result-object v2

    goto :goto_0

    .line 492
    :cond_6
    const-string v3, "GEOMETRYCOLLECTION"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 493
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readGeometryCollectionText()Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v2

    goto :goto_0

    .line 495
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown geometry type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/io/WKTReader;->parseErrorWithLine(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private readLineStringText()Lcom/vividsolutions/jts/geom/LineString;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 531
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method

.method private readLinearRingText()Lcom/vividsolutions/jts/geom/LinearRing;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 549
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v0

    return-object v0
.end method

.method private readMultiLineStringText()Lcom/vividsolutions/jts/geom/MultiLineString;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 657
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextEmptyOrOpener()Ljava/lang/String;

    move-result-object v3

    .line 658
    .local v3, "nextToken":Ljava/lang/String;
    const-string v4, "EMPTY"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 659
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v4

    .line 671
    :goto_0
    return-object v4

    .line 661
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 662
    .local v2, "lineStrings":Ljava/util/ArrayList;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readLineStringText()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    .line 663
    .local v1, "lineString":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 664
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v3

    .line 665
    :goto_1
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 666
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readLineStringText()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    .line 667
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 670
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Lcom/vividsolutions/jts/geom/LineString;

    .line 671
    .local v0, "array":[Lcom/vividsolutions/jts/geom/LineString;
    iget-object v5, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/vividsolutions/jts/geom/LineString;

    check-cast v4, [Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v5, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v4

    goto :goto_0
.end method

.method private readMultiPointText()Lcom/vividsolutions/jts/geom/MultiPoint;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 572
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextEmptyOrOpener()Ljava/lang/String;

    move-result-object v1

    .line 573
    .local v1, "nextToken":Ljava/lang/String;
    const-string v5, "EMPTY"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 574
    iget-object v5, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v6, 0x0

    new-array v6, v6, [Lcom/vividsolutions/jts/geom/Point;

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Point;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v5

    .line 596
    :goto_0
    return-object v5

    .line 580
    :cond_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->lookaheadWord()Ljava/lang/String;

    move-result-object v2

    .line 581
    .local v2, "nextWord":Ljava/lang/String;
    const-string v5, "("

    if-eq v2, v5, :cond_1

    .line 582
    iget-object v5, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getCoordinatesNoLeftParen()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/vividsolutions/jts/io/WKTReader;->toPoints([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Point;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Point;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v5

    goto :goto_0

    .line 586
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 587
    .local v4, "points":Ljava/util/ArrayList;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readPointText()Lcom/vividsolutions/jts/geom/Point;

    move-result-object v3

    .line 588
    .local v3, "point":Lcom/vividsolutions/jts/geom/Point;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 589
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v1

    .line 590
    :goto_1
    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 591
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readPointText()Lcom/vividsolutions/jts/geom/Point;

    move-result-object v3

    .line 592
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 593
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 595
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [Lcom/vividsolutions/jts/geom/Point;

    .line 596
    .local v0, "array":[Lcom/vividsolutions/jts/geom/Point;
    iget-object v6, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/vividsolutions/jts/geom/Point;

    check-cast v5, [Lcom/vividsolutions/jts/geom/Point;

    invoke-virtual {v6, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Point;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v5

    goto :goto_0
.end method

.method private readMultiPolygonText()Lcom/vividsolutions/jts/geom/MultiPolygon;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 686
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextEmptyOrOpener()Ljava/lang/String;

    move-result-object v1

    .line 687
    .local v1, "nextToken":Ljava/lang/String;
    const-string v4, "EMPTY"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 688
    iget-object v4, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/vividsolutions/jts/geom/Polygon;

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPolygon([Lcom/vividsolutions/jts/geom/Polygon;)Lcom/vividsolutions/jts/geom/MultiPolygon;

    move-result-object v4

    .line 700
    :goto_0
    return-object v4

    .line 690
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 691
    .local v3, "polygons":Ljava/util/ArrayList;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readPolygonText()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v2

    .line 692
    .local v2, "polygon":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 693
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v1

    .line 694
    :goto_1
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 695
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readPolygonText()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v2

    .line 696
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 697
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 699
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v0, v4, [Lcom/vividsolutions/jts/geom/Polygon;

    .line 700
    .local v0, "array":[Lcom/vividsolutions/jts/geom/Polygon;
    iget-object v5, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/vividsolutions/jts/geom/Polygon;

    check-cast v4, [Lcom/vividsolutions/jts/geom/Polygon;

    invoke-virtual {v5, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPolygon([Lcom/vividsolutions/jts/geom/Polygon;)Lcom/vividsolutions/jts/geom/MultiPolygon;

    move-result-object v4

    goto :goto_0
.end method

.method private readPointText()Lcom/vividsolutions/jts/geom/Point;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 511
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextEmptyOrOpener()Ljava/lang/String;

    move-result-object v0

    .line 512
    .local v0, "nextToken":Ljava/lang/String;
    const-string v2, "EMPTY"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 513
    iget-object v3, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v2, 0x0

    check-cast v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v1

    .line 517
    :goto_0
    return-object v1

    .line 515
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getPreciseCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v1

    .line 516
    .local v1, "point":Lcom/vividsolutions/jts/geom/Point;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloser()Ljava/lang/String;

    goto :goto_0
.end method

.method private readPolygonText()Lcom/vividsolutions/jts/geom/Polygon;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 629
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextEmptyOrOpener()Ljava/lang/String;

    move-result-object v3

    .line 630
    .local v3, "nextToken":Ljava/lang/String;
    const-string v5, "EMPTY"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 631
    iget-object v5, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v6, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    new-array v7, v8, [Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v6

    new-array v7, v8, [Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v5, v6, v7}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v5

    .line 643
    :goto_0
    return-object v5

    .line 634
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 635
    .local v2, "holes":Ljava/util/ArrayList;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readLinearRingText()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v4

    .line 636
    .local v4, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v3

    .line 637
    :goto_1
    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 638
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readLinearRingText()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v1

    .line 639
    .local v1, "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 640
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->getNextCloserOrComma()Ljava/lang/String;

    move-result-object v3

    .line 641
    goto :goto_1

    .line 642
    .end local v1    # "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [Lcom/vividsolutions/jts/geom/LinearRing;

    .line 643
    .local v0, "array":[Lcom/vividsolutions/jts/geom/LinearRing;
    iget-object v6, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/vividsolutions/jts/geom/LinearRing;

    check-cast v5, [Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v6, v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v5

    goto :goto_0
.end method

.method private toPoints([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Point;
    .locals 4
    .param p1, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 609
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 610
    .local v1, "points":Ljava/util/ArrayList;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 611
    iget-object v2, p0, Lcom/vividsolutions/jts/io/WKTReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 610
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 613
    :cond_0
    const/4 v2, 0x0

    new-array v2, v2, [Lcom/vividsolutions/jts/geom/Point;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/vividsolutions/jts/geom/Point;

    check-cast v2, [Lcom/vividsolutions/jts/geom/Point;

    return-object v2
.end method

.method private tokenString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    iget v0, v0, Ljava/io/StreamTokenizer;->ttype:I

    sparse-switch v0, :sswitch_data_0

    .line 445
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    iget v1, v1, Ljava/io/StreamTokenizer;->ttype:I

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 439
    :sswitch_0
    const-string v0, "<NUMBER>"

    goto :goto_0

    .line 441
    :sswitch_1
    const-string v0, "End-of-Line"

    goto :goto_0

    .line 442
    :sswitch_2
    const-string v0, "End-of-Stream"

    goto :goto_0

    .line 443
    :sswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    iget-object v1, v1, Ljava/io/StreamTokenizer;->sval:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 437
    nop

    :sswitch_data_0
    .sparse-switch
        -0x3 -> :sswitch_3
        -0x2 -> :sswitch_0
        -0x1 -> :sswitch_2
        0xa -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public read(Ljava/io/Reader;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x2e

    const/16 v5, 0x2d

    const/16 v4, 0x2b

    .line 191
    new-instance v1, Ljava/io/StreamTokenizer;

    invoke-direct {v1, p1}, Ljava/io/StreamTokenizer;-><init>(Ljava/io/Reader;)V

    iput-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    .line 193
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v1}, Ljava/io/StreamTokenizer;->resetSyntax()V

    .line 194
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    const/16 v2, 0x61

    const/16 v3, 0x7a

    invoke-virtual {v1, v2, v3}, Ljava/io/StreamTokenizer;->wordChars(II)V

    .line 195
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    const/16 v2, 0x41

    const/16 v3, 0x5a

    invoke-virtual {v1, v2, v3}, Ljava/io/StreamTokenizer;->wordChars(II)V

    .line 196
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    const/16 v2, 0xa0

    const/16 v3, 0xff

    invoke-virtual {v1, v2, v3}, Ljava/io/StreamTokenizer;->wordChars(II)V

    .line 197
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    const/16 v2, 0x30

    const/16 v3, 0x39

    invoke-virtual {v1, v2, v3}, Ljava/io/StreamTokenizer;->wordChars(II)V

    .line 198
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v1, v5, v5}, Ljava/io/StreamTokenizer;->wordChars(II)V

    .line 199
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v1, v4, v4}, Ljava/io/StreamTokenizer;->wordChars(II)V

    .line 200
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    invoke-virtual {v1, v6, v6}, Ljava/io/StreamTokenizer;->wordChars(II)V

    .line 201
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    const/4 v2, 0x0

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Ljava/io/StreamTokenizer;->whitespaceChars(II)V

    .line 202
    iget-object v1, p0, Lcom/vividsolutions/jts/io/WKTReader;->tokenizer:Ljava/io/StreamTokenizer;

    const/16 v2, 0x23

    invoke-virtual {v1, v2}, Ljava/io/StreamTokenizer;->commentChar(I)V

    .line 205
    :try_start_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/io/WKTReader;->readGeometryTaggedText()Lcom/vividsolutions/jts/geom/Geometry;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Lcom/vividsolutions/jts/io/ParseException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/io/ParseException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public read(Ljava/lang/String;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "wellKnownText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vividsolutions/jts/io/ParseException;
        }
    .end annotation

    .prologue
    .line 172
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 174
    .local v0, "reader":Ljava/io/StringReader;
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/io/WKTReader;->read(Ljava/io/Reader;)Lcom/vividsolutions/jts/geom/Geometry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 177
    invoke-virtual {v0}, Ljava/io/StringReader;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/StringReader;->close()V

    throw v1
.end method
