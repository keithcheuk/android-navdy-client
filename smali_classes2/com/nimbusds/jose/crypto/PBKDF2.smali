.class Lcom/nimbusds/jose/crypto/PBKDF2;
.super Ljava/lang/Object;
.source "PBKDF2.java"


# static fields
.field public static ZERO_BYTE:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [B

    sput-object v0, Lcom/nimbusds/jose/crypto/PBKDF2;->ZERO_BYTE:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    return-void
.end method

.method public static deriveKey([B[BILcom/nimbusds/jose/crypto/PRFParams;)Ljavax/crypto/SecretKey;
    .locals 16
    .param p0, "password"    # [B
    .param p1, "formattedSalt"    # [B
    .param p2, "iterationCount"    # I
    .param p3, "prfParams"    # Lcom/nimbusds/jose/crypto/PRFParams;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 89
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual/range {p3 .. p3}, Lcom/nimbusds/jose/crypto/PRFParams;->getMACAlgorithm()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v12}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 91
    .local v7, "macKey":Ljavax/crypto/SecretKey;
    invoke-virtual/range {p3 .. p3}, Lcom/nimbusds/jose/crypto/PRFParams;->getMacProvider()Ljava/security/Provider;

    move-result-object v12

    invoke-static {v7, v12}, Lcom/nimbusds/jose/crypto/HMAC;->getInitMac(Ljavax/crypto/SecretKey;Ljava/security/Provider;)Ljavax/crypto/Mac;

    move-result-object v10

    .line 93
    .local v10, "prf":Ljavax/crypto/Mac;
    invoke-virtual {v10}, Ljavax/crypto/Mac;->getMacLength()I

    move-result v4

    .line 97
    .local v4, "hLen":I
    const-wide v8, 0xffffffffL

    .line 98
    .local v8, "maxDerivedKeyLength":J
    invoke-virtual/range {p3 .. p3}, Lcom/nimbusds/jose/crypto/PRFParams;->getDerivedKeyByteLength()I

    move-result v12

    int-to-long v12, v12

    cmp-long v12, v12, v8

    if-lez v12, :cond_0

    .line 99
    new-instance v12, Lcom/nimbusds/jose/JOSEException;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "derived key too long "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/nimbusds/jose/crypto/PRFParams;->getDerivedKeyByteLength()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 111
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/nimbusds/jose/crypto/PRFParams;->getDerivedKeyByteLength()I

    move-result v12

    int-to-double v12, v12

    int-to-double v14, v4

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v12

    double-to-int v6, v12

    .line 112
    .local v6, "l":I
    invoke-virtual/range {p3 .. p3}, Lcom/nimbusds/jose/crypto/PRFParams;->getDerivedKeyByteLength()I

    move-result v12

    add-int/lit8 v13, v6, -0x1

    mul-int/2addr v13, v4

    sub-int v11, v12, v13

    .line 145
    .local v11, "r":I
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 146
    .local v3, "byteArrayOutputStream":Ljava/io/ByteArrayOutputStream;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v6, :cond_1

    .line 155
    new-instance v12, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v13

    const-string v14, "AES"

    invoke-direct {v12, v13, v14}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v12

    .line 147
    :cond_1
    add-int/lit8 v12, v5, 0x1

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v12, v10}, Lcom/nimbusds/jose/crypto/PBKDF2;->extractBlock([BIILjavax/crypto/Mac;)[B

    move-result-object v2

    .line 148
    .local v2, "block":[B
    add-int/lit8 v12, v6, -0x1

    if-ne v5, v12, :cond_2

    .line 149
    const/4 v12, 0x0

    invoke-static {v2, v12, v11}, Lcom/nimbusds/jose/util/ByteUtils;->subArray([BII)[B

    move-result-object v2

    .line 151
    :cond_2
    const/4 v12, 0x0

    array-length v13, v2

    invoke-virtual {v3, v2, v12, v13}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 146
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private static extractBlock([BIILjavax/crypto/Mac;)[B
    .locals 9
    .param p0, "salt"    # [B
    .param p1, "iterationCount"    # I
    .param p2, "blockIndex"    # I
    .param p3, "prf"    # Ljavax/crypto/Mac;

    .prologue
    const/4 v8, 0x1

    .line 174
    const/4 v4, 0x0

    .line 175
    .local v4, "lastU":[B
    const/4 v5, 0x0

    .line 177
    .local v5, "xorU":[B
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-le v1, p1, :cond_0

    .line 197
    return-object v5

    .line 180
    :cond_0
    if-ne v1, v8, :cond_2

    .line 182
    const/4 v6, 0x2

    new-array v6, v6, [[B

    const/4 v7, 0x0

    aput-object p0, v6, v7

    invoke-static {p2}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v6}, Lcom/nimbusds/jose/util/ByteUtils;->concat([[B)[B

    move-result-object v2

    .line 183
    .local v2, "inputBytes":[B
    invoke-virtual {p3, v2}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    .line 184
    .local v0, "currentU":[B
    move-object v5, v0

    .line 195
    .end local v2    # "inputBytes":[B
    :cond_1
    move-object v4, v0

    .line 177
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 188
    .end local v0    # "currentU":[B
    :cond_2
    invoke-virtual {p3, v4}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    .line 189
    .restart local v0    # "currentU":[B
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v6, v0

    if-ge v3, v6, :cond_1

    .line 191
    aget-byte v6, v0, v3

    aget-byte v7, v5, v3

    xor-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v5, v3

    .line 189
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static formatSalt(Lcom/nimbusds/jose/JWEAlgorithm;[B)[B
    .locals 5
    .param p0, "alg"    # Lcom/nimbusds/jose/JWEAlgorithm;
    .param p1, "salt"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/nimbusds/jose/JWEAlgorithm;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v4}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 53
    .local v0, "algBytes":[B
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 56
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 57
    sget-object v3, Lcom/nimbusds/jose/crypto/PBKDF2;->ZERO_BYTE:[B

    invoke-virtual {v2, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 58
    invoke-virtual {v2, p1}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3

    .line 60
    :catch_0
    move-exception v1

    .line 62
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method
