.class public Lcom/nimbusds/jose/util/ByteUtils;
.super Ljava/lang/Object;
.source "ByteUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bitLength(I)I
    .locals 1
    .param p0, "byteLength"    # I

    .prologue
    .line 74
    mul-int/lit8 v0, p0, 0x8

    return v0
.end method

.method public static bitLength([B)I
    .locals 1
    .param p0, "byteArray"    # [B

    .prologue
    .line 87
    if-nez p0, :cond_0

    .line 88
    const/4 v0, 0x0

    .line 90
    :goto_0
    return v0

    :cond_0
    array-length v0, p0

    invoke-static {v0}, Lcom/nimbusds/jose/util/ByteUtils;->bitLength(I)I

    move-result v0

    goto :goto_0
.end method

.method public static byteLength(I)I
    .locals 1
    .param p0, "bitLength"    # I

    .prologue
    .line 104
    div-int/lit8 v0, p0, 0x8

    return v0
.end method

.method public static varargs concat([[B)[B
    .locals 5
    .param p0, "byteArrays"    # [[B

    .prologue
    .line 28
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 30
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    array-length v4, p0

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_0

    .line 38
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3

    .line 30
    :cond_0
    aget-object v1, p0, v3

    .line 32
    .local v1, "bytes":[B
    if-nez v1, :cond_1

    .line 30
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 36
    :cond_1
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 40
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "bytes":[B
    :catch_0
    move-exception v2

    .line 42
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public static subArray([BII)[B
    .locals 3
    .param p0, "byteArray"    # [B
    .param p1, "beginIndex"    # I
    .param p2, "length"    # I

    .prologue
    .line 59
    new-array v0, p2, [B

    .line 60
    .local v0, "subArray":[B
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p0, p1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 61
    return-object v0
.end method
