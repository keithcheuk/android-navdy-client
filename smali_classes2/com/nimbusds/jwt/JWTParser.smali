.class public final Lcom/nimbusds/jwt/JWTParser;
.super Ljava/lang/Object;
.source "JWTParser.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/nimbusds/jwt/JWT;
    .locals 9
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 41
    const-string v5, "."

    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 43
    .local v2, "firstDotPos":I
    const/4 v5, -0x1

    if-ne v2, v5, :cond_0

    .line 44
    new-instance v5, Ljava/text/ParseException;

    const-string v6, "Invalid JWT serialization: Missing dot delimiter(s)"

    invoke-direct {v5, v6, v8}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v5

    .line 46
    :cond_0
    new-instance v3, Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {p0, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 51
    .local v3, "header":Lcom/nimbusds/jose/util/Base64URL;
    :try_start_0
    invoke-virtual {v3}, Lcom/nimbusds/jose/util/Base64URL;->decodeToString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/nimbusds/jose/util/JSONObjectUtils;->parse(Ljava/lang/String;)Lnet/minidev/json/JSONObject;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 58
    .local v4, "jsonObject":Lnet/minidev/json/JSONObject;
    invoke-static {v4}, Lcom/nimbusds/jose/Header;->parseAlgorithm(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/Algorithm;

    move-result-object v0

    .line 60
    .local v0, "alg":Lcom/nimbusds/jose/Algorithm;
    sget-object v5, Lcom/nimbusds/jose/Algorithm;->NONE:Lcom/nimbusds/jose/Algorithm;

    invoke-virtual {v0, v5}, Lcom/nimbusds/jose/Algorithm;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 61
    invoke-static {p0}, Lcom/nimbusds/jwt/PlainJWT;->parse(Ljava/lang/String;)Lcom/nimbusds/jwt/PlainJWT;

    move-result-object v5

    .line 65
    :goto_0
    return-object v5

    .line 53
    .end local v0    # "alg":Lcom/nimbusds/jose/Algorithm;
    .end local v4    # "jsonObject":Lnet/minidev/json/JSONObject;
    :catch_0
    move-exception v1

    .line 55
    .local v1, "e":Ljava/text/ParseException;
    new-instance v5, Ljava/text/ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid unsecured/JWS/JWE header: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v8}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v5

    .line 62
    .end local v1    # "e":Ljava/text/ParseException;
    .restart local v0    # "alg":Lcom/nimbusds/jose/Algorithm;
    .restart local v4    # "jsonObject":Lnet/minidev/json/JSONObject;
    :cond_1
    instance-of v5, v0, Lcom/nimbusds/jose/JWSAlgorithm;

    if-eqz v5, :cond_2

    .line 63
    invoke-static {p0}, Lcom/nimbusds/jwt/SignedJWT;->parse(Ljava/lang/String;)Lcom/nimbusds/jwt/SignedJWT;

    move-result-object v5

    goto :goto_0

    .line 64
    :cond_2
    instance-of v5, v0, Lcom/nimbusds/jose/JWEAlgorithm;

    if-eqz v5, :cond_3

    .line 65
    invoke-static {p0}, Lcom/nimbusds/jwt/EncryptedJWT;->parse(Ljava/lang/String;)Lcom/nimbusds/jwt/EncryptedJWT;

    move-result-object v5

    goto :goto_0

    .line 67
    :cond_3
    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unexpected algorithm type: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5
.end method
