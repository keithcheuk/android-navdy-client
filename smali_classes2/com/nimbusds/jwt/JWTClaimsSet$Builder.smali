.class public Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
.super Ljava/lang/Object;
.source "JWTClaimsSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nimbusds/jwt/JWTClaimsSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final claims:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    .line 126
    return-void
.end method

.method public constructor <init>(Lcom/nimbusds/jwt/JWTClaimsSet;)V
    .locals 2
    .param p1, "jwtClaimsSet"    # Lcom/nimbusds/jwt/JWTClaimsSet;

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    .line 138
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    invoke-static {p1}, Lcom/nimbusds/jwt/JWTClaimsSet;->access$0(Lcom/nimbusds/jwt/JWTClaimsSet;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 139
    return-void
.end method


# virtual methods
.method public audience(Ljava/lang/String;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
    .locals 3
    .param p1, "aud"    # Ljava/lang/String;

    .prologue
    .line 195
    if-nez p1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const-string v1, "aud"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    :goto_0
    return-object p0

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const-string v1, "aud"

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public audience(Ljava/util/List;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/nimbusds/jwt/JWTClaimsSet$Builder;"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "aud":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const-string v1, "aud"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    return-object p0
.end method

.method public build()Lcom/nimbusds/jwt/JWTClaimsSet;
    .locals 3

    .prologue
    .line 287
    new-instance v0, Lcom/nimbusds/jwt/JWTClaimsSet;

    iget-object v1, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/nimbusds/jwt/JWTClaimsSet;-><init>(Ljava/util/Map;Lcom/nimbusds/jwt/JWTClaimsSet;)V

    return-object v0
.end method

.method public claim(Ljava/lang/String;Ljava/lang/Object;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    return-object p0
.end method

.method public expirationTime(Ljava/util/Date;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
    .locals 2
    .param p1, "exp"    # Ljava/util/Date;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const-string v1, "exp"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    return-object p0
.end method

.method public issueTime(Ljava/util/Date;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
    .locals 2
    .param p1, "iat"    # Ljava/util/Date;

    .prologue
    .line 244
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const-string v1, "iat"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    return-object p0
.end method

.method public issuer(Ljava/lang/String;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
    .locals 2
    .param p1, "iss"    # Ljava/lang/String;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const-string v1, "iss"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    return-object p0
.end method

.method public jwtID(Ljava/lang/String;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
    .locals 2
    .param p1, "jti"    # Ljava/lang/String;

    .prologue
    .line 258
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const-string v1, "jti"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    return-object p0
.end method

.method public notBeforeTime(Ljava/util/Date;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
    .locals 2
    .param p1, "nbf"    # Ljava/util/Date;

    .prologue
    .line 229
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const-string v1, "nbf"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    return-object p0
.end method

.method public subject(Ljava/lang/String;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;
    .locals 2
    .param p1, "sub"    # Ljava/lang/String;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claims:Ljava/util/Map;

    const-string v1, "sub"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    return-object p0
.end method
