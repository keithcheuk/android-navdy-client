.class public Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;
.super Ljava/lang/Object;
.source "DefaultJWTProcessor.java"

# interfaces
.implements Lcom/nimbusds/jwt/proc/ConfigurableJWTProcessor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Lcom/nimbusds/jose/proc/SecurityContext;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/nimbusds/jwt/proc/ConfigurableJWTProcessor",
        "<TC;>;"
    }
.end annotation


# static fields
.field private static final INVALID_NESTED_JWT_EXCEPTION:Lcom/nimbusds/jwt/proc/BadJWTException;

.field private static final INVALID_SIGNATURE:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWE_DECRYPTER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

.field private static final NO_JWE_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWE_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWS_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWS_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWS_VERIFIER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

.field private static final NO_MATCHING_DECRYPTERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_MATCHING_VERIFIERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final PLAIN_JWT_REJECTED_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;


# instance fields
.field private claimsVerifier:Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;

.field private jweDecrypterFactory:Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

.field private jweKeySelector:Lcom/nimbusds/jose/proc/JWEKeySelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nimbusds/jose/proc/JWEKeySelector",
            "<TC;>;"
        }
    .end annotation
.end field

.field private jwsKeySelector:Lcom/nimbusds/jose/proc/JWSKeySelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nimbusds/jose/proc/JWSKeySelector",
            "<TC;>;"
        }
    .end annotation
.end field

.field private jwsVerifierFactory:Lcom/nimbusds/jose/proc/JWSVerifierFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "Unsecured (plain) JWTs are rejected, extend class to handle"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 72
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->PLAIN_JWT_REJECTED_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 75
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "Signed JWT rejected: No JWS key selector is configured"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 74
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWS_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 77
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "Encrypted JWT rejected: No JWE key selector is configured"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 76
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWE_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 79
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    const-string v1, "No JWS verifier is configured"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    .line 78
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWS_VERIFIER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

    .line 81
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    const-string v1, "No JWE decrypter is configured"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    .line 80
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWE_DECRYPTER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

    .line 83
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "Signed JWT rejected: No matching key(s) found"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 82
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWS_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 85
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "Encrypted JWT rejected: No matching key(s) found"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 84
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWE_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 87
    new-instance v0, Lcom/nimbusds/jose/proc/BadJWSException;

    const-string v1, "Signed JWT rejected: Invalid signature"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJWSException;-><init>(Ljava/lang/String;)V

    .line 86
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->INVALID_SIGNATURE:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 89
    new-instance v0, Lcom/nimbusds/jwt/proc/BadJWTException;

    const-string v1, "The payload is not a nested JWT"

    invoke-direct {v0, v1}, Lcom/nimbusds/jwt/proc/BadJWTException;-><init>(Ljava/lang/String;)V

    .line 88
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->INVALID_NESTED_JWT_EXCEPTION:Lcom/nimbusds/jwt/proc/BadJWTException;

    .line 91
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "JWS object rejected: No matching verifier(s) found"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 90
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_MATCHING_VERIFIERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 93
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "Encrypted JWT rejected: No matching decrypter(s) found"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 92
    sput-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_MATCHING_DECRYPTERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 93
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;

    invoke-direct {v0}, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jwsVerifierFactory:Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    .line 116
    new-instance v0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;

    invoke-direct {v0}, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jweDecrypterFactory:Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    .line 122
    new-instance v0, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;

    invoke-direct {v0}, Lcom/nimbusds/jwt/proc/DefaultJWTClaimsVerifier;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->claimsVerifier:Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;

    .line 68
    return-void
.end method

.method private verifyAndReturnClaims(Lcom/nimbusds/jwt/JWT;)Lcom/nimbusds/jwt/JWTClaimsSet;
    .locals 4
    .param p1, "jwt"    # Lcom/nimbusds/jwt/JWT;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jwt/proc/BadJWTException;
        }
    .end annotation

    .prologue
    .line 211
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    :try_start_0
    invoke-interface {p1}, Lcom/nimbusds/jwt/JWT;->getJWTClaimsSet()Lcom/nimbusds/jwt/JWTClaimsSet;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 218
    .local v0, "claimsSet":Lcom/nimbusds/jwt/JWTClaimsSet;
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWTClaimsVerifier()Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 219
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWTClaimsVerifier()Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;->verify(Lcom/nimbusds/jwt/JWTClaimsSet;)V

    .line 222
    :cond_0
    return-object v0

    .line 213
    .end local v0    # "claimsSet":Lcom/nimbusds/jwt/JWTClaimsSet;
    :catch_0
    move-exception v1

    .line 215
    .local v1, "e":Ljava/text/ParseException;
    new-instance v2, Lcom/nimbusds/jwt/proc/BadJWTException;

    invoke-virtual {v1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/nimbusds/jwt/proc/BadJWTException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method


# virtual methods
.method public getJWEDecrypterFactory()Lcom/nimbusds/jose/proc/JWEDecrypterFactory;
    .locals 1

    .prologue
    .line 170
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jweDecrypterFactory:Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    return-object v0
.end method

.method public getJWEKeySelector()Lcom/nimbusds/jose/proc/JWEKeySelector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/nimbusds/jose/proc/JWEKeySelector",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 142
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jweKeySelector:Lcom/nimbusds/jose/proc/JWEKeySelector;

    return-object v0
.end method

.method public getJWSKeySelector()Lcom/nimbusds/jose/proc/JWSKeySelector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/nimbusds/jose/proc/JWSKeySelector",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 128
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jwsKeySelector:Lcom/nimbusds/jose/proc/JWSKeySelector;

    return-object v0
.end method

.method public getJWSVerifierFactory()Lcom/nimbusds/jose/proc/JWSVerifierFactory;
    .locals 1

    .prologue
    .line 156
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jwsVerifierFactory:Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    return-object v0
.end method

.method public getJWTClaimsVerifier()Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;
    .locals 1

    .prologue
    .line 184
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->claimsVerifier:Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;

    return-object v0
.end method

.method public process(Lcom/nimbusds/jwt/EncryptedJWT;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;
    .locals 8
    .param p1, "encryptedJWT"    # Lcom/nimbusds/jwt/EncryptedJWT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jwt/EncryptedJWT;",
            "TC;)",
            "Lcom/nimbusds/jwt/JWTClaimsSet;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/proc/BadJOSEException;,
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 314
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWEKeySelector()Lcom/nimbusds/jose/proc/JWEKeySelector;

    move-result-object v5

    if-nez v5, :cond_0

    .line 316
    sget-object v5, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWE_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v5

    .line 319
    :cond_0
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWEDecrypterFactory()Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    move-result-object v5

    if-nez v5, :cond_1

    .line 320
    sget-object v5, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWE_DECRYPTER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

    throw v5

    .line 323
    :cond_1
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWEKeySelector()Lcom/nimbusds/jose/proc/JWEKeySelector;

    move-result-object v5

    invoke-virtual {p1}, Lcom/nimbusds/jwt/EncryptedJWT;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v6

    invoke-interface {v5, v6, p2}, Lcom/nimbusds/jose/proc/JWEKeySelector;->selectJWEKeys(Lcom/nimbusds/jose/JWEHeader;Lcom/nimbusds/jose/proc/SecurityContext;)Ljava/util/List;

    move-result-object v3

    .line 325
    .local v3, "keyCandidates":Ljava/util/List;, "Ljava/util/List<+Ljava/security/Key;>;"
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 326
    :cond_2
    sget-object v5, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWE_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v5

    .line 329
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 331
    .local v2, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<+Ljava/security/Key;>;"
    :cond_4
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_5

    .line 369
    sget-object v5, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_MATCHING_DECRYPTERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v5

    .line 333
    :cond_5
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWEDecrypterFactory()Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    move-result-object v6

    invoke-virtual {p1}, Lcom/nimbusds/jwt/EncryptedJWT;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v7

    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/security/Key;

    invoke-interface {v6, v7, v5}, Lcom/nimbusds/jose/proc/JWEDecrypterFactory;->createJWEDecrypter(Lcom/nimbusds/jose/JWEHeader;Ljava/security/Key;)Lcom/nimbusds/jose/JWEDecrypter;

    move-result-object v0

    .line 335
    .local v0, "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    if-eqz v0, :cond_4

    .line 340
    :try_start_0
    invoke-virtual {p1, v0}, Lcom/nimbusds/jwt/EncryptedJWT;->decrypt(Lcom/nimbusds/jose/JWEDecrypter;)V
    :try_end_0
    .catch Lcom/nimbusds/jose/JOSEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    const-string v5, "JWT"

    invoke-virtual {p1}, Lcom/nimbusds/jwt/EncryptedJWT;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v6

    invoke-virtual {v6}, Lcom/nimbusds/jose/JWEHeader;->getContentType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 356
    invoke-virtual {p1}, Lcom/nimbusds/jwt/EncryptedJWT;->getPayload()Lcom/nimbusds/jose/Payload;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nimbusds/jose/Payload;->toSignedJWT()Lcom/nimbusds/jwt/SignedJWT;

    move-result-object v4

    .line 358
    .local v4, "nestedJWT":Lcom/nimbusds/jwt/SignedJWT;
    if-nez v4, :cond_6

    .line 360
    sget-object v5, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->INVALID_NESTED_JWT_EXCEPTION:Lcom/nimbusds/jwt/proc/BadJWTException;

    throw v5

    .line 342
    .end local v4    # "nestedJWT":Lcom/nimbusds/jwt/SignedJWT;
    :catch_0
    move-exception v1

    .line 344
    .local v1, "e":Lcom/nimbusds/jose/JOSEException;
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_4

    .line 350
    new-instance v5, Lcom/nimbusds/jose/proc/BadJWEException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Encrypted JWT rejected: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/nimbusds/jose/JOSEException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Lcom/nimbusds/jose/proc/BadJWEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 363
    .end local v1    # "e":Lcom/nimbusds/jose/JOSEException;
    .restart local v4    # "nestedJWT":Lcom/nimbusds/jwt/SignedJWT;
    :cond_6
    invoke-virtual {p0, v4, p2}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->process(Lcom/nimbusds/jwt/SignedJWT;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;

    move-result-object v5

    .line 366
    .end local v4    # "nestedJWT":Lcom/nimbusds/jwt/SignedJWT;
    :goto_0
    return-object v5

    :cond_7
    invoke-direct {p0, p1}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->verifyAndReturnClaims(Lcom/nimbusds/jwt/JWT;)Lcom/nimbusds/jwt/JWTClaimsSet;

    move-result-object v5

    goto :goto_0
.end method

.method public process(Lcom/nimbusds/jwt/JWT;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;
    .locals 3
    .param p1, "jwt"    # Lcom/nimbusds/jwt/JWT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jwt/JWT;",
            "TC;)",
            "Lcom/nimbusds/jwt/JWTClaimsSet;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/proc/BadJOSEException;,
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 238
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    instance-of v0, p1, Lcom/nimbusds/jwt/SignedJWT;

    if-eqz v0, :cond_0

    .line 239
    check-cast p1, Lcom/nimbusds/jwt/SignedJWT;

    .end local p1    # "jwt":Lcom/nimbusds/jwt/JWT;
    invoke-virtual {p0, p1, p2}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->process(Lcom/nimbusds/jwt/SignedJWT;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;

    move-result-object v0

    .line 247
    :goto_0
    return-object v0

    .line 242
    .restart local p1    # "jwt":Lcom/nimbusds/jwt/JWT;
    :cond_0
    instance-of v0, p1, Lcom/nimbusds/jwt/EncryptedJWT;

    if-eqz v0, :cond_1

    .line 243
    check-cast p1, Lcom/nimbusds/jwt/EncryptedJWT;

    .end local p1    # "jwt":Lcom/nimbusds/jwt/JWT;
    invoke-virtual {p0, p1, p2}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->process(Lcom/nimbusds/jwt/EncryptedJWT;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;

    move-result-object v0

    goto :goto_0

    .line 246
    .restart local p1    # "jwt":Lcom/nimbusds/jwt/JWT;
    :cond_1
    instance-of v0, p1, Lcom/nimbusds/jwt/PlainJWT;

    if-eqz v0, :cond_2

    .line 247
    check-cast p1, Lcom/nimbusds/jwt/PlainJWT;

    .end local p1    # "jwt":Lcom/nimbusds/jwt/JWT;
    invoke-virtual {p0, p1, p2}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->process(Lcom/nimbusds/jwt/PlainJWT;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;

    move-result-object v0

    goto :goto_0

    .line 251
    .restart local p1    # "jwt":Lcom/nimbusds/jwt/JWT;
    :cond_2
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected JWT object type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public process(Lcom/nimbusds/jwt/PlainJWT;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;
    .locals 1
    .param p1, "plainJWT"    # Lcom/nimbusds/jwt/PlainJWT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jwt/PlainJWT;",
            "TC;)",
            "Lcom/nimbusds/jwt/JWTClaimsSet;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/proc/BadJOSEException;,
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 259
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    invoke-direct {p0, p1}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->verifyAndReturnClaims(Lcom/nimbusds/jwt/JWT;)Lcom/nimbusds/jwt/JWTClaimsSet;

    .line 261
    sget-object v0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->PLAIN_JWT_REJECTED_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v0
.end method

.method public process(Lcom/nimbusds/jwt/SignedJWT;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;
    .locals 7
    .param p1, "signedJWT"    # Lcom/nimbusds/jwt/SignedJWT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jwt/SignedJWT;",
            "TC;)",
            "Lcom/nimbusds/jwt/JWTClaimsSet;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/proc/BadJOSEException;,
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 269
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWSKeySelector()Lcom/nimbusds/jose/proc/JWSKeySelector;

    move-result-object v4

    if-nez v4, :cond_0

    .line 271
    sget-object v4, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWS_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v4

    .line 274
    :cond_0
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWSVerifierFactory()Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    move-result-object v4

    if-nez v4, :cond_1

    .line 275
    sget-object v4, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWS_VERIFIER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

    throw v4

    .line 278
    :cond_1
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWSKeySelector()Lcom/nimbusds/jose/proc/JWSKeySelector;

    move-result-object v4

    invoke-virtual {p1}, Lcom/nimbusds/jwt/SignedJWT;->getHeader()Lcom/nimbusds/jose/JWSHeader;

    move-result-object v5

    invoke-interface {v4, v5, p2}, Lcom/nimbusds/jose/proc/JWSKeySelector;->selectJWSKeys(Lcom/nimbusds/jose/JWSHeader;Lcom/nimbusds/jose/proc/SecurityContext;)Ljava/util/List;

    move-result-object v1

    .line 280
    .local v1, "keyCandidates":Ljava/util/List;, "Ljava/util/List<+Ljava/security/Key;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 281
    :cond_2
    sget-object v4, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_JWS_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v4

    .line 284
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 286
    .local v0, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<+Ljava/security/Key;>;"
    :cond_4
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 306
    sget-object v4, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->NO_MATCHING_VERIFIERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v4

    .line 288
    :cond_5
    invoke-virtual {p0}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->getJWSVerifierFactory()Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    move-result-object v5

    invoke-virtual {p1}, Lcom/nimbusds/jwt/SignedJWT;->getHeader()Lcom/nimbusds/jose/JWSHeader;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/security/Key;

    invoke-interface {v5, v6, v4}, Lcom/nimbusds/jose/proc/JWSVerifierFactory;->createJWSVerifier(Lcom/nimbusds/jose/JWSHeader;Ljava/security/Key;)Lcom/nimbusds/jose/JWSVerifier;

    move-result-object v3

    .line 290
    .local v3, "verifier":Lcom/nimbusds/jose/JWSVerifier;
    if-eqz v3, :cond_4

    .line 294
    invoke-virtual {p1, v3}, Lcom/nimbusds/jwt/SignedJWT;->verify(Lcom/nimbusds/jose/JWSVerifier;)Z

    move-result v2

    .line 296
    .local v2, "validSignature":Z
    if-eqz v2, :cond_6

    .line 297
    invoke-direct {p0, p1}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->verifyAndReturnClaims(Lcom/nimbusds/jwt/JWT;)Lcom/nimbusds/jwt/JWTClaimsSet;

    move-result-object v4

    return-object v4

    .line 300
    :cond_6
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 302
    sget-object v4, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->INVALID_SIGNATURE:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v4
.end method

.method public process(Ljava/lang/String;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;
    .locals 1
    .param p1, "jwtString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TC;)",
            "Lcom/nimbusds/jwt/JWTClaimsSet;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;,
            Lcom/nimbusds/jose/proc/BadJOSEException;,
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 230
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    invoke-static {p1}, Lcom/nimbusds/jwt/JWTParser;->parse(Ljava/lang/String;)Lcom/nimbusds/jwt/JWT;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->process(Lcom/nimbusds/jwt/JWT;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jwt/JWTClaimsSet;

    move-result-object v0

    return-object v0
.end method

.method public setJWEDecrypterFactory(Lcom/nimbusds/jose/proc/JWEDecrypterFactory;)V
    .locals 0
    .param p1, "factory"    # Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    .prologue
    .line 177
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    iput-object p1, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jweDecrypterFactory:Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    .line 178
    return-void
.end method

.method public setJWEKeySelector(Lcom/nimbusds/jose/proc/JWEKeySelector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/proc/JWEKeySelector",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 149
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    .local p1, "jweKeySelector":Lcom/nimbusds/jose/proc/JWEKeySelector;, "Lcom/nimbusds/jose/proc/JWEKeySelector<TC;>;"
    iput-object p1, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jweKeySelector:Lcom/nimbusds/jose/proc/JWEKeySelector;

    .line 150
    return-void
.end method

.method public setJWSKeySelector(Lcom/nimbusds/jose/proc/JWSKeySelector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/proc/JWSKeySelector",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    .local p1, "jwsKeySelector":Lcom/nimbusds/jose/proc/JWSKeySelector;, "Lcom/nimbusds/jose/proc/JWSKeySelector<TC;>;"
    iput-object p1, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jwsKeySelector:Lcom/nimbusds/jose/proc/JWSKeySelector;

    .line 136
    return-void
.end method

.method public setJWSVerifierFactory(Lcom/nimbusds/jose/proc/JWSVerifierFactory;)V
    .locals 0
    .param p1, "factory"    # Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    .prologue
    .line 163
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    iput-object p1, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->jwsVerifierFactory:Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    .line 164
    return-void
.end method

.method public setJWTClaimsVerifier(Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;)V
    .locals 0
    .param p1, "claimsVerifier"    # Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;

    .prologue
    .line 191
    .local p0, "this":Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;, "Lcom/nimbusds/jwt/proc/DefaultJWTProcessor<TC;>;"
    iput-object p1, p0, Lcom/nimbusds/jwt/proc/DefaultJWTProcessor;->claimsVerifier:Lcom/nimbusds/jwt/proc/JWTClaimsVerifier;

    .line 192
    return-void
.end method
