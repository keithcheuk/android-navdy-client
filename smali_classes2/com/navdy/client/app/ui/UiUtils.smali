.class public Lcom/navdy/client/app/ui/UiUtils;
.super Ljava/lang/Object;
.source "UiUtils.java"


# static fields
.field public static final BLUR_RADIUS:F = 16.0f

.field private static rs:Landroid/renderscript/RenderScript;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/ui/UiUtils;->rs:Landroid/renderscript/RenderScript;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static blurThisBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "bitmapOriginal"    # Landroid/graphics/Bitmap;

    .prologue
    .line 42
    :try_start_0
    invoke-static {p0}, Lcom/navdy/client/app/ui/UiUtils;->blurThisBitmapTheFancyWay(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 46
    .local v0, "blurryBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    .line 43
    .end local v0    # "blurryBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 44
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {p0}, Lcom/navdy/client/app/ui/UiUtils;->blurThisBitmapTheGhettoWay(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0    # "blurryBitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method private static blurThisBitmapTheFancyWay(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "bitmapOriginal"    # Landroid/graphics/Bitmap;

    .prologue
    .line 66
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 67
    .local v0, "applicationContext":Landroid/content/Context;
    sget-object v5, Lcom/navdy/client/app/ui/UiUtils;->rs:Landroid/renderscript/RenderScript;

    if-nez v5, :cond_0

    .line 69
    invoke-static {v0}, Landroid/renderscript/RenderScript;->create(Landroid/content/Context;)Landroid/renderscript/RenderScript;

    move-result-object v5

    sput-object v5, Lcom/navdy/client/app/ui/UiUtils;->rs:Landroid/renderscript/RenderScript;

    .line 72
    :cond_0
    const/4 v2, 0x0

    .line 73
    .local v2, "input":Landroid/renderscript/Allocation;
    const/4 v3, 0x0

    .line 74
    .local v3, "output":Landroid/renderscript/Allocation;
    const/4 v4, 0x0

    .line 76
    .local v4, "script":Landroid/renderscript/ScriptIntrinsicBlur;
    :try_start_0
    sget-object v5, Lcom/navdy/client/app/ui/UiUtils;->rs:Landroid/renderscript/RenderScript;

    invoke-static {v5, p0}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v2

    .line 77
    sget-object v5, Lcom/navdy/client/app/ui/UiUtils;->rs:Landroid/renderscript/RenderScript;

    invoke-virtual {v2}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;)Landroid/renderscript/Allocation;

    move-result-object v3

    .line 78
    sget-object v5, Lcom/navdy/client/app/ui/UiUtils;->rs:Landroid/renderscript/RenderScript;

    sget-object v6, Lcom/navdy/client/app/ui/UiUtils;->rs:Landroid/renderscript/RenderScript;

    invoke-static {v6}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/renderscript/ScriptIntrinsicBlur;->create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicBlur;

    move-result-object v4

    .line 79
    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v4, v5}, Landroid/renderscript/ScriptIntrinsicBlur;->setRadius(F)V

    .line 80
    invoke-virtual {v4, v2}, Landroid/renderscript/ScriptIntrinsicBlur;->setInput(Landroid/renderscript/Allocation;)V

    .line 81
    invoke-virtual {v4, v3}, Landroid/renderscript/ScriptIntrinsicBlur;->forEach(Landroid/renderscript/Allocation;)V

    .line 82
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 83
    .local v1, "blurryBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v3, v1}, Landroid/renderscript/Allocation;->copyTo(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    if-eqz v2, :cond_1

    .line 86
    invoke-virtual {v2}, Landroid/renderscript/Allocation;->destroy()V

    .line 88
    :cond_1
    if-eqz v3, :cond_2

    .line 89
    invoke-virtual {v3}, Landroid/renderscript/Allocation;->destroy()V

    .line 91
    :cond_2
    if-eqz v4, :cond_3

    .line 92
    invoke-virtual {v4}, Landroid/renderscript/ScriptIntrinsicBlur;->destroy()V

    .line 95
    :cond_3
    return-object v1

    .line 85
    .end local v1    # "blurryBitmap":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v5

    if-eqz v2, :cond_4

    .line 86
    invoke-virtual {v2}, Landroid/renderscript/Allocation;->destroy()V

    .line 88
    :cond_4
    if-eqz v3, :cond_5

    .line 89
    invoke-virtual {v3}, Landroid/renderscript/Allocation;->destroy()V

    .line 91
    :cond_5
    if-eqz v4, :cond_6

    .line 92
    invoke-virtual {v4}, Landroid/renderscript/ScriptIntrinsicBlur;->destroy()V

    :cond_6
    throw v5
.end method

.method private static blurThisBitmapTheGhettoWay(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "bitmapOriginal"    # Landroid/graphics/Bitmap;

    .prologue
    .line 51
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 52
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v5, 0x8

    iput v5, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 53
    const/4 v3, 0x0

    .line 55
    .local v3, "stream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    .end local v3    # "stream":Ljava/io/ByteArrayOutputStream;
    .local v4, "stream":Ljava/io/ByteArrayOutputStream;
    :try_start_1
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x32

    invoke-virtual {p0, v5, v6, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 57
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 58
    .local v1, "byteArray":[B
    const/4 v5, 0x0

    array-length v6, v1

    invoke-static {v1, v5, v6, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 60
    .local v0, "blurryBitmap":Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 62
    return-object v0

    .line 60
    .end local v0    # "blurryBitmap":Landroid/graphics/Bitmap;
    .end local v1    # "byteArray":[B
    .end local v4    # "stream":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "stream":Ljava/io/ByteArrayOutputStream;
    :catchall_0
    move-exception v5

    :goto_0
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v5

    .end local v3    # "stream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "stream":Ljava/io/ByteArrayOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "stream":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "stream":Ljava/io/ByteArrayOutputStream;
    goto :goto_0
.end method

.method public static convertDpToPx(F)I
    .locals 3
    .param p0, "dp"    # F

    .prologue
    .line 34
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 35
    .local v0, "context":Landroid/content/Context;
    const/4 v1, 0x1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, p0, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    return v1
.end method

.method public static getActionBarHeight(Landroid/app/Activity;)I
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 121
    const/4 v0, 0x0

    .line 122
    .local v0, "actionBarHeight":I
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 123
    .local v1, "tv":Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x10102eb

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    iget v2, v1, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    .line 126
    :cond_0
    return v0
.end method

.method public static getDisplayHeight(Landroid/app/Activity;)I
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 114
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 115
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 116
    .local v1, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 117
    iget v2, v1, Landroid/graphics/Point;->y:I

    return v2
.end method

.method public static getGoogleMapBottomPadding(Landroid/content/res/Resources;)I
    .locals 2
    .param p0, "resources"    # Landroid/content/res/Resources;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 99
    const v0, 0x7f0b0088

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const v1, 0x7f0b012f

    .line 100
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    const v1, 0x7f0b00bb

    .line 101
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static getHereMapBottomPadding(Landroid/content/res/Resources;)I
    .locals 2
    .param p0, "resources"    # Landroid/content/res/Resources;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 105
    const v0, 0x7f0b0134

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const v1, 0x7f0b00d6

    .line 106
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static getTabsHeight(Landroid/content/res/Resources;)I
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 110
    const v0, 0x7f0b0132

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method
