.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/CustomItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "sharedPrefKey"    # Ljava/lang/String;
    .param p2, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 358
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 359
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 360
    .local v0, "sharedPrefEditor":Landroid/content/SharedPreferences$Editor;
    const/4 v2, 0x1

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 361
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 362
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v2, :cond_0

    .line 363
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->rebuildSuggestions()V

    .line 365
    :cond_0
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 366
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;I)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v8, 0x0

    .line 193
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "position clicked: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 194
    iget-object v9, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v9, v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-nez v9, :cond_1

    .line 195
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "onClick with a null adapter! WTF!"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v9, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v9, v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v9, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v7

    .line 201
    .local v7, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    if-nez p2, :cond_3

    const/4 v3, 0x1

    .line 203
    .local v3, "isHeader":Z
    :goto_1
    if-nez v3, :cond_2

    instance-of v9, p1, Lcom/navdy/client/app/ui/customviews/TripCardView;

    if-eqz v9, :cond_6

    .line 204
    :cond_2
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v8}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$200(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->isInOneOfThePendingTripStates()Z

    move-result v5

    .line 205
    .local v5, "isInPendingTrip":Z
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v8}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$200(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->isInOneOfTheActiveTripStates()Z

    move-result v4

    .line 207
    .local v4, "isInActiveTrip":Z
    if-eqz v5, :cond_4

    .line 208
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v8, v8, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iget-object v9, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->clickPendingTripCard(Landroid/content/Context;)V

    goto :goto_0

    .end local v3    # "isHeader":Z
    .end local v4    # "isInActiveTrip":Z
    .end local v5    # "isInPendingTrip":Z
    :cond_3
    move v3, v8

    .line 201
    goto :goto_1

    .line 209
    .restart local v3    # "isHeader":Z
    .restart local v4    # "isInActiveTrip":Z
    .restart local v5    # "isInPendingTrip":Z
    :cond_4
    if-eqz v4, :cond_5

    .line 210
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v8, v8, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v8}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->clickActiveTripCard()V

    goto :goto_0

    .line 212
    :cond_5
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    new-instance v9, Landroid/content/Intent;

    iget-object v10, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v10}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getContext()Landroid/content/Context;

    move-result-object v10

    const-class v11, Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-direct {v9, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 218
    .end local v4    # "isInActiveTrip":Z
    .end local v5    # "isInPendingTrip":Z
    :cond_6
    if-eqz v7, :cond_0

    .line 219
    iget-object v9, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .line 221
    .local v0, "activity":Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Suggestion;->getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-result-object v9

    sget-object v10, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->LOADING:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v9, v10, :cond_0

    .line 226
    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Suggestion;->isTip()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 227
    sget-object v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$12;->$SwitchMap$com$navdy$client$app$framework$models$Suggestion$SuggestionType:[I

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Suggestion;->getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 230
    const-string v8, "user_enabled_glances_once_before"

    new-instance v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$1;

    invoke-direct {v9, p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$1;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 240
    :pswitch_0
    const-string v8, "user_already_saw_microphone_tip"

    new-instance v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$2;

    invoke-direct {v9, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$2;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;)V

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 249
    :pswitch_1
    const-string v8, "user_already_saw_microphone_tip"

    new-instance v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$3;

    invoke-direct {v9, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$3;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;)V

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 259
    :pswitch_2
    const-string v8, "user_already_saw_google_now_tip"

    new-instance v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$4;

    invoke-direct {v9, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$4;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;)V

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 269
    :pswitch_3
    const-string v8, "user_already_saw_music_tip"

    new-instance v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$5;

    invoke-direct {v9, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$5;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;)V

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 279
    :pswitch_4
    const-string v8, "user_already_saw_gesture_tip"

    new-instance v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$6;

    invoke-direct {v9, p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$6;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 287
    :pswitch_5
    const-string v8, "user_tried_gestures_once_before"

    new-instance v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$7;

    invoke-direct {v9, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$7;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;)V

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 296
    :pswitch_6
    const-string v8, "user_already_saw_add_home_tip"

    new-instance v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$8;

    invoke-direct {v9, p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$8;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 308
    :pswitch_7
    const-string v8, "user_already_saw_add_work_tip"

    new-instance v9, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$9;

    invoke-direct {v9, p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$9;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->handleTipClick(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 320
    :pswitch_8
    sget-object v9, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->FAVORITE:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-static {v9, v8, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->startSearchActivityFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;ZLandroid/app/Activity;)V

    goto/16 :goto_0

    .line 326
    :pswitch_9
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    new-instance v9, Landroid/content/Intent;

    iget-object v10, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v10}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getContext()Landroid/content/Context;

    move-result-object v10

    const-class v11, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-direct {v9, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v8, v9}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 329
    :pswitch_a
    new-instance v2, Landroid/content/Intent;

    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v8}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getContext()Landroid/content/Context;

    move-result-object v8

    const-class v9, Lcom/navdy/client/app/ui/homescreen/DemoVideoDialogActivity;

    invoke-direct {v2, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 330
    .local v2, "i":Landroid/content/Intent;
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v8, v2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 333
    .end local v2    # "i":Landroid/content/Intent;
    :cond_7
    iget-object v1, v7, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 334
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "destination object status: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 336
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v6

    .line 337
    .local v6, "setDestinationTracker":Lcom/navdy/client/app/tracking/SetDestinationTracker;
    const-string v8, "Suggestion_List"

    invoke-virtual {v6, v8}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setSourceValue(Ljava/lang/String;)V

    .line 338
    invoke-static {v7}, Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes$TYPE_VALUES;->getSetDestinationTypeFromSuggestion(Lcom/navdy/client/app/framework/models/Suggestion;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 340
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v8

    const v9, 0x7f1002f5

    if-ne v8, v9, :cond_8

    .line 341
    instance-of v8, p1, Landroid/widget/ImageButton;

    if-eqz v8, :cond_0

    .line 342
    invoke-static {v1, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->startDetailsActivity(Lcom/navdy/client/app/framework/models/Destination;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 345
    :cond_8
    new-instance v8, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$10;

    invoke-direct {v8, p0, v6, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3$10;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;Lcom/navdy/client/app/tracking/SetDestinationTracker;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-virtual {v0, v8}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showRequestNewRouteDialog(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
