.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$9;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->addTripRouteAndCenterMap(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

.field final synthetic val$route:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 716
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$9;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$9;->val$route:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 727
    return-void
.end method

.method public onReady(Lcom/here/android/mpa/mapping/Map;)V
    .locals 3
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 719
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$9;->val$route:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getRoute()Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v0

    .line 720
    .local v0, "geoPolyline":Lcom/here/android/mpa/common/GeoPolyline;
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$9;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v1, p1, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$900(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V

    .line 721
    if-eqz v0, :cond_0

    .line 722
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$9;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$800(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    move-result-object v1

    sget-object v2, Lcom/here/android/mpa/mapping/Map$Animation;->LINEAR:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual {v1, v0, v2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnRoute(Lcom/here/android/mpa/common/GeoPolyline;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 724
    :cond_0
    return-void
.end method
