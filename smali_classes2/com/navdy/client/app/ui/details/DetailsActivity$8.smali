.class Lcom/navdy/client/app/ui/details/DetailsActivity$8;
.super Landroid/os/AsyncTask;
.source "DetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/details/DetailsActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Lcom/navdy/client/app/framework/models/Destination;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 387
    iput-object p1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$8;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 391
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$8;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$100(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$8;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$100(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iget v0, v0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 394
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 387
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/details/DetailsActivity$8;->doInBackground([Ljava/lang/Object;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "d"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 399
    if-eqz p1, :cond_0

    .line 400
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$8;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$102(Lcom/navdy/client/app/ui/details/DetailsActivity;Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$8;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$1200(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    .line 403
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$8;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$300(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    .line 404
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 387
    check-cast p1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/details/DetailsActivity$8;->onPostExecute(Lcom/navdy/client/app/framework/models/Destination;)V

    return-void
.end method
