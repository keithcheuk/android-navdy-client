.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;
.super Lcom/navdy/client/app/framework/util/CarMdCallBack;
.source "EditCarInfoUsingWebApiActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/CarMdCallBack;-><init>()V

    return-void
.end method


# virtual methods
.method public processFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 238
    return-void
.end method

.method public processResponse(Lokhttp3/ResponseBody;)V
    .locals 6
    .param p1, "responseBody"    # Lokhttp3/ResponseBody;

    .prologue
    .line 195
    :try_start_0
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v1

    .line 196
    .local v1, "jsonString":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

    invoke-virtual {v3, v1}, Lcom/navdy/client/app/framework/util/CarMdClient;->getListFromJsonResponse(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 197
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 198
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    new-instance v4, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;

    invoke-direct {v4, p0, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 226
    .end local v1    # "jsonString":Ljava/lang/String;
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-void

    .line 218
    .restart local v1    # "jsonString":Ljava/lang/String;
    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Empty make list!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    .end local v1    # "jsonString":Ljava/lang/String;
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$700(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Car MD: unable to get the list of years out of the response body: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->processFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
