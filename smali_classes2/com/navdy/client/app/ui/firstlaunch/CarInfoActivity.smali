.class public Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "CarInfoActivity.java"


# static fields
.field public static final DEFAULT_OBD_LOCATION:I

.field private static final obdLocations:[I


# instance fields
.field private nextStep:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->obdLocations:[I

    .line 45
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->obdLocations:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->DEFAULT_OBD_LOCATION:I

    return-void

    .line 33
    nop

    :array_0
    .array-data 4
        0x7f0202ae
        0x7f0202af
        0x7f0202b0
        0x7f0202b1
        0x7f0202b2
        0x7f0202b3
        0x7f0202b4
        0x7f0202b5
        0x7f0202b6
        0x7f020244
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method

.method private getLocalizedLocationDescription(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "accessNote"    # Ljava/lang/String;
    .param p2, "note"    # Ljava/lang/String;

    .prologue
    .line 126
    invoke-static {p1}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->getLocalizedAccessNote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 127
    invoke-static {p2}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->getLocalizedNote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 128
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 130
    .local v0, "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 131
    move-object v1, p2

    .line 135
    .local v1, "obdLocationString":Ljava/lang/String;
    :goto_0
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    const v2, 0x7f080318

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 138
    :cond_0
    return-object v1

    .line 133
    .end local v1    # "obdLocationString":Ljava/lang/String;
    :cond_1
    const v2, 0x7f0804a2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "obdLocationString":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->finish()V

    .line 207
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 168
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 171
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 172
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->hideSystemUI()V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->showSystemUI()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super/range {p0 .. p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v17, 0x7f030082

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->setContentView(I)V

    .line 57
    const v17, 0x7f100181

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 58
    .local v15, "yearMakeModel":Landroid/widget/TextView;
    const v17, 0x7f100193

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 59
    .local v13, "obdLocation":Landroid/widget/ImageView;
    const v17, 0x7f100213

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 60
    .local v12, "obdCrimePhoto":Landroid/widget/ImageView;
    const v17, 0x7f100217

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 61
    .local v8, "help":Landroid/widget/TextView;
    const v17, 0x7f100215

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 64
    .local v7, "done":Landroid/widget/Button;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    .line 66
    .local v6, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v17, "vehicle-make"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 67
    .local v10, "makeString":Ljava/lang/String;
    const-string v17, "vehicle-year"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 68
    .local v16, "yearString":Ljava/lang/String;
    const-string v17, "vehicle-model"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 69
    .local v11, "modelString":Ljava/lang/String;
    const-string v17, "CAR_OBD_LOCATION_NUM"

    sget v18, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->DEFAULT_OBD_LOCATION:I

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v14

    .line 70
    .local v14, "obdLocationNumber":I
    const v17, 0x7f0802eb

    invoke-static/range {v17 .. v17}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v9

    .line 72
    .local v9, "helpString":Landroid/text/Spanned;
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    const-string v18, "extra_next_step"

    invoke-virtual/range {v17 .. v18}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->nextStep:Ljava/lang/String;

    .line 75
    invoke-static/range {v16 .. v16}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 76
    invoke-static {v10}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 77
    invoke-static {v11}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 78
    :cond_0
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->startEditInfoActivity(Landroid/view/View;)V

    .line 123
    :cond_1
    :goto_0
    return-void

    .line 83
    :cond_2
    if-eqz v15, :cond_3

    .line 84
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCarYearMakeModelString()Ljava/lang/String;

    move-result-object v4

    .line 85
    .local v4, "carInfoString":Ljava/lang/String;
    invoke-virtual {v15, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    .end local v4    # "carInfoString":Ljava/lang/String;
    :cond_3
    if-eqz v13, :cond_5

    .line 89
    if-ltz v14, :cond_4

    sget-object v17, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->obdLocations:[I

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v14, v0, :cond_9

    .line 90
    :cond_4
    sget-object v17, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->obdLocations:[I

    sget v18, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->DEFAULT_OBD_LOCATION:I

    aget v17, v17, v18

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 96
    :cond_5
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->getObdImage()Landroid/graphics/Bitmap;

    move-result-object v5

    .line 97
    .local v5, "crimePhoto":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_6

    .line 98
    if-eqz v5, :cond_a

    .line 99
    invoke-virtual {v12, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 105
    :cond_6
    :goto_2
    if-eqz v8, :cond_7

    .line 106
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    :cond_7
    new-instance v17, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v18, 0x7f080317

    .line 110
    invoke-virtual/range {v17 .. v18}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v17

    .line 111
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 113
    if-eqz v7, :cond_1

    .line 114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->nextStep:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "installation_flow"

    invoke-static/range {v17 .. v18}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->nextStep:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "app_setup"

    .line 115
    invoke-static/range {v17 .. v18}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 116
    :cond_8
    const v17, 0x7f0802f7

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setText(I)V

    .line 117
    const/16 v17, 0x0

    const/16 v18, 0x0

    const v19, 0x7f02016f

    const/16 v20, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    goto/16 :goto_0

    .line 92
    .end local v5    # "crimePhoto":Landroid/graphics/Bitmap;
    :cond_9
    sget-object v17, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->obdLocations:[I

    aget v17, v17, v14

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 101
    .restart local v5    # "crimePhoto":Landroid/graphics/Bitmap;
    :cond_a
    const v17, 0x7f0201e7

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 119
    :cond_b
    const v17, 0x7f080138

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setText(I)V

    .line 120
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    goto/16 :goto_0
.end method

.method public onCrimePhotoClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 226
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/PhotoViewerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 227
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_FILE_PATH"

    const-string v2, "obdImage"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 229
    return-void
.end method

.method public onDoneClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 213
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->nextStep:Ljava/lang/String;

    const-string v2, "installation_flow"

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_step"

    const v2, 0x7f030077

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 216
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 220
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->finish()V

    goto :goto_0
.end method

.method public onFlashlightClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 193
    invoke-static {}, Lcom/navdy/client/app/framework/FlashlightManager;->getInstance()Lcom/navdy/client/app/framework/FlashlightManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/FlashlightManager;->switchLight(Landroid/app/Activity;)V

    .line 194
    return-void
.end method

.method public onHelpClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 200
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 201
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "default_problem_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 202
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 203
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 162
    invoke-static {}, Lcom/navdy/client/app/framework/FlashlightManager;->getInstance()Lcom/navdy/client/app/framework/FlashlightManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/FlashlightManager;->turnFlashLightOff()V

    .line 163
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onPause()V

    .line 164
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    .line 143
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 144
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->hideSystemUiDependingOnOrientation()V

    .line 146
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 147
    .local v1, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v5, "car_obd_location_access_note"

    const-string v6, ""

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "accessNote":Ljava/lang/String;
    const-string v5, "car_obd_location_note"

    const-string v6, ""

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 150
    .local v2, "note":Ljava/lang/String;
    invoke-direct {p0, v0, v2}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->getLocalizedLocationDescription(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 152
    .local v4, "obdLocationString":Ljava/lang/String;
    const v5, 0x7f100194

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 153
    .local v3, "obdLocationDesc":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 154
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    :cond_0
    const-string v5, "First_Launch_Car_Info"

    invoke-static {v5}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public onWatchVideoClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 232
    const v0, 0x7f030071

    invoke-static {v0, p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startVideoPlayerForThisStep(ILandroid/app/Activity;)V

    .line 233
    return-void
.end method

.method public startEditInfoActivity(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 184
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 185
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 186
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->finish()V

    .line 187
    return-void
.end method
