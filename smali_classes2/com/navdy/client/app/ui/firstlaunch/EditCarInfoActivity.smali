.class public Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "EditCarInfoActivity.java"


# static fields
.field static final APP_SETUP:Ljava/lang/String; = "app_setup"

.field static final EXTRA_NEXT_STEP:Ljava/lang/String; = "extra_next_step"

.field static final INSTALL:Ljava/lang/String; = "installation_flow"

.field private static final OTHER_DIALOG_ID:I = 0x1

.field private static final STATE_SELECTOR_MODE:Ljava/lang/String; = "state_selector_mode"


# instance fields
.field private ignoreTheNextMakeSelection:Z

.field private ignoreTheNextModelSelection:Z

.field private ignoreTheNextYearSelection:Z

.field private isInSelectorMode:Z

.field private make:Landroid/widget/Spinner;

.field final makeSelectionListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private makeString:Ljava/lang/String;

.field private makeTv:Landroid/widget/EditText;

.field private model:Landroid/widget/Spinner;

.field final modelSelectionListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private modelString:Ljava/lang/String;

.field private modelTv:Landroid/widget/EditText;

.field private year:Landroid/widget/Spinner;

.field private yearMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;",
            ">;>;>;"
        }
    .end annotation
.end field

.field final yearSelectionListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private yearString:Ljava/lang/String;

.field private yearTv:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearMap:Ljava/util/HashMap;

    .line 62
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextYearSelection:Z

    .line 63
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextMakeSelection:Z

    .line 64
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextModelSelection:Z

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->isInSelectorMode:Z

    .line 216
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearSelectionListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 246
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$3;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$3;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeSelectionListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 276
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$4;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$4;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelSelectionListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-void
.end method

.method static synthetic access$002(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearMap:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->isInSelectorMode:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextModelSelection:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextModelSelection:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->goToNextStep()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->initCarSelectorScreen()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextYearSelection:Z

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextYearSelection:Z

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->showDialogAboutOther()V

    return-void
.end method

.method static synthetic access$702(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->updateSpinnersWithSelection()V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextMakeSelection:Z

    return v0
.end method

.method static synthetic access$902(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextMakeSelection:Z

    return p1
.end method

.method private goToNextStep()V
    .locals 3

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_next_step"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 466
    .local v0, "nextStep":Ljava/lang/String;
    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$7;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 486
    return-void
.end method

.method private initCarSelectorScreen()V
    .locals 3

    .prologue
    .line 186
    const v1, 0x7f10020f

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    .line 187
    const v1, 0x7f100210

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    .line 188
    const v1, 0x7f100211

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    .line 190
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    if-nez v1, :cond_1

    .line 191
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Missing layout element."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 213
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    .line 196
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    .line 197
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 199
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 200
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v1, "vehicle-year"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    .line 201
    const-string v1, "vehicle-make"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    .line 202
    const-string v1, "vehicle-model"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    .line 206
    .end local v0    # "customerPrefs":Landroid/content/SharedPreferences;
    :cond_2
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->updateSpinnersWithSelection()V

    .line 208
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearSelectionListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 209
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeSelectionListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 210
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelSelectionListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 212
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->isInSelectorMode:Z

    goto :goto_0
.end method

.method private initManualEntryScreen()V
    .locals 3

    .prologue
    .line 500
    const v1, 0x7f100207

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearTv:Landroid/widget/EditText;

    .line 501
    const v1, 0x7f100209

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeTv:Landroid/widget/EditText;

    .line 502
    const v1, 0x7f10020b

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelTv:Landroid/widget/EditText;

    .line 504
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearTv:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeTv:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelTv:Landroid/widget/EditText;

    if-nez v1, :cond_1

    .line 505
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Missing layout element."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 525
    :goto_0
    return-void

    .line 509
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    .line 510
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    .line 511
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 513
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 514
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v1, "vehicle-year"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    .line 515
    const-string v1, "vehicle-make"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    .line 516
    const-string v1, "vehicle-model"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    .line 520
    .end local v0    # "customerPrefs":Landroid/content/SharedPreferences;
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearTv:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 521
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeTv:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 522
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelTv:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 524
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->isInSelectorMode:Z

    goto :goto_0
.end method

.method public static saveCarInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 10
    .param p0, "customerPrefs"    # Landroid/content/SharedPreferences;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "yearString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "makeString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "modelString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "comesFromManualEntry"    # Z
    .param p5, "isInFle"    # Z

    .prologue
    .line 569
    const-string v6, "vehicle-year"

    const-string v7, ""

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 570
    .local v3, "oldYearString":Ljava/lang/String;
    const-string v6, "vehicle-make"

    const-string v7, ""

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 571
    .local v1, "oldMakeString":Ljava/lang/String;
    const-string v6, "vehicle-model"

    const-string v7, ""

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 573
    .local v2, "oldModelString":Ljava/lang/String;
    invoke-static {v3, p1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 574
    invoke-static {v1, p2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 575
    invoke-static {v2, p3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 577
    :cond_0
    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Resetting car md data."

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 578
    invoke-static {p0}, Lcom/navdy/client/app/tracking/Tracker;->resetPhotoAndLocation(Landroid/content/SharedPreferences;)V

    .line 580
    new-instance v0, Ljava/util/HashMap;

    const/4 v6, 0x3

    invoke-direct {v0, v6}, Ljava/util/HashMap;-><init>(I)V

    .line 582
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "Car_Year"

    invoke-virtual {v0, v6, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    const-string v6, "Car_Make"

    invoke-virtual {v0, v6, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    const-string v6, "Car_Model"

    invoke-virtual {v0, v6, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    const-string v7, "During_Fle"

    if-eqz p5, :cond_2

    const-string v6, "True"

    :goto_0
    invoke-virtual {v0, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 586
    const-string v6, "Car_Info_Changed"

    invoke-static {v6, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 589
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    const-string v6, "profile_serial_number"

    sget-object v7, Lcom/navdy/client/app/ui/settings/SettingsConstants$ProfilePreferences;->SERIAL_NUM_DEFAULT:Ljava/lang/Long;

    .line 590
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 589
    invoke-interface {p0, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 592
    .local v4, "serial":J
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 593
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    .line 594
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p3

    .line 597
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "vehicle-year"

    .line 598
    invoke-interface {v6, v7, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "vehicle-make"

    .line 599
    invoke-interface {v6, v7, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "vehicle-model"

    .line 600
    invoke-interface {v6, v7, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "vehicle-data-entry-manual"

    .line 601
    invoke-interface {v6, v7, p4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "profile_serial_number"

    const-wide/16 v8, 0x1

    add-long/2addr v8, v4

    .line 602
    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 603
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 605
    const-string v6, "Car_Year"

    invoke-static {v6, p1}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    const-string v6, "Car_Make"

    invoke-static {v6, p2}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    const-string v6, "Car_Model"

    invoke-static {v6, p3}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    const-string v6, "Vehicle_Data_Entry_Manual"

    invoke-static {p4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    return-void

    .line 585
    .end local v4    # "serial":J
    .restart local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    const-string v6, "False"

    goto :goto_0
.end method

.method public static saveObdInfo(Landroid/content/SharedPreferences;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;)V
    .locals 3
    .param p0, "customerPrefs"    # Landroid/content/SharedPreferences;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "obdLocation"    # Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 613
    if-nez p1, :cond_0

    .line 614
    invoke-static {p0}, Lcom/navdy/client/app/tracking/Tracker;->resetPhotoAndLocation(Landroid/content/SharedPreferences;)V

    .line 624
    :goto_0
    return-void

    .line 619
    :cond_0
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "car_obd_location_note"

    iget-object v2, p1, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;->note:Ljava/lang/String;

    .line 620
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "car_obd_location_access_note"

    iget-object v2, p1, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;->accessNote:Ljava/lang/String;

    .line 621
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAR_OBD_LOCATION_NUM"

    iget v2, p1, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;->location:I

    .line 622
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 623
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method private showDialogAboutOther()V
    .locals 3

    .prologue
    .line 489
    const/4 v0, 0x1

    const v1, 0x7f08029b

    .line 490
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08029c

    .line 491
    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 489
    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 492
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->showManualEntry(Landroid/view/View;)V

    .line 493
    return-void
.end method

.method private updateSpinnersWithSelection()V
    .locals 23

    .prologue
    .line 306
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextYearSelection:Z

    .line 307
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextMakeSelection:Z

    .line 308
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->ignoreTheNextModelSelection:Z

    .line 310
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 313
    .local v2, "context":Landroid/content/Context;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 314
    .local v16, "yearList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v20, 0x7f08046c

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearMap:Ljava/util/HashMap;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v18

    .line 316
    .local v18, "years":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 317
    .local v19, "yearsSorted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 318
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 319
    const v20, 0x7f080326

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    new-instance v15, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;-><init>(Landroid/widget/Spinner;Ljava/util/List;)V

    .line 321
    .local v15, "yearAdapter":Lorg/droidparts/adapter/widget/StringSpinnerAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 322
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "setting year list to: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v17

    .line 324
    .local v17, "yearSelection":I
    if-ltz v17, :cond_0

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 329
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 330
    .local v4, "makeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v20, 0x7f080467

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearMap:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    .line 333
    .local v5, "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    if-eqz v5, :cond_1

    .line 334
    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    .line 335
    .local v7, "makes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 336
    .local v8, "makesSorted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 337
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 340
    .end local v5    # "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    .end local v7    # "makes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v8    # "makesSorted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    const v20, 0x7f080326

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    new-instance v3, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v3, v0, v4}, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;-><init>(Landroid/widget/Spinner;Ljava/util/List;)V

    .line 342
    .local v3, "makeAdapter":Lorg/droidparts/adapter/widget/StringSpinnerAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "setting make list to: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 345
    .local v6, "makeSelection":I
    if-ltz v6, :cond_2

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setSelection(I)V

    .line 350
    :cond_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 351
    .local v10, "modelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v20, 0x7f080468

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_3

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearMap:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    .line 354
    .restart local v5    # "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    if-eqz v5, :cond_3

    .line 355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/HashMap;

    .line 356
    .local v11, "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    if-eqz v11, :cond_3

    .line 357
    invoke-virtual {v11}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v13

    .line 358
    .local v13, "models":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 359
    .local v14, "modelsSorted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v14}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 360
    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 364
    .end local v5    # "makeMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;>;"
    .end local v11    # "modelMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;>;"
    .end local v13    # "models":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v14    # "modelsSorted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    const v20, 0x7f080326

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    new-instance v9, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v9, v0, v10}, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;-><init>(Landroid/widget/Spinner;Ljava/util/List;)V

    .line 366
    .local v9, "modelAdapter":Lorg/droidparts/adapter/widget/StringSpinnerAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "setting model list to: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v12

    .line 369
    .local v12, "modelSelection":I
    if-ltz v12, :cond_4

    .line 370
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/widget/Spinner;->setSelection(I)V

    .line 372
    :cond_4
    return-void
.end method


# virtual methods
.method public fieldsAreValid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "yearString"    # Ljava/lang/String;
    .param p2, "makeString"    # Ljava/lang/String;
    .param p3, "modelString"    # Ljava/lang/String;

    .prologue
    .line 556
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "[12][0-9]{3}"

    .line 557
    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    invoke-static {p3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 460
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->finish()V

    .line 461
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 70
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    const v5, 0x7f030081

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->setContentView(I)V

    .line 73
    new-instance v5, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v6, 0x7f0803fd

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 75
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->hasCachedCarList()Z

    move-result v5

    if-nez v5, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->showProgressDialog()V

    .line 79
    :cond_0
    if-eqz p1, :cond_4

    .line 80
    const-string v5, "vehicle-year"

    iget-object v6, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "yr":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 83
    iput-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    .line 85
    :cond_1
    const-string v5, "vehicle-make"

    iget-object v6, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "ma":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 88
    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    .line 90
    :cond_2
    const-string v5, "vehicle-model"

    iget-object v6, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "mo":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 93
    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    .line 96
    :cond_3
    const-string v5, "state_selector_mode"

    invoke-virtual {p1, v5, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v3, :cond_5

    :goto_0
    iput-boolean v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->isInSelectorMode:Z

    .line 99
    .end local v0    # "ma":Ljava/lang/String;
    .end local v1    # "mo":Ljava/lang/String;
    .end local v2    # "yr":Ljava/lang/String;
    :cond_4
    new-instance v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V

    new-array v4, v4, [Ljava/lang/Void;

    .line 120
    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 121
    return-void

    .restart local v0    # "ma":Ljava/lang/String;
    .restart local v1    # "mo":Ljava/lang/String;
    .restart local v2    # "yr":Ljava/lang/String;
    :cond_5
    move v3, v4

    .line 96
    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 166
    const-string v0, "First_Launch_Edit_Car_Info"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 127
    iget-boolean v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->isInSelectorMode:Z

    if-eqz v3, :cond_5

    .line 128
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    if-nez v3, :cond_1

    .line 129
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Missing layout element."

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 160
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    .line 134
    .local v2, "yearInt":I
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 135
    .local v0, "makeInt":I
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    .line 136
    .local v1, "modelInt":I
    if-eqz v2, :cond_2

    .line 137
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    .line 139
    :cond_2
    if-eqz v0, :cond_3

    .line 140
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    .line 142
    :cond_3
    if-eqz v1, :cond_4

    .line 143
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    .line 156
    .end local v0    # "makeInt":I
    .end local v1    # "modelInt":I
    .end local v2    # "yearInt":I
    :cond_4
    :goto_1
    const-string v3, "vehicle-year"

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v3, "vehicle-make"

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v3, "vehicle-model"

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v4, "state_selector_mode"

    iget-boolean v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->isInSelectorMode:Z

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_2
    invoke-virtual {p1, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 146
    :cond_5
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearTv:Landroid/widget/EditText;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeTv:Landroid/widget/EditText;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelTv:Landroid/widget/EditText;

    if-nez v3, :cond_7

    .line 147
    :cond_6
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Missing layout element."

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_7
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearTv:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearString:Ljava/lang/String;

    .line 152
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeTv:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->makeString:Ljava/lang/String;

    .line 153
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelTv:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->modelString:Ljava/lang/String;

    goto :goto_1

    .line 159
    :cond_8
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public onSetCarInfoClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 533
    const v0, 0x7f100207

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    .line 534
    .local v8, "year":Landroid/widget/EditText;
    const v0, 0x7f100209

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    .line 535
    .local v6, "make":Landroid/widget/EditText;
    const v0, 0x7f10020b

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    .line 537
    .local v7, "model":Landroid/widget/EditText;
    if-eqz v8, :cond_0

    if-eqz v6, :cond_0

    if-nez v7, :cond_1

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Missing layout element."

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 553
    :goto_0
    return-void

    .line 542
    :cond_1
    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 543
    .local v2, "yearString":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 544
    .local v3, "makeString":Ljava/lang/String;
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 546
    .local v4, "modelString":Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->fieldsAreValid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 547
    const v0, 0x7f0803a3

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0

    .line 551
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 552
    .local v1, "customerPrefs":Landroid/content/SharedPreferences;
    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->saveCarInfoAndDownloadObdInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onSetCarMdInfoClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 380
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    if-nez v0, :cond_1

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Missing layout element."

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 407
    :goto_0
    return-void

    .line 385
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 386
    .local v1, "customerPrefs":Landroid/content/SharedPreferences;
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v8

    .line 387
    .local v8, "yearInt":I
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v6

    .line 388
    .local v6, "makeInt":I
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v7

    .line 389
    .local v7, "modelInt":I
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 390
    .local v2, "yearString":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 391
    .local v3, "makeString":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 393
    .local v4, "modelString":Ljava/lang/String;
    if-eqz v8, :cond_2

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->year:Landroid/widget/Spinner;

    .line 394
    invoke-virtual {v0}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_2

    if-eqz v6, :cond_2

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->make:Landroid/widget/Spinner;

    .line 396
    invoke-virtual {v0}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v6, v0, :cond_2

    if-eqz v7, :cond_2

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->model:Landroid/widget/Spinner;

    .line 398
    invoke-virtual {v0}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v7, v0, :cond_2

    .line 399
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 400
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 401
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 402
    :cond_2
    const v0, 0x7f0803a3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move-object v0, p0

    .line 406
    invoke-virtual/range {v0 .. v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->saveCarInfoAndDownloadObdInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public saveCarInfoAndDownloadObdInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1, "customerPrefs"    # Landroid/content/SharedPreferences;
    .param p2, "yearString"    # Ljava/lang/String;
    .param p3, "makeString"    # Ljava/lang/String;
    .param p4, "modelString"    # Ljava/lang/String;
    .param p5, "comesFromManualEntry"    # Z

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->showProgressDialog()V

    .line 418
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_next_step"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 419
    .local v6, "nextStep":Ljava/lang/String;
    const-string v0, "installation_flow"

    invoke-static {v6, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    .line 422
    .local v5, "isInFle":Z
    invoke-static {p1}, Lcom/navdy/client/app/tracking/Tracker;->resetPhotoAndLocation(Landroid/content/SharedPreferences;)V

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    .line 425
    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->saveCarInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 428
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->yearMap:Ljava/util/HashMap;

    invoke-static {p2, p3, p4, v0}, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;->getObdLocation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;

    move-result-object v7

    .line 431
    .local v7, "obdLocation":Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
    invoke-static {p1, v7}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->saveObdInfo(Landroid/content/SharedPreferences;Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;)V

    .line 434
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$5;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$5;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 445
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$6;

    invoke-direct {v1, p0, p2, p3, p4}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$6;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 457
    return-void
.end method

.method public showCarSelector(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 176
    const v0, 0x7f030081

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->setContentView(I)V

    .line 177
    const v0, 0x7f1000b1

    const v1, 0x7f0201e0

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->loadImage(II)V

    .line 178
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->initCarSelectorScreen()V

    .line 179
    return-void
.end method

.method public showManualEntry(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 170
    const v0, 0x7f030080

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->setContentView(I)V

    .line 171
    const v0, 0x7f1000b1

    const v1, 0x7f0201e0

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->loadImage(II)V

    .line 172
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->initManualEntryScreen()V

    .line 173
    return-void
.end method
