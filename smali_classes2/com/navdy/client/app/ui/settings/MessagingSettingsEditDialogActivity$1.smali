.class Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;
.super Ljava/lang/Object;
.source "MessagingSettingsEditDialogActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final animShake:Landroid/view/animation/Animation;

.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)V
    .locals 2
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    const v1, 0x7f040012

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->animShake:Landroid/view/animation/Animation;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 8
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v4, 0x0

    .line 65
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v1, 0x1

    .line 66
    .local v1, "messageIsValid":Z
    :goto_0
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->access$000(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)Landroid/widget/Button;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 67
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-virtual {v5}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0015

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 68
    .local v0, "maxChars":I
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v5

    if-lt v5, v0, :cond_1

    .line 69
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    const-string v6, "vibrator"

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Vibrator;

    .line 70
    .local v3, "systemVibratorService":Landroid/os/Vibrator;
    const-wide/16 v6, 0x64

    invoke-virtual {v3, v6, v7}, Landroid/os/Vibrator;->vibrate(J)V

    .line 71
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->access$100(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)Landroid/widget/EditText;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->animShake:Landroid/view/animation/Animation;

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->startAnimation(Landroid/view/animation/Animation;)V

    .line 72
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->access$100(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    .line 73
    .local v2, "selectionEnd":I
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->access$100(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {p1, v4, v6}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->access$100(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)Landroid/widget/EditText;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->access$100(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->length()I

    move-result v5

    if-le v2, v5, :cond_0

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;->access$100(Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->length()I

    move-result v2

    .end local v2    # "selectionEnd":I
    :cond_0
    invoke-virtual {v4, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 76
    .end local v3    # "systemVibratorService":Landroid/os/Vibrator;
    :cond_1
    return-void

    .end local v0    # "maxChars":I
    .end local v1    # "messageIsValid":Z
    :cond_2
    move v1, v4

    .line 65
    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 57
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 61
    return-void
.end method
