.class Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;
.super Landroid/widget/ArrayAdapter;
.source "GlancesAppAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/content/pm/ApplicationInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final appsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private glancesAreEnabled:Z

.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final packageManager:Landroid/content/pm/PackageManager;

.field private sharedPrefs:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "textViewResourceId"    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "appsList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 42
    iput-boolean v2, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->glancesAreEnabled:Z

    .line 48
    iput-object p3, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->appsList:Ljava/util/List;

    .line 50
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 51
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->sharedPrefs:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v1, "glances"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->glancesAreEnabled:Z

    .line 55
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->packageManager:Landroid/content/pm/PackageManager;

    .line 57
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 58
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->appsList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->appsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Landroid/content/pm/ApplicationInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->appsList:Ljava/util/List;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->appsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 70
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->getItem(I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 75
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 82
    if-eqz p2, :cond_0

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 83
    :cond_0
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030027

    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 86
    :cond_1
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 88
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->appsList:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 89
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->appsList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ApplicationInfo;

    .line 90
    .local v1, "data":Landroid/content/pm/ApplicationInfo;
    if-eqz v1, :cond_3

    .line 91
    const v4, 0x7f1000e0

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    .line 92
    .local v0, "appName":Landroid/widget/Switch;
    if-eqz v0, :cond_2

    .line 93
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-boolean v4, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->glancesAreEnabled:Z

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 97
    new-instance v4, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;

    invoke-direct {v4, p0, v1, v0}, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;-><init>(Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;Landroid/content/pm/ApplicationInfo;Landroid/widget/Switch;)V

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->sharedPrefs:Landroid/content/SharedPreferences;

    iget-object v5, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 108
    .local v3, "isChecked":Z
    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 110
    invoke-virtual {p0, v0, v3}, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->setSwitchColor(Landroid/widget/Switch;Z)V

    .line 113
    .end local v3    # "isChecked":Z
    :cond_2
    const v4, 0x7f1000df

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 114
    .local v2, "iconView":Landroid/widget/ImageView;
    if-eqz v2, :cond_3

    .line 115
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 116
    new-instance v4, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$2;

    invoke-direct {v4, p0, v0}, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$2;-><init>(Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;Landroid/widget/Switch;)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    .end local v0    # "appName":Landroid/widget/Switch;
    .end local v1    # "data":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "iconView":Landroid/widget/ImageView;
    :cond_3
    return-object p2
.end method

.method public setSwitchColor(Landroid/widget/Switch;Z)V
    .locals 7
    .param p1, "appSwitch"    # Landroid/widget/Switch;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "isChecked"    # Z

    .prologue
    .line 132
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 133
    .local v0, "context":Landroid/content/Context;
    if-eqz p2, :cond_2

    const v5, 0x7f0f0015

    :goto_0
    invoke-static {v0, v5}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    .line 134
    .local v3, "trackColor":I
    if-eqz p2, :cond_3

    const v5, 0x7f0f0012

    :goto_1
    invoke-static {v0, v5}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 136
    .local v1, "thumbColor":I
    invoke-virtual {p1}, Landroid/widget/Switch;->getTrackDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 137
    .local v4, "trackDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/widget/Switch;->getThumbDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 138
    .local v2, "thumbDrawable":Landroid/graphics/drawable/Drawable;
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x17

    if-ge v5, v6, :cond_4

    .line 139
    if-eqz v4, :cond_0

    .line 140
    sget-object v5, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v3, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 142
    :cond_0
    if-eqz v2, :cond_1

    .line 143
    sget-object v5, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 153
    :cond_1
    :goto_2
    return-void

    .line 133
    .end local v1    # "thumbColor":I
    .end local v2    # "thumbDrawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "trackColor":I
    .end local v4    # "trackDrawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    const v5, 0x7f0f0059

    goto :goto_0

    .line 134
    .restart local v3    # "trackColor":I
    :cond_3
    const v5, 0x7f0f005c

    goto :goto_1

    .line 146
    .restart local v1    # "thumbColor":I
    .restart local v2    # "thumbDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v4    # "trackDrawable":Landroid/graphics/drawable/Drawable;
    :cond_4
    if-eqz v4, :cond_5

    .line 147
    invoke-virtual {v4, v3}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 149
    :cond_5
    if-eqz v2, :cond_1

    .line 150
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    goto :goto_2
.end method
