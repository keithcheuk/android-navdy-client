.class Lcom/navdy/client/app/ui/search/SearchActivity$17;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;->updateRecyclerViewForServiceSearch(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

.field final synthetic val$destinations:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 947
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->val$destinations:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 950
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1400(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 951
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->val$destinations:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithTextSearchResults(Ljava/util/List;)V

    .line 953
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->val$destinations:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->val$destinations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 954
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addNoResultsItem()V

    .line 955
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$3600(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 958
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$17;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2200(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 959
    return-void
.end method
