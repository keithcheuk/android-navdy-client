.class Lcom/navdy/client/app/ui/search/SearchActivity$22$1;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity$22;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/search/SearchActivity$22;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity$22;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/search/SearchActivity$22;

    .prologue
    .line 1268
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$22$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$22;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 1276
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$22$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$22;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/SearchActivity$22;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$4000(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "navigationHelper failed to get latLng for ContactModel Destination"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1277
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$22$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$22;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/SearchActivity$22;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$3900(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 1278
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$22$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$22;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/SearchActivity$22;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$3900(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 1272
    return-void
.end method
