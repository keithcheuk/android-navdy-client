.class public Lcom/navdy/client/app/ui/WebViewActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "WebViewActivity.java"


# static fields
.field public static final ATTRIBUTION:Ljava/lang/String; = "Attributions"

.field public static final EXTRA_HTML:Ljava/lang/String; = "extra_html"

.field public static final EXTRA_PREVENT_LINKS:Ljava/lang/String; = "prevent_links"

.field public static final EXTRA_TYPE:Ljava/lang/String; = "type"

.field public static final PRIVACY:Ljava/lang/String; = "Privacy"

.field public static final TERMS:Ljava/lang/String; = "Terms"

.field public static final ZENDESK:Ljava/lang/String; = "Zendesk"


# instance fields
.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/WebViewActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/WebViewActivity;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/navdy/client/app/ui/WebViewActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public injectMobileStyleForZendeskPage(Landroid/webkit/WebView;)V
    .locals 3
    .param p1, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 141
    :try_start_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 142
    const-string v1, "javascript:(function() {var parent = document.getElementsByTagName(\'head\').item(0);var style = document.createElement(\'style\');style.type = \'text/css\';style.appendChild(document.createTextNode(\".request-nav, .footer, .comments-title, .comment-list, .comment-load-more-comments, .comment-closed-notification, .comment-submit-request { display: none !important }\"));parent.appendChild(style);})()"

    invoke-virtual {p1, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 36
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v2, 0x7f03010c

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/WebViewActivity;->setContentView(I)V

    .line 40
    const v2, 0x7f1003fc

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/WebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 41
    .local v0, "webView":Landroid/webkit/WebView;
    if-nez v0, :cond_0

    .line 42
    const v2, 0x7f0804d7

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/navdy/client/app/ui/WebViewActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 43
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/WebViewActivity;->finish()V

    .line 127
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/WebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    .line 49
    .local v9, "intent":Landroid/content/Intent;
    const-string v2, "type"

    invoke-virtual {v9, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    .line 53
    const-string v2, "prevent_links"

    invoke-virtual {v9, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 56
    .local v10, "preventLinks":Z
    iget-object v4, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_1
    move v1, v2

    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 70
    :goto_2
    if-nez v10, :cond_2

    const-string v1, "Privacy"

    iget-object v2, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "Terms"

    iget-object v2, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    .line 72
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 73
    :cond_2
    new-instance v1, Lcom/navdy/client/app/ui/WebViewActivity$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/WebViewActivity$1;-><init>(Lcom/navdy/client/app/ui/WebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 103
    :cond_3
    :goto_3
    :try_start_0
    const-string v1, "Attributions"

    iget-object v2, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 106
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/WebViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 108
    .local v6, "attribution":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 110
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/google/android/gms/common/GoogleApiAvailability;->getOpenSourceSoftwareLicenseInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/plain"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 124
    .end local v6    # "attribution":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 125
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 56
    .end local v7    # "e":Ljava/lang/Exception;
    :sswitch_0
    const-string v3, "Privacy"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :sswitch_1
    const-string v1, "Terms"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v3

    goto :goto_1

    :sswitch_2
    const-string v1, "Attributions"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto/16 :goto_1

    .line 58
    :pswitch_0
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v2, 0x7f0803b0

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    goto/16 :goto_2

    .line 61
    :pswitch_1
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v2, 0x7f0804a0

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    goto/16 :goto_2

    .line 64
    :pswitch_2
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v2, 0x7f0800a7

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    goto/16 :goto_2

    .line 84
    :cond_4
    const-string v1, "Zendesk"

    iget-object v2, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 85
    new-instance v1, Lcom/navdy/client/app/ui/WebViewActivity$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/WebViewActivity$2;-><init>(Lcom/navdy/client/app/ui/WebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto/16 :goto_3

    .line 112
    :cond_5
    const/4 v8, 0x0

    .line 113
    .local v8, "fileName":Ljava/lang/String;
    :try_start_1
    const-string v1, "Privacy"

    iget-object v2, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 114
    const-string v8, "file:///android_res/raw/privacypolicy.html"

    .line 121
    :cond_6
    :goto_4
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    sget-object v2, Landroid/webkit/WebSettings$LayoutAlgorithm;->SINGLE_COLUMN:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    .line 122
    invoke-virtual {v0, v8}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 115
    :cond_7
    const-string v1, "Terms"

    iget-object v2, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 116
    const-string v8, "file:///android_res/raw/terms.html"

    goto :goto_4

    .line 117
    :cond_8
    const-string v1, "Zendesk"

    iget-object v2, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 118
    const-string v1, "extra_html"

    invoke-virtual {v9, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    goto :goto_4

    .line 56
    :sswitch_data_0
    .sparse-switch
        -0x32ca2c0c -> :sswitch_2
        0x4cf5967 -> :sswitch_1
        0x5079bb68 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 133
    iget-object v0, p0, Lcom/navdy/client/app/ui/WebViewActivity;->type:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/tracking/TrackerConstants$Screen$Web;->tag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 134
    return-void
.end method
