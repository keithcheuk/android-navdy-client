.class final Lcom/navdy/client/app/ui/PhotoUtils$3;
.super Ljava/lang/Object;
.source "PhotoUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/PhotoUtils;->getPhotoFromGallery(Lcom/navdy/client/app/ui/base/BaseActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Lcom/navdy/client/app/ui/base/BaseActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseActivity;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/navdy/client/app/ui/PhotoUtils$3;->val$activity:Lcom/navdy/client/app/ui/base/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 112
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 113
    .local v1, "getIntent":Landroid/content/Intent;
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    const-string v2, "Attach an image"

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 117
    .local v0, "chooserIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/navdy/client/app/ui/PhotoUtils$3;->val$activity:Lcom/navdy/client/app/ui/base/BaseActivity;

    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, Lcom/navdy/client/app/ui/base/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 118
    return-void
.end method
