.class final Lcom/navdy/client/app/ui/base/BaseHereMapFragment$1;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->initMarkerImage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 3
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 76
    new-instance v1, Lcom/here/android/mpa/common/Image;

    invoke-direct {v1}, Lcom/here/android/mpa/common/Image;-><init>()V

    invoke-static {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$002(Lcom/here/android/mpa/common/Image;)Lcom/here/android/mpa/common/Image;

    .line 78
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$000()Lcom/here/android/mpa/common/Image;

    move-result-object v1

    const v2, 0x7f0201d3

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "could not initialize map marker img"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
