.class public final enum Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
.super Ljava/lang/Enum;
.source "BaseHereMapFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

.field public static final enum MAP_ENGINE_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

.field public static final enum NO_MAP_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 682
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    const-string v1, "MAP_ENGINE_ERROR"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->MAP_ENGINE_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    .line 683
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    const-string v1, "NO_MAP_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->NO_MAP_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    .line 681
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->MAP_ENGINE_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->NO_MAP_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->$VALUES:[Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 681
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 681
    const-class v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
    .locals 1

    .prologue
    .line 681
    sget-object v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->$VALUES:[Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    invoke-virtual {v0}, [Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    return-object v0
.end method
