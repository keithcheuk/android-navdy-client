.class public interface abstract Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;
.super Ljava/lang/Object;
.source "GooglePlacesSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GoogleSearchListener"
.end annotation


# virtual methods
.method public abstract onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation
.end method
