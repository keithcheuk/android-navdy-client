.class Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/navigation/HereRouteManager;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/navigation/HereRouteManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 4
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 133
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$400()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 134
    :try_start_0
    sget-object v0, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-eq p1, v0, :cond_0

    .line 135
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HereRouteManager failed to initialize with error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    sget-object v2, Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;->FAILED:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$502(Lcom/navdy/client/app/framework/navigation/HereRouteManager;Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;)Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    .line 138
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$600(Lcom/navdy/client/app/framework/navigation/HereRouteManager;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$600(Lcom/navdy/client/app/framework/navigation/HereRouteManager;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;

    invoke-interface {v0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;->onFailed()V

    goto :goto_0

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 142
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v2, "HereRouteManager successfully initialized, running initListeners"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    sget-object v2, Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;->INITIALIZED:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$502(Lcom/navdy/client/app/framework/navigation/HereRouteManager;Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;)Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    .line 145
    :goto_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$600(Lcom/navdy/client/app/framework/navigation/HereRouteManager;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$600(Lcom/navdy/client/app/framework/navigation/HereRouteManager;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;

    invoke-interface {v0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;->onInit()V

    goto :goto_1

    .line 149
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    return-void
.end method
