.class public Lcom/navdy/client/app/framework/map/HereMapsManager;
.super Ljava/lang/Object;
.source "HereMapsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/map/HereMapsManager$State;
    }
.end annotation


# static fields
.field private static final HERE_MAPS_INTERNAL_FOLDER:Ljava/lang/String; = "/.here-maps"

.field private static final MAP_ENGINE_PROCESS_NAME:Ljava/lang/String; = "Navdy.Mapservice"

.field private static final MAP_SERVICE_INTENT:Ljava/lang/String; = "com.navdy.mapservice.MapService"

.field private static final initListenersLock:Ljava/lang/Object;

.field private static final instance:Lcom/navdy/client/app/framework/map/HereMapsManager;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private initError:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

.field private final initListeners:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/here/android/mpa/common/OnEngineInitListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Landroid/os/Handler;

.field private final mapEngine:Lcom/here/android/mpa/common/MapEngine;

.field private final onEngineInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;

.field private volatile state:Lcom/navdy/client/app/framework/map/HereMapsManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/map/HereMapsManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager;->initListenersLock:Ljava/lang/Object;

    .line 40
    new-instance v0, Lcom/navdy/client/app/framework/map/HereMapsManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/map/HereMapsManager;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager;->instance:Lcom/navdy/client/app/framework/map/HereMapsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/navdy/client/app/framework/map/HereMapsManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/map/HereMapsManager$1;-><init>(Lcom/navdy/client/app/framework/map/HereMapsManager;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->onEngineInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;

    .line 92
    invoke-static {}, Lcom/here/android/mpa/common/MapEngine;->getInstance()Lcom/here/android/mpa/common/MapEngine;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->mapEngine:Lcom/here/android/mpa/common/MapEngine;

    .line 93
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->initListeners:Ljava/util/Queue;

    .line 94
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->mainThread:Landroid/os/Handler;

    .line 96
    sget-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->IDLE:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    iput-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->state:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/map/HereMapsManager;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereMapsManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->initError:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/framework/map/HereMapsManager;Lcom/here/android/mpa/common/OnEngineInitListener$Error;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereMapsManager;
    .param p1, "x1"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->initError:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    return-object p1
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/map/HereMapsManager;)Lcom/navdy/client/app/framework/map/HereMapsManager$State;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereMapsManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->state:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/client/app/framework/map/HereMapsManager;Lcom/navdy/client/app/framework/map/HereMapsManager$State;)Lcom/navdy/client/app/framework/map/HereMapsManager$State;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereMapsManager;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->state:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    return-object p1
.end method

.method static synthetic access$300()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager;->initListenersLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/map/HereMapsManager;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereMapsManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->initListeners:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/map/HereMapsManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereMapsManager;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->mainThread:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/map/HereMapsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereMapsManager;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/HereMapsManager;->initializeIfPermitted()V

    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/map/HereMapsManager;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager;->instance:Lcom/navdy/client/app/framework/map/HereMapsManager;

    return-object v0
.end method

.method private initialize()V
    .locals 3

    .prologue
    .line 168
    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "initializing here map engine"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->INITIALIZING:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    iput-object v1, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->state:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    .line 171
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 173
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/.here-maps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.navdy.mapservice.MapService"

    invoke-static {v1, v2}, Lcom/here/android/mpa/common/MapSettings;->setIsolatedDiskCacheRootPath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 175
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->mapEngine:Lcom/here/android/mpa/common/MapEngine;

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->onEngineInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;

    invoke-virtual {v1, v0, v2}, Lcom/here/android/mpa/common/MapEngine;->init(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 176
    return-void
.end method

.method private initializeIfPermitted()V
    .locals 7

    .prologue
    .line 148
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 150
    .local v1, "context":Landroid/content/Context;
    const-string v4, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {v1, v4}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 151
    .local v2, "readExternalStoragePermission":Z
    const-string v4, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v1, v4}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 152
    .local v3, "writeExternalStoragePermission":Z
    const-string v4, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v1, v4}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveThisPermission(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 154
    .local v0, "accessFineLocationPermission":Z
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 157
    sget-object v4, Lcom/navdy/client/app/framework/map/HereMapsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "initialization is permitted, initializing..."

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 158
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/HereMapsManager;->initialize()V

    .line 165
    :goto_0
    return-void

    .line 160
    :cond_0
    sget-object v4, Lcom/navdy/client/app/framework/map/HereMapsManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initialization is not permitted yet, permission granted for READ_EXTERNAL_STORAGE="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; writeExternalStoragePermission="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; accessFineLocationPermission="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addOnInitializedListener(Lcom/here/android/mpa/common/OnEngineInitListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/here/android/mpa/common/OnEngineInitListener;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->state:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->FAILED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    if-ne v0, v1, :cond_0

    .line 126
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->state:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->INITIALIZED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    if-ne v0, v1, :cond_1

    .line 105
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->mainThread:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/map/HereMapsManager$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/map/HereMapsManager$2;-><init>(Lcom/navdy/client/app/framework/map/HereMapsManager;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 114
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/map/HereMapsManager$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/map/HereMapsManager$3;-><init>(Lcom/navdy/client/app/framework/map/HereMapsManager;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager;->state:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->INITIALIZED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public killSelfAndMapEngine()V
    .locals 5

    .prologue
    .line 133
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 135
    .local v0, "manager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 136
    .local v1, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v3, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    const-string v4, "Navdy.Mapservice"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 137
    iget v2, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 141
    .end local v1    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->kill()V

    .line 142
    return-void
.end method
