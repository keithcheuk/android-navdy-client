.class final Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->doCoordAndAddressLookup()V

    return-void
.end method

.method private doCoordAndAddressLookup()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasAlreadyBeenProcessed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 127
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 128
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasDetailedAddress()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v2, "destination has good data but it\'s not stored in cache or DB, saving and calling back with success "

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v4, v2, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    iget-object v5, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    move-object v2, v1

    move-object v3, v1

    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$300(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    .line 179
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 139
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 140
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "processDestination, destination does not have address, assigning display coordinates as address"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v2, v2, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v2, v2, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->setRawAddressNotForDisplay(Ljava/lang/String;)V

    .line 142
    new-instance v6, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v0, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v2, v2, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 154
    .local v6, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :goto_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1$2;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;)V

    invoke-static {v6, v0, v1}, Lcom/navdy/client/app/framework/map/MapUtils;->doReverseGeocodingFor(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 143
    .end local v6    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 144
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "processDestination, destination does not have address, assigning nav coordinates as address"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v2, v2, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v2, v2, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->setRawAddressNotForDisplay(Ljava/lang/String;)V

    .line 146
    new-instance v6, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v0, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v2, v2, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .restart local v6    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    goto :goto_1

    .line 148
    .end local v6    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_3
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "processDestination, destination does not have address or coords, calling back with error"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    sget-object v2, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->INVALID_REQUEST:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    invoke-static {v0, v1, v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$400(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V

    goto/16 :goto_0

    .line 166
    :cond_4
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 167
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 168
    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->isTitleIsInTheAddress(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 169
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 170
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->isContact()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v7, 0x1

    .line 172
    .local v7, "shouldAddNameToAddress":Z
    :goto_2
    if-eqz v7, :cond_6

    .line 173
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "destination is not favorite, trying adding name with address"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$500(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto/16 :goto_0

    .line 170
    .end local v7    # "shouldAddNameToAddress":Z
    :cond_5
    const/4 v7, 0x0

    goto :goto_2

    .line 176
    .restart local v7    # "shouldAddNameToAddress":Z
    :cond_6
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "destination is favorite/contact, trying just with address"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$600(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 99
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->reloadSelfFromDatabase()V

    .line 101
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->isPlaceDetailsInfoStale()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 103
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1$1;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;)V

    .line 116
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$100()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;Landroid/os/Handler;)V

    .line 117
    .local v0, "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runDetailsSearchWebApi(Ljava/lang/String;)V

    .line 122
    .end local v0    # "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;->doCoordAndAddressLookup()V

    goto :goto_0
.end method
