.class final Lcom/navdy/client/app/framework/models/Destination$1;
.super Ljava/lang/Object;
.source "Destination.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/models/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/navdy/client/app/framework/models/Destination;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 564
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0, p1}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 561
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/models/Destination$1;->createFromParcel(Landroid/os/Parcel;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 569
    new-array v0, p1, [Lcom/navdy/client/app/framework/models/Destination;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 561
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/models/Destination$1;->newArray(I)[Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method
