.class Lcom/navdy/client/app/framework/AppInstance$2;
.super Ljava/lang/Object;
.source "AppInstance.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/AppInstance;

.field final synthetic val$big:Z

.field final synthetic val$toastDuration:I

.field final synthetic val$toastText:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/AppInstance;Ljava/lang/String;IZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/AppInstance;

    .prologue
    .line 248
    iput-object p1, p0, Lcom/navdy/client/app/framework/AppInstance$2;->this$0:Lcom/navdy/client/app/framework/AppInstance;

    iput-object p2, p0, Lcom/navdy/client/app/framework/AppInstance$2;->val$toastText:Ljava/lang/String;

    iput p3, p0, Lcom/navdy/client/app/framework/AppInstance$2;->val$toastDuration:I

    iput-boolean p4, p0, Lcom/navdy/client/app/framework/AppInstance$2;->val$big:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 251
    iget-object v4, p0, Lcom/navdy/client/app/framework/AppInstance$2;->this$0:Lcom/navdy/client/app/framework/AppInstance;

    invoke-static {v4}, Lcom/navdy/client/app/framework/AppInstance;->access$100(Lcom/navdy/client/app/framework/AppInstance;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/framework/AppInstance$2;->val$toastText:Ljava/lang/String;

    iget v6, p0, Lcom/navdy/client/app/framework/AppInstance$2;->val$toastDuration:I

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 252
    .local v2, "toast":Landroid/widget/Toast;
    invoke-virtual {v2}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v3

    .line 253
    .local v3, "view":Landroid/view/View;
    iget-boolean v4, p0, Lcom/navdy/client/app/framework/AppInstance$2;->val$big:Z

    if-eqz v4, :cond_1

    .line 254
    instance-of v4, v3, Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    move-object v0, v3

    .line 255
    check-cast v0, Landroid/widget/LinearLayout;

    .line 256
    .local v0, "linearLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 257
    .local v1, "messageTextView":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 258
    const/high16 v4, 0x42400000    # 48.0f

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 261
    .end local v0    # "linearLayout":Landroid/widget/LinearLayout;
    .end local v1    # "messageTextView":Landroid/widget/TextView;
    :cond_0
    const/16 v4, 0x11

    invoke-virtual {v2, v4, v7, v7}, Landroid/widget/Toast;->setGravity(III)V

    .line 263
    :cond_1
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 264
    return-void
.end method
