.class public Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;
.super Ljava/lang/Object;
.source "DisplayNavigationServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RouteCalculationProgress"
.end annotation


# instance fields
.field public final handle:Ljava/lang/String;

.field public final progress:I

.field public final requestId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;
    .param p2, "handle"    # Ljava/lang/String;
    .param p3, "progress"    # I

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;->requestId:Ljava/lang/String;

    .line 129
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;->handle:Ljava/lang/String;

    .line 130
    iput p3, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;->progress:I

    .line 131
    return-void
.end method
