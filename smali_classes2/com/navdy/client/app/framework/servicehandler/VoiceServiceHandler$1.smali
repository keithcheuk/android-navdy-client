.class Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;
.super Ljava/lang/Object;
.source "VoiceServiceHandler.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/TTSAudioRouter$VoiceSearchSetupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleVoiceSearchRequest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioInputReady(ZZ)V
    .locals 7
    .param p1, "usingBluetooth"    # Z
    .param p2, "hfpConnectionFailed"    # Z

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 298
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$002(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Z)Z

    .line 299
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 300
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$200(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/media/SoundPool;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v1}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)I

    move-result v1

    const/4 v4, 0x1

    const/4 v5, 0x0

    move v3, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$600(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 317
    return-void
.end method
