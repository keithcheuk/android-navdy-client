.class Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;
.super Ljava/lang/Object;
.source "MusicServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->onMusicArtworkRequest(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

.field final synthetic val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 543
    iget-object v8, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v8, v8, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 544
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "This artwork request is only for collections"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 599
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    :try_start_0
    iget-object v8, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v8, v8, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    int-to-long v2, v8

    .line 556
    .local v2, "collectionId":J
    const/4 v1, 0x0

    .line 557
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 558
    .local v4, "contentResolver":Landroid/content/ContentResolver;
    sget-object v8, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 559
    invoke-static {v4, v2, v3}, Lcom/navdy/client/app/framework/util/MusicUtils;->getArtworkForAlbum(Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 566
    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v8

    if-nez v8, :cond_4

    .line 567
    :cond_3
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "Received photo has null or empty byte array"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 570
    :cond_4
    const/4 v0, 0x0

    .line 573
    .local v0, "artwork":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 574
    .local v7, "photo":Lokio/ByteString;
    if-eqz v1, :cond_8

    .line 575
    :try_start_1
    iget-object v8, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v8, v8, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->size:Ljava/lang/Integer;

    .line 576
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->size:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    sget-object v10, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    .line 575
    invoke-static {v1, v8, v9, v10}, Lcom/navdy/service/library/util/ScalingUtilities;->createScaledBitmapAndRecycleOriginalIfScaled(Landroid/graphics/Bitmap;IILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 577
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->bitmap2ByteBuffer(Landroid/graphics/Bitmap;)[B

    move-result-object v5

    .line 578
    .local v5, "data":[B
    if-nez v5, :cond_7

    .line 579
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "Error artworking"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595
    if-eqz v0, :cond_0

    .line 596
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 551
    .end local v0    # "artwork":Landroid/graphics/Bitmap;
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "collectionId":J
    .end local v4    # "contentResolver":Landroid/content/ContentResolver;
    .end local v5    # "data":[B
    .end local v7    # "photo":Lokio/ByteString;
    :catch_0
    move-exception v6

    .line 552
    .local v6, "e":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Couldn\'t parse "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v10, v10, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 560
    .end local v6    # "e":Ljava/lang/Throwable;
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "collectionId":J
    .restart local v4    # "contentResolver":Landroid/content/ContentResolver;
    :cond_5
    sget-object v8, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 561
    invoke-static {v4, v2, v3}, Lcom/navdy/client/app/framework/util/MusicUtils;->getArtworkForArtist(Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 562
    :cond_6
    sget-object v8, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 563
    invoke-static {v4, v2, v3}, Lcom/navdy/client/app/framework/util/MusicUtils;->getArtworkForPlaylist(Landroid/content/ContentResolver;J)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_1

    .line 583
    .restart local v0    # "artwork":Landroid/graphics/Bitmap;
    .restart local v5    # "data":[B
    .restart local v7    # "photo":Lokio/ByteString;
    :cond_7
    :try_start_2
    invoke-static {v5}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v7

    .line 586
    .end local v5    # "data":[B
    :cond_8
    new-instance v8, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    invoke-direct {v8}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;-><init>()V

    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 587
    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 588
    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;->val$request:Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    .line 589
    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    move-result-object v8

    .line 590
    invoke-virtual {v8, v7}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->photo(Lokio/ByteString;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    move-result-object v8

    .line 591
    invoke-virtual {v8}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->build()Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    move-result-object v8

    .line 586
    invoke-static {v8}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 595
    if-eqz v0, :cond_0

    .line 596
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 592
    :catch_1
    move-exception v6

    .line 593
    .local v6, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "Error updating the artwork"

    invoke-virtual {v8, v9, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 595
    if-eqz v0, :cond_0

    .line 596
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 595
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    if-eqz v0, :cond_9

    .line 596
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_9
    throw v8
.end method
