.class final enum Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;
.super Ljava/lang/Enum;
.source "DestinationSuggestionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "UploadResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

.field public static final enum FAILURE:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

.field public static final enum SUCCESS:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 383
    new-instance v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->SUCCESS:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    .line 384
    new-instance v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->FAILURE:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    .line 382
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    sget-object v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->SUCCESS:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->FAILURE:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->$VALUES:[Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 382
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 382
    const-class v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;
    .locals 1

    .prologue
    .line 382
    sget-object v0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->$VALUES:[Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    return-object v0
.end method
