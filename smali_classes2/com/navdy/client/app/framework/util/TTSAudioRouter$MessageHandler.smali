.class Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;
.super Landroid/os/Handler;
.source "TTSAudioRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/TTSAudioRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageHandler"
.end annotation


# instance fields
.field mAudioRouterReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/util/TTSAudioRouter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/TTSAudioRouter;Landroid/os/Looper;)V
    .locals 1
    .param p1, "router"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 499
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 500
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->mAudioRouterReference:Ljava/lang/ref/WeakReference;

    .line 501
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 505
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 506
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->mAudioRouterReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    .line 507
    .local v0, "router":Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    if-eqz v0, :cond_0

    .line 508
    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->handleMessage(I)V

    .line 512
    :goto_0
    return-void

    .line 510
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "TTS router messages received after router is GCed "

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
