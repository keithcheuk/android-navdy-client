.class public final enum Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;
.super Ljava/lang/Enum;
.source "ContactsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/ContactsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Attributes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

.field public static final enum ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

.field public static final enum BOTH:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

.field public static final enum PHONE:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 124
    new-instance v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    const-string v1, "ADDRESS"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 125
    new-instance v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->PHONE:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 126
    new-instance v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->BOTH:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 123
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->PHONE:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->BOTH:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->$VALUES:[Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 123
    const-class v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->$VALUES:[Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    return-object v0
.end method
