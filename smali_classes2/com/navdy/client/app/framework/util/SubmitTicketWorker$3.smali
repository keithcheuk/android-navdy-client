.class Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;
.super Lcom/zendesk/service/ZendeskCallback;
.source "SubmitTicketWorker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->sendToZenddesk(Ljava/io/File;Lorg/json/JSONObject;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/CreateRequest;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$folder:Ljava/io/File;

.field final synthetic val$ticketContents:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Lorg/json/JSONObject;Ljava/io/File;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    .prologue
    .line 324
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$ticketContents:Lorg/json/JSONObject;

    iput-object p3, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$folder:Ljava/io/File;

    iput-object p4, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 3
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 354
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ticket request failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 356
    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getStatus()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 364
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ticket request failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 366
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$000(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V

    .line 367
    return-void

    .line 358
    :sswitch_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Internet error"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 361
    :sswitch_1
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Ticket is malformed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 356
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x1a6 -> :sswitch_1
    .end sparse-switch
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/request/CreateRequest;)V
    .locals 4
    .param p1, "createRequest"    # Lcom/zendesk/sdk/model/request/CreateRequest;

    .prologue
    const/4 v3, 0x1

    .line 327
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "ticket request successful"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 329
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v1, v3}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$802(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Z)Z

    .line 331
    const v1, 0x7f08050f

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/navdy/client/app/ui/base/BaseActivity;->showShortToast(I[Ljava/lang/Object;)V

    .line 333
    const-string v1, "Support_Ticket_Created"

    invoke-static {v1}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 336
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$ticketContents:Lorg/json/JSONObject;

    const-string v2, "ticket_action"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "upload_await_hud_log"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$ticketContents:Lorg/json/JSONObject;

    const-string v2, "ticket_action"

    const-string v3, "fetch_hud_log_upload_delete"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 338
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$ticketContents:Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$folder:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/navdy/client/app/framework/util/SupportTicketService;->writeJsonObjectToFile(Lorg/json/JSONObject;Ljava/lang/String;Lcom/navdy/service/library/log/Logger;)V

    .line 339
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "tickets_awaiting_hud_log_exist"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 348
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$300(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V

    .line 349
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->resetRetryCount()V

    .line 350
    return-void

    .line 341
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleting submitted ticket folder: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$folder:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 342
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$appContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->val$folder:Ljava/io/File;

    invoke-static {v1, v2}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception found trying to read JSON object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 324
    check-cast p1, Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$3;->onSuccess(Lcom/zendesk/sdk/model/request/CreateRequest;)V

    return-void
.end method
