.class Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;
.super Lcom/zendesk/service/ZendeskCallback;
.source "SubmitTicketWorker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->submitTicket(Lorg/json/JSONObject;Ljava/io/File;Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/UploadResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

.field final synthetic val$folder:Ljava/io/File;

.field final synthetic val$ticketContents:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Ljava/io/File;Lorg/json/JSONObject;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;->val$folder:Ljava/io/File;

    iput-object p3, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;->val$ticketContents:Lorg/json/JSONObject;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 3
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 235
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file failed to upload: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$000(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V

    .line 237
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/request/UploadResponse;)V
    .locals 4
    .param p1, "uploadResponse"    # Lcom/zendesk/sdk/model/request/UploadResponse;

    .prologue
    .line 229
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "file successfully uploaded"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;->val$folder:Ljava/io/File;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;->val$ticketContents:Lorg/json/JSONObject;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UploadResponse;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$700(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Ljava/io/File;Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 226
    check-cast p1, Lcom/zendesk/sdk/model/request/UploadResponse;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$2;->onSuccess(Lcom/zendesk/sdk/model/request/UploadResponse;)V

    return-void
.end method
