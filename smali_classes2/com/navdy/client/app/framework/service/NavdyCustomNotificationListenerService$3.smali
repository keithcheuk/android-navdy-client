.class Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;
.super Ljava/lang/Object;
.source "NavdyCustomNotificationListenerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->onNotificationRemoved(Landroid/service/notification/StatusBarNotification;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

.field final synthetic val$sbn:Landroid/service/notification/StatusBarNotification;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    .prologue
    .line 516
    iput-object p1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    iput-object p2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 519
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    if-nez v2, :cond_0

    .line 546
    :goto_0
    return-void

    .line 523
    :cond_0
    sget-object v2, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNotificationRemoved: pkg ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    .line 524
    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " tag ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    .line 525
    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " id ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    .line 526
    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " time ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    .line 527
    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getPostTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 523
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 529
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    iget-object v3, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->access$400(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 530
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    iget-object v3, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    invoke-virtual {v3}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->removeActiveMusicApp(Ljava/lang/String;)V

    .line 533
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    iget-object v3, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->cancelPendingHandlerIfAnyFor(Landroid/service/notification/StatusBarNotification;)V

    .line 536
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    invoke-virtual {v2}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/app/NotificationCompat;->isGroupSummary(Landroid/app/Notification;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 537
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    invoke-virtual {v2}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/app/NotificationCompat;->getGroup(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    .line 538
    .local v0, "group":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    invoke-static {v2}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->access$100(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->removePendingHandlingForGroup(Ljava/lang/String;)V

    .line 543
    .end local v0    # "group":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    iget-object v3, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->val$sbn:Landroid/service/notification/StatusBarNotification;

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->access$500(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v1

    .line 544
    .local v1, "notificationHashKey":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;->this$0:Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    invoke-static {v2}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->access$600(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method
