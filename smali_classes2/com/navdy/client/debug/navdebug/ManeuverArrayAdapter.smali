.class public Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ManeuverArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/here/android/mpa/routing/Maneuver;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mManeuvers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p2, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    const v0, 0x7f0300b8

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 32
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;->mActivity:Landroid/app/Activity;

    .line 33
    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;->mManeuvers:Ljava/util/List;

    .line 34
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 39
    if-eqz p2, :cond_0

    .line 40
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter$ViewHolder;

    .line 48
    .local v0, "holder":Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter$ViewHolder;
    :goto_0
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;->mManeuvers:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/routing/Maneuver;

    .line 49
    .local v2, "m":Lcom/here/android/mpa/routing/Maneuver;
    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter$ViewHolder;->mManeuverDetailPanel:Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;

    invoke-virtual {v3, v2}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->setManeuver(Lcom/here/android/mpa/routing/Maneuver;)V

    .line 50
    return-object p2

    .line 42
    .end local v0    # "holder":Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter$ViewHolder;
    .end local v2    # "m":Lcom/here/android/mpa/routing/Maneuver;
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 43
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-static {p3}, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->inflate(Landroid/view/ViewGroup;)Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;

    move-result-object p2

    .line 44
    new-instance v0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter$ViewHolder;

    move-object v3, p2

    check-cast v3, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;

    invoke-direct {v0, v3}, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter$ViewHolder;-><init>(Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;)V

    .line 45
    .restart local v0    # "holder":Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter$ViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method
