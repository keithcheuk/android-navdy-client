.class public Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel$$ViewInjector;
.super Ljava/lang/Object;
.source "ManeuverDetailPanel$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f1002ff

    const-string v2, "field \'mActionView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mActionView:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f100300

    const-string v2, "field \'mIconView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mIconView:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f100301

    const-string v2, "field \'mTurnView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mTurnView:Landroid/widget/TextView;

    .line 16
    const v1, 0x7f100302

    const-string v2, "field \'mAngleView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mAngleView:Landroid/widget/TextView;

    .line 18
    const v1, 0x7f100303

    const-string v2, "field \'mTrafficDirectionView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mTrafficDirectionView:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f100304

    const-string v2, "field \'mTransportModeView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mTransportModeView:Landroid/widget/TextView;

    .line 22
    const v1, 0x7f100305

    const-string v2, "field \'mDistFromStartView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mDistFromStartView:Landroid/widget/TextView;

    .line 24
    const v1, 0x7f100306

    const-string v2, "field \'mDistFromPrevView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mDistFromPrevView:Landroid/widget/TextView;

    .line 26
    const v1, 0x7f100307

    const-string v2, "field \'mDistToNextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mDistToNextView:Landroid/widget/TextView;

    .line 28
    const v1, 0x7f100308

    const-string v2, "field \'mInstructionView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mInstructionView:Landroid/widget/TextView;

    .line 30
    const v1, 0x7f100309

    const-string v2, "field \'mCurStreetNumView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mCurStreetNumView:Landroid/widget/TextView;

    .line 32
    const v1, 0x7f10030a

    const-string v2, "field \'mCurStreetNameView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mCurStreetNameView:Landroid/widget/TextView;

    .line 34
    const v1, 0x7f10030b

    const-string v2, "field \'mNextStreetNumView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 35
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetNumView:Landroid/widget/TextView;

    .line 36
    const v1, 0x7f10030c

    const-string v2, "field \'mNextStreetNameView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 37
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetNameView:Landroid/widget/TextView;

    .line 38
    const v1, 0x7f10030d

    const-string v2, "field \'mNextStreetImageView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 39
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetImageView:Landroid/widget/ImageView;

    .line 40
    const v1, 0x7f10030e

    const-string v2, "field \'mSignpostNumberView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 41
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostNumberView:Landroid/widget/TextView;

    .line 42
    const v1, 0x7f10030f

    const-string v2, "field \'mSignpostTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 43
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostTextView:Landroid/widget/TextView;

    .line 44
    const v1, 0x7f100310

    const-string v2, "field \'mSignpostIconView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 45
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostIconView:Landroid/widget/ImageView;

    .line 46
    const v1, 0x7f100311

    const-string v2, "field \'mSignpostLabelDirectionView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 47
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostLabelDirectionView:Landroid/widget/TextView;

    .line 48
    const v1, 0x7f100312

    const-string v2, "field \'mRoadElementsView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 49
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mRoadElementsView:Landroid/widget/TextView;

    .line 50
    const v1, 0x7f100313

    const-string v2, "field \'mStartTimeView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 51
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mStartTimeView:Landroid/widget/TextView;

    .line 52
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;

    .prologue
    const/4 v0, 0x0

    .line 55
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mActionView:Landroid/widget/TextView;

    .line 56
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mIconView:Landroid/widget/TextView;

    .line 57
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mTurnView:Landroid/widget/TextView;

    .line 58
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mAngleView:Landroid/widget/TextView;

    .line 59
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mTrafficDirectionView:Landroid/widget/TextView;

    .line 60
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mTransportModeView:Landroid/widget/TextView;

    .line 61
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mDistFromStartView:Landroid/widget/TextView;

    .line 62
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mDistFromPrevView:Landroid/widget/TextView;

    .line 63
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mDistToNextView:Landroid/widget/TextView;

    .line 64
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mInstructionView:Landroid/widget/TextView;

    .line 65
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mCurStreetNumView:Landroid/widget/TextView;

    .line 66
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mCurStreetNameView:Landroid/widget/TextView;

    .line 67
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetNumView:Landroid/widget/TextView;

    .line 68
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetNameView:Landroid/widget/TextView;

    .line 69
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mNextStreetImageView:Landroid/widget/ImageView;

    .line 70
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostNumberView:Landroid/widget/TextView;

    .line 71
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostTextView:Landroid/widget/TextView;

    .line 72
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostIconView:Landroid/widget/ImageView;

    .line 73
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mSignpostLabelDirectionView:Landroid/widget/TextView;

    .line 74
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mRoadElementsView:Landroid/widget/TextView;

    .line 75
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/ManeuverDetailPanel;->mStartTimeView:Landroid/widget/TextView;

    .line 76
    return-void
.end method
