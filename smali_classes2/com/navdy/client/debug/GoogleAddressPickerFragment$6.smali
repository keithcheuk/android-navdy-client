.class Lcom/navdy/client/debug/GoogleAddressPickerFragment$6;
.super Ljava/lang/Object;
.source "GoogleAddressPickerFragment.java"

# interfaces
.implements Lcom/google/maps/PendingResult$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getGoogleNavigationCoordinate(Lcom/google/android/gms/location/places/Place;Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/maps/PendingResult$Callback",
        "<[",
        "Lcom/google/maps/model/DirectionsRoute;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

.field final synthetic val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 494
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$6;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iput-object p2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$6;->val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 521
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$6;->val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    invoke-interface {v0, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;->onFailure(Ljava/lang/Throwable;)V

    .line 522
    return-void
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 494
    check-cast p1, [Lcom/google/maps/model/DirectionsRoute;

    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$6;->onResult([Lcom/google/maps/model/DirectionsRoute;)V

    return-void
.end method

.method public onResult([Lcom/google/maps/model/DirectionsRoute;)V
    .locals 8
    .param p1, "result"    # [Lcom/google/maps/model/DirectionsRoute;

    .prologue
    .line 497
    const/4 v1, 0x0

    .line 498
    .local v1, "navLatLng":Lcom/google/android/gms/maps/model/LatLng;
    array-length v3, p1

    if-nez v3, :cond_0

    .line 499
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "no route found!"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 500
    iget-object v3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$6;->val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;->onSuccess(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 517
    :goto_0
    return-void

    .line 503
    :cond_0
    const/4 v3, 0x0

    aget-object v2, p1, v3

    .line 504
    .local v2, "route":Lcom/google/maps/model/DirectionsRoute;
    iget-object v3, v2, Lcom/google/maps/model/DirectionsRoute;->legs:[Lcom/google/maps/model/DirectionsLeg;

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/maps/model/DirectionsRoute;->legs:[Lcom/google/maps/model/DirectionsLeg;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 505
    iget-object v3, v2, Lcom/google/maps/model/DirectionsRoute;->legs:[Lcom/google/maps/model/DirectionsLeg;

    iget-object v4, v2, Lcom/google/maps/model/DirectionsRoute;->legs:[Lcom/google/maps/model/DirectionsLeg;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v0, v3, v4

    .line 506
    .local v0, "leg":Lcom/google/maps/model/DirectionsLeg;
    if-eqz v0, :cond_1

    .line 507
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "leg startAddress:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/maps/model/DirectionsLeg;->startAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " latlng:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/maps/model/DirectionsLeg;->startLocation:Lcom/google/maps/model/LatLng;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 509
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "leg startAddress:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/maps/model/DirectionsLeg;->endAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " latlng:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/maps/model/DirectionsLeg;->endLocation:Lcom/google/maps/model/LatLng;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 511
    iget-object v3, v0, Lcom/google/maps/model/DirectionsLeg;->endLocation:Lcom/google/maps/model/LatLng;

    if-eqz v3, :cond_1

    .line 512
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    .end local v1    # "navLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v3, v0, Lcom/google/maps/model/DirectionsLeg;->endLocation:Lcom/google/maps/model/LatLng;

    iget-wide v4, v3, Lcom/google/maps/model/LatLng;->lat:D

    iget-object v3, v0, Lcom/google/maps/model/DirectionsLeg;->endLocation:Lcom/google/maps/model/LatLng;

    iget-wide v6, v3, Lcom/google/maps/model/LatLng;->lng:D

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 516
    .end local v0    # "leg":Lcom/google/maps/model/DirectionsLeg;
    .restart local v1    # "navLatLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$6;->val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    invoke-interface {v3, v1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;->onSuccess(Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0
.end method
