.class public Lcom/navdy/client/debug/SubmitTicketFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "SubmitTicketFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/SubmitTicketFragment;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/SubmitTicketFragment;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f1003eb

    const-string v2, "field \'mTitle\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/EditText;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/SubmitTicketFragment;->mTitle:Landroid/widget/EditText;

    .line 12
    const v1, 0x7f1003ec

    const-string v2, "field \'mDescription\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/EditText;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/SubmitTicketFragment;->mDescription:Landroid/widget/EditText;

    .line 14
    const v1, 0x7f1003ea

    const-string v2, "field \'spinner\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/Spinner;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/SubmitTicketFragment;->spinner:Landroid/widget/Spinner;

    .line 16
    const v1, 0x7f1003ed

    const-string v2, "method \'onSubmit\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/debug/SubmitTicketFragment$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/SubmitTicketFragment$$ViewInjector$1;-><init>(Lcom/navdy/client/debug/SubmitTicketFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/SubmitTicketFragment;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/SubmitTicketFragment;

    .prologue
    const/4 v0, 0x0

    .line 28
    iput-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mTitle:Landroid/widget/EditText;

    .line 29
    iput-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->mDescription:Landroid/widget/EditText;

    .line 30
    iput-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment;->spinner:Landroid/widget/Spinner;

    .line 31
    return-void
.end method
