.class Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "DeviceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/devicepicker/DeviceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private clickListener:Landroid/view/View$OnClickListener;

.field private connectionTypes:[Landroid/widget/Button;

.field private device:Lcom/navdy/client/debug/devicepicker/Device;

.field private key:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v2, 0x3

    new-array v2, v2, [Landroid/widget/Button;

    iput-object v2, p0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->connectionTypes:[Landroid/widget/Button;

    .line 71
    new-instance v2, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder$1;-><init>(Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;)V

    iput-object v2, p0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->clickListener:Landroid/view/View$OnClickListener;

    .line 80
    const v2, 0x7f1002f9

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->key:Landroid/widget/TextView;

    .line 82
    sget-object v2, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->button_ids:[I

    if-nez v2, :cond_0

    .line 83
    invoke-static {}, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->lookupIds()V

    .line 85
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->button_ids:[I

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 86
    sget-object v2, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->button_ids:[I

    aget v2, v2, v1

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 87
    .local v0, "button":Landroid/widget/Button;
    iget-object v2, p0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    sget-object v2, Lcom/navdy/client/debug/devicepicker/DeviceAdapter;->types:[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 89
    iget-object v2, p0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->connectionTypes:[Landroid/widget/Button;

    aput-object v0, v2, v1

    .line 85
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    .end local v0    # "button":Landroid/widget/Button;
    :cond_1
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lcom/navdy/client/debug/devicepicker/DeviceAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Lcom/navdy/client/debug/devicepicker/DeviceAdapter$1;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;Lcom/navdy/client/debug/devicepicker/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/navdy/client/debug/devicepicker/Device;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->setDevice(Lcom/navdy/client/debug/devicepicker/Device;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;)Lcom/navdy/client/debug/devicepicker/Device;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->device:Lcom/navdy/client/debug/devicepicker/Device;

    return-object v0
.end method

.method private setDevice(Lcom/navdy/client/debug/devicepicker/Device;)V
    .locals 7
    .param p1, "device"    # Lcom/navdy/client/debug/devicepicker/Device;

    .prologue
    const/4 v5, 0x0

    .line 94
    iput-object p1, p0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->device:Lcom/navdy/client/debug/devicepicker/Device;

    .line 95
    iget-object v4, p0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->key:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/navdy/client/debug/devicepicker/Device;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    invoke-virtual {p1}, Lcom/navdy/client/debug/devicepicker/Device;->getConnectionTypes()[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v2

    .line 97
    .local v2, "connectionTypes":[Lcom/navdy/service/library/device/connection/ConnectionInfo;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_2

    .line 98
    iget-object v4, p0, Lcom/navdy/client/debug/devicepicker/DeviceAdapter$ViewHolder;->connectionTypes:[Landroid/widget/Button;

    aget-object v0, v4, v3

    .line 99
    .local v0, "button":Landroid/widget/Button;
    aget-object v4, v2, v3

    if-eqz v4, :cond_0

    move v4, v5

    :goto_1
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    move-object v1, v0

    .line 100
    check-cast v1, Landroid/widget/Checkable;

    .line 101
    .local v1, "checkable":Landroid/widget/Checkable;
    invoke-virtual {p1}, Lcom/navdy/client/debug/devicepicker/Device;->getPreferredType()I

    move-result v4

    if-ne v3, v4, :cond_1

    const/4 v4, 0x1

    :goto_2
    invoke-interface {v1, v4}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 97
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 99
    .end local v1    # "checkable":Landroid/widget/Checkable;
    :cond_0
    const/16 v4, 0x8

    goto :goto_1

    .restart local v1    # "checkable":Landroid/widget/Checkable;
    :cond_1
    move v4, v5

    .line 101
    goto :goto_2

    .line 103
    .end local v0    # "button":Landroid/widget/Button;
    .end local v1    # "checkable":Landroid/widget/Checkable;
    :cond_2
    return-void
.end method
