.class public Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;
.super Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;
.source "MockLocationTransmitter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;
    }
.end annotation


# static fields
.field private static final DELAY_MS:I = 0x3e8

.field private static final NETWORK_UPDATE_INTERVAL:I = 0x2710

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private coordinates:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private lastNetworkUpdate:J

.field private locationUpdater:Ljava/lang/Runnable;

.field private recordingDuration:J

.field private recordingEndTime:J

.field private recordingStartTime:J

.field private timeSource:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;

.field private transmitting:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->lastNetworkUpdate:J

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->coordinates:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->handler:Landroid/os/Handler;

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->transmitting:Z

    .line 68
    new-instance v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$1;-><init>(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)V

    iput-object v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->locationUpdater:Ljava/lang/Runnable;

    .line 90
    new-instance v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$2;-><init>(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)V

    iput-object v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->comparator:Ljava/util/Comparator;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;Landroid/location/Location;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->transmitLocation(Landroid/location/Location;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->lastNetworkUpdate:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->lastNetworkUpdate:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;Landroid/location/Location;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->transmitLocation(Landroid/location/Location;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->transmitting:Z

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private hasTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 119
    const/4 v0, 0x0

    .line 121
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "select DISTINCT tbl_name from sqlite_master where tbl_name = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-lez v3, :cond_0

    const/4 v2, 0x1

    .line 126
    :cond_0
    if-eqz v0, :cond_1

    .line 127
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 130
    :cond_1
    :goto_0
    return v2

    .line 123
    :catch_0
    move-exception v1

    .line 124
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Failed to read table info"

    invoke-virtual {v3, v4, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    if-eqz v0, :cond_1

    .line 127
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 126
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_2

    .line 127
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v2
.end method


# virtual methods
.method public getLocation()Landroid/location/Location;
    .locals 10

    .prologue
    .line 100
    iget-object v5, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->timeSource:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;

    invoke-interface {v5}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;->getCurrentTime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->recordingDuration:J

    rem-long/2addr v6, v8

    iget-wide v8, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->recordingStartTime:J

    add-long v2, v6, v8

    .line 101
    .local v2, "mappedTime":J
    new-instance v1, Landroid/location/Location;

    const-string v5, "gps"

    invoke-direct {v1, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 102
    .local v1, "dummy":Landroid/location/Location;
    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 103
    iget-object v5, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->coordinates:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->comparator:Ljava/util/Comparator;

    invoke-static {v5, v1, v6}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v4

    .line 104
    .local v4, "position":I
    if-gez v4, :cond_0

    .line 105
    neg-int v5, v4

    add-int/lit8 v4, v5, -0x1

    .line 108
    :cond_0
    iget-object v5, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->coordinates:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v4, v5, :cond_1

    .line 115
    .end local v1    # "dummy":Landroid/location/Location;
    :goto_0
    return-object v1

    .line 113
    .restart local v1    # "dummy":Landroid/location/Location;
    :cond_1
    new-instance v0, Landroid/location/Location;

    iget-object v5, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->coordinates:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/location/Location;

    invoke-direct {v0, v5}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    .line 114
    .local v0, "copy":Landroid/location/Location;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Landroid/location/Location;->setTime(J)V

    move-object v1, v0

    .line 115
    goto :goto_0
.end method

.method public hasLocationServices()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method public setSource(Ljava/lang/String;Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;)Z
    .locals 18
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "timeSource"    # Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;

    .prologue
    .line 135
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->coordinates:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    .line 136
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->timeSource:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;

    .line 138
    sget-object v14, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "Reading locations from db"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 139
    const/4 v14, 0x0

    const/high16 v15, 0x10000000

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 142
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v14, "location"

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v14}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->hasTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 143
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 144
    const/4 v14, 0x0

    .line 192
    :goto_0
    return v14

    .line 149
    :cond_0
    const/4 v4, 0x0

    .line 151
    .local v4, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v14, "select * from location order by time"

    const/4 v15, 0x0

    invoke-virtual {v5, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 153
    const/4 v9, 0x1

    .line 154
    .local v9, "first":Z
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 155
    const/4 v2, 0x0

    .line 156
    .local v2, "col":I
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "col":I
    .local v3, "col":I
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 157
    .local v11, "provider":Ljava/lang/String;
    new-instance v10, Landroid/location/Location;

    invoke-direct {v10, v11}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 158
    .local v10, "location":Landroid/location/Location;
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "col":I
    .restart local v2    # "col":I
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v14

    invoke-virtual {v10, v14}, Landroid/location/Location;->setAccuracy(F)V

    .line 159
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "col":I
    .restart local v3    # "col":I
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v14

    invoke-virtual {v10, v14, v15}, Landroid/location/Location;->setAltitude(D)V

    .line 160
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "col":I
    .restart local v2    # "col":I
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v14

    invoke-virtual {v10, v14}, Landroid/location/Location;->setBearing(F)V

    .line 161
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "col":I
    .restart local v3    # "col":I
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v14

    invoke-virtual {v10, v14, v15}, Landroid/location/Location;->setLatitude(D)V

    .line 162
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "col":I
    .restart local v2    # "col":I
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v14

    invoke-virtual {v10, v14, v15}, Landroid/location/Location;->setLongitude(D)V

    .line 163
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "col":I
    .restart local v3    # "col":I
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v14

    invoke-virtual {v10, v14}, Landroid/location/Location;->setSpeed(F)V

    .line 164
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "col":I
    .restart local v2    # "col":I
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 165
    .local v12, "time":J
    invoke-virtual {v10, v12, v13}, Landroid/location/Location;->setTime(J)V

    .line 167
    if-eqz v9, :cond_1

    .line 168
    const/4 v9, 0x0

    .line 169
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->recordingStartTime:J

    .line 171
    :cond_1
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->recordingEndTime:J

    .line 173
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->coordinates:Ljava/util/ArrayList;

    invoke-virtual {v14, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 176
    .end local v2    # "col":I
    .end local v9    # "first":Z
    .end local v10    # "location":Landroid/location/Location;
    .end local v11    # "provider":Ljava/lang/String;
    .end local v12    # "time":J
    :catch_0
    move-exception v8

    .line 177
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v14, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "Failed to load locations"

    invoke-virtual {v14, v15, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179
    if-eqz v4, :cond_2

    .line 180
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 183
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_2
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 186
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->coordinates:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    const/4 v15, 0x1

    if-le v14, v15, :cond_5

    .line 187
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->recordingEndTime:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->recordingStartTime:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->recordingDuration:J

    .line 188
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->recordingDuration:J

    long-to-double v14, v14

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v6, v14, v16

    .line 189
    .local v6, "duration":D
    sget-object v14, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Read "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->coordinates:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " locations from db - duration:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " seconds"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 190
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 179
    .end local v6    # "duration":D
    .restart local v9    # "first":Z
    :cond_3
    if-eqz v4, :cond_2

    .line 180
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 179
    .end local v9    # "first":Z
    :catchall_0
    move-exception v14

    if-eqz v4, :cond_4

    .line 180
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v14

    .line 192
    :cond_5
    const/4 v14, 0x0

    goto/16 :goto_0
.end method

.method protected startLocationUpdates()V
    .locals 4

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->transmitting:Z

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->transmitting:Z

    .line 56
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->locationUpdater:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 58
    :cond_0
    return-void
.end method

.method protected stopLocationUpdates()V
    .locals 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->transmitting:Z

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->transmitting:Z

    .line 64
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->locationUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 66
    :cond_0
    return-void
.end method
