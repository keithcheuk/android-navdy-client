.class Lcom/navdy/client/debug/gesture/GestureControlFragment$2;
.super Ljava/lang/Object;
.source "GestureControlFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/gesture/GestureControlFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/gesture/GestureControlFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/gesture/GestureControlFragment;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$2;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 89
    const/4 v0, 0x0

    .line 91
    .local v0, "message":Lcom/squareup/wire/Message;
    sparse-switch p2, :sswitch_data_0

    .line 128
    invoke-static {}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown keycode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 131
    :goto_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 132
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment$2;->this$0:Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-static {v1, v0}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->access$100(Lcom/navdy/client/debug/gesture/GestureControlFragment;Lcom/squareup/wire/Message;)V

    .line 134
    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 102
    :sswitch_0
    add-int/lit8 v1, p2, -0x8

    add-int/lit8 v7, v1, 0x1

    .line 103
    .local v7, "choice":I
    new-instance v0, Lcom/navdy/service/library/events/Notification;

    .end local v0    # "message":Lcom/squareup/wire/Message;
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, ""

    const-string v3, "Menu select"

    const-string v4, ""

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "sms"

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/Notification;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .restart local v0    # "message":Lcom/squareup/wire/Message;
    goto :goto_0

    .line 106
    .end local v7    # "choice":I
    :sswitch_1
    new-instance v0, Lcom/navdy/service/library/events/Notification;

    .end local v0    # "message":Lcom/squareup/wire/Message;
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, ""

    const-string v3, "+1 415-867-5309"

    const-string v4, ""

    const-string v5, "ANSWER"

    const-string v6, "sms"

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/Notification;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    .restart local v0    # "message":Lcom/squareup/wire/Message;
    goto :goto_0

    .line 109
    :sswitch_2
    new-instance v0, Lcom/navdy/service/library/events/Notification;

    .end local v0    # "message":Lcom/squareup/wire/Message;
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, ""

    const-string v3, "+1 800-555-1212"

    const-string v4, ""

    const-string v5, "IGNORE"

    const-string v6, "sms"

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/Notification;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .restart local v0    # "message":Lcom/squareup/wire/Message;
    goto :goto_0

    .line 112
    :sswitch_3
    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v0

    .line 113
    goto :goto_0

    .line 116
    :sswitch_4
    new-instance v0, Lcom/navdy/service/library/events/input/GestureEvent;

    .end local v0    # "message":Lcom/squareup/wire/Message;
    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_TO_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 117
    .restart local v0    # "message":Lcom/squareup/wire/Message;
    goto :goto_0

    .line 119
    :sswitch_5
    new-instance v0, Lcom/navdy/service/library/events/input/GestureEvent;

    .end local v0    # "message":Lcom/squareup/wire/Message;
    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 120
    .restart local v0    # "message":Lcom/squareup/wire/Message;
    goto :goto_0

    .line 122
    :sswitch_6
    new-instance v0, Lcom/navdy/service/library/events/input/GestureEvent;

    .end local v0    # "message":Lcom/squareup/wire/Message;
    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_RIGHT:Lcom/navdy/service/library/events/input/Gesture;

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 123
    .restart local v0    # "message":Lcom/squareup/wire/Message;
    goto :goto_0

    .line 125
    :sswitch_7
    new-instance v0, Lcom/navdy/service/library/events/input/GestureEvent;

    .end local v0    # "message":Lcom/squareup/wire/Message;
    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_UP:Lcom/navdy/service/library/events/input/Gesture;

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 126
    .restart local v0    # "message":Lcom/squareup/wire/Message;
    goto/16 :goto_0

    :cond_1
    move v1, v8

    .line 134
    goto :goto_1

    .line 91
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xc -> :sswitch_0
        0x13 -> :sswitch_7
        0x14 -> :sswitch_4
        0x15 -> :sswitch_5
        0x16 -> :sswitch_6
        0x1d -> :sswitch_1
        0x25 -> :sswitch_2
        0x29 -> :sswitch_3
        0x42 -> :sswitch_4
    .end sparse-switch
.end method
