.class Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;
.super Ljava/lang/Object;
.source "TaskManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/task/TaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PriorityRunnable"
.end annotation


# instance fields
.field final order:J

.field final priority:I

.field final runnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Ljava/lang/Runnable;Lcom/navdy/service/library/task/TaskManager$TaskPriority;J)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "priority"    # Lcom/navdy/service/library/task/TaskManager$TaskPriority;
    .param p3, "order"    # J

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object p1, p0, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->runnable:Ljava/lang/Runnable;

    .line 121
    invoke-virtual {p2}, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->getValue()I

    move-result v0

    iput v0, p0, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->priority:I

    .line 122
    iput-wide p3, p0, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->order:J

    .line 123
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->runnable:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "ex":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "TaskManager:"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
