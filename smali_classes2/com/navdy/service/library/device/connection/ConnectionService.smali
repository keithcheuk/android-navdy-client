.class public abstract Lcom/navdy/service/library/device/connection/ConnectionService;
.super Landroid/app/Service;
.source "ConnectionService.java"

# interfaces
.implements Lcom/navdy/service/library/device/connection/ConnectionListener$Listener;
.implements Lcom/navdy/service/library/device/RemoteDevice$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/connection/ConnectionService$State;,
        Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;,
        Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;
    }
.end annotation


# static fields
.field public static final ACCESSORY_IAP2:Ljava/util/UUID;

.field public static final ACTION_LINK_BANDWIDTH_LEVEL_CHANGED:Ljava/lang/String; = "LINK_BANDWIDTH_LEVEL_CHANGED"

.field public static final CATEGORY_NAVDY_LINK:Ljava/lang/String; = "NAVDY_LINK"

.field private static final CONNECT_TIMEOUT:I = 0x2710

.field private static final DEAD_CONNECTION_TIME:I = 0xea60

.field private static final DEVICES_CHANGED_EVENT:Lcom/navdy/service/library/events/connection/ConnectionStatus;

.field public static final DEVICE_IAP2:Ljava/util/UUID;

.field private static final EVENT_DISCONNECT:I = 0x3

.field protected static final EVENT_HEARTBEAT:I = 0x2

.field private static final EVENT_RESTART_LISTENERS:I = 0x4

.field private static final EVENT_STATE_CHANGE:I = 0x1

.field public static final EXTRA_BANDWIDTH_LEVEL:Ljava/lang/String; = "EXTRA_BANDWIDTH_MODE"

.field private static final HEARTBEAT_INTERVAL:I = 0xfa0

.field private static final IDLE_TIMEOUT:I = 0x3e8

.field public static final ILLEGAL_ARGUMENT:Ljava/lang/String; = "ILLEGAL_ARGUMENT"

.field public static final NAVDY_IAP_NAME:Ljava/lang/String; = "Navdy iAP"

.field public static final NAVDY_PROTO_SERVICE_NAME:Ljava/lang/String; = "Navdy"

.field public static final NAVDY_PROTO_SERVICE_UUID:Ljava/util/UUID;

.field public static final NAVDY_PROXY_TUNNEL_UUID:Ljava/util/UUID;

.field private static final PAIRING_TIMEOUT:I = 0x7530

.field public static final REASON_DEAD_CONNECTION:Ljava/lang/String; = "DEAD_CONNECTION"

.field private static final RECONNECT_DELAY:I = 0x7d0

.field private static final RECONNECT_TIMEOUT:I = 0x2710

.field protected static final RETRY_CONNECTING_AFTER_FAILURE_TIMEOUT:I = 0x7530

.field private static final SLEEP_TIMEOUT:I = 0xea60


# instance fields
.field private bluetoothReceiver:Landroid/content/BroadcastReceiver;

.field private broadcasting:Z

.field protected final connectionLock:Ljava/lang/Object;

.field protected deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

.field private forceReconnect:Z

.field protected inProcess:Z

.field private lastMessageReceivedTime:J

.field protected final listenerLock:Ljava/lang/Object;

.field private listenersArray:[Lcom/navdy/hud/app/IEventListener;

.field private listening:Z

.field protected final logger:Lcom/navdy/service/library/log/Logger;

.field protected mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

.field private mEventSource:Lcom/navdy/hud/app/IEventSource$Stub;

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/IEventListener;",
            ">;"
        }
    .end annotation
.end field

.field protected mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

.field protected mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

.field protected mWire:Lcom/squareup/wire/Wire;

.field private pendingConnectHandler:Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;

.field protected promiscuous:Z

.field private proxyService:Lcom/navdy/service/library/device/connection/ProxyService;

.field private volatile quitting:Z

.field protected reconnectRunnable:Ljava/lang/Runnable;

.field protected serverMode:Z

.field protected volatile serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

.field private volatile serviceLooper:Landroid/os/Looper;

.field protected state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field private stateAge:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_PAIRED_DEVICES_CHANGED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/connection/ConnectionStatus;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService;->DEVICES_CHANGED_EVENT:Lcom/navdy/service/library/events/connection/ConnectionStatus;

    .line 64
    const-string v0, "1992B7D7-C9B9-4F4F-AA7F-9FE33D95851E"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService;->NAVDY_PROTO_SERVICE_UUID:Ljava/util/UUID;

    .line 65
    const-string v0, "D72BC85F-F015-4F24-A72F-35924E10888F"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService;->NAVDY_PROXY_TUNNEL_UUID:Ljava/util/UUID;

    .line 68
    const-string v0, "00000000-deca-fade-deca-deafdecacafe"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService;->DEVICE_IAP2:Ljava/util/UUID;

    .line 69
    const-string v0, "00000000-deca-fade-deca-deafdecacaff"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService;->ACCESSORY_IAP2:Ljava/util/UUID;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 55
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    .line 94
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->connectionLock:Ljava/lang/Object;

    .line 100
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listenerLock:Ljava/lang/Object;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mListeners:Ljava/util/List;

    .line 102
    new-array v0, v2, [Lcom/navdy/hud/app/IEventListener;

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listenersArray:[Lcom/navdy/hud/app/IEventListener;

    .line 106
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->stateAge:J

    .line 113
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serverMode:Z

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->inProcess:Z

    .line 121
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->promiscuous:Z

    .line 123
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listening:Z

    .line 124
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->broadcasting:Z

    .line 126
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->forceReconnect:Z

    .line 277
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$1;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/ConnectionService$1;-><init>(Lcom/navdy/service/library/device/connection/ConnectionService;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mEventSource:Lcom/navdy/hud/app/IEventSource$Stub;

    .line 623
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->START:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 703
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$2;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/ConnectionService$2;-><init>(Lcom/navdy/service/library/device/connection/ConnectionService;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->reconnectRunnable:Ljava/lang/Runnable;

    .line 715
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$3;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/connection/ConnectionService$3;-><init>(Lcom/navdy/service/library/device/connection/ConnectionService;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/service/library/device/connection/ConnectionService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/ConnectionService;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listening:Z

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/service/library/device/connection/ConnectionService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/ConnectionService;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->broadcasting:Z

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/service/library/device/connection/ConnectionService;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/ConnectionService;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/service/library/device/connection/ConnectionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/ConnectionService;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->createListenerList()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/service/library/device/connection/ConnectionService;)[Lcom/navdy/hud/app/IEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/ConnectionService;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listenersArray:[Lcom/navdy/hud/app/IEventListener;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/service/library/device/connection/ConnectionService;Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;)Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/device/connection/ConnectionService;
    .param p1, "x1"    # Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->pendingConnectHandler:Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;

    return-object p1
.end method

.method private checkConnection()V
    .locals 6

    .prologue
    .line 193
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v2

    if-nez v2, :cond_4

    .line 194
    :cond_0
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dead connection remotedevice="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-nez v2, :cond_2

    const-string v2, "null"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " isConnected:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-nez v2, :cond_3

    const-string v2, "false"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 195
    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 208
    :cond_1
    :goto_2
    return-void

    .line 194
    :cond_2
    const-string v2, "not null"

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_1

    .line 196
    :cond_4
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->getLinkStatus()Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    if-ne v2, v3, :cond_1

    .line 197
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->lastMessageReceivedTime:J

    sub-long v0, v2, v4

    .line 198
    .local v0, "timeElapsed":J
    const-wide/32 v2, 0xea60

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 199
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dead connection timed out:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->reconnectAfterDeadConnection()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 201
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Reconncting after dead connection timed out disconnect"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 202
    const-string v2, "DEAD_CONNECTION"

    invoke-virtual {p0, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->reconnect(Ljava/lang/String;)V

    goto :goto_2

    .line 204
    :cond_5
    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_2
.end method

.method private createListenerList()V
    .locals 2

    .prologue
    .line 968
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/navdy/hud/app/IEventListener;

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listenersArray:[Lcom/navdy/hud/app/IEventListener;

    .line 969
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mListeners:Ljava/util/List;

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listenersArray:[Lcom/navdy/hud/app/IEventListener;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 970
    return-void
.end method

.method private handleDeviceConnect(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 869
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onDeviceConnected:remembering device"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 870
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionService;->rememberPairedDevice(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 872
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onDeviceConnected:heartBeat started"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 873
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->lastMessageReceivedTime:J

    .line 875
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 876
    .local v0, "appContext":Landroid/content/Context;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connecting with app context: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 877
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->startLink()Z

    move-result v1

    if-nez v1, :cond_0

    .line 879
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 881
    :cond_0
    return-void
.end method

.method private handleRemoteDisconnect(Lcom/navdy/service/library/events/connection/DisconnectRequest;)V
    .locals 1
    .param p1, "disconnectRequest"    # Lcom/navdy/service/library/events/connection/DisconnectRequest;

    .prologue
    .line 975
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_1

    .line 976
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/DisconnectRequest;->forget:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/connection/DisconnectRequest;->forget:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 977
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->forgetPairedDevice(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 979
    :cond_0
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 981
    :cond_1
    return-void
.end method

.method private hasPaired()Z
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->hasPaired()Z

    move-result v0

    return v0
.end method

.method private sendPingIfNeeded()V
    .locals 8

    .prologue
    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    iget-wide v6, v1, Lcom/navdy/service/library/device/RemoteDevice;->lastMessageSentTime:J

    sub-long v2, v4, v6

    .line 178
    .local v2, "timeElapsed":J
    const-wide/16 v4, 0xfa0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 179
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    new-instance v4, Lcom/navdy/service/library/events/Ping;

    invoke-direct {v4}, Lcom/navdy/service/library/events/Ping;-><init>()V

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 180
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serverMode:Z

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "NAVDY-PACKET [H2P-Outgoing-Event] Ping"

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    .end local v2    # "timeElapsed":J
    :cond_0
    :goto_0
    return-void

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public connect(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V
    .locals 2
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 559
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice;

    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->inProcess:Z

    invoke-direct {v0, p0, p1, v1}, Lcom/navdy/service/library/device/RemoteDevice;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V

    .line 560
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setActiveDevice(Lcom/navdy/service/library/device/RemoteDevice;)Z

    .line 561
    return-void
.end method

.method protected abstract createProxyService()Lcom/navdy/service/library/device/connection/ProxyService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected enterState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V
    .locals 5
    .param p1, "state"    # Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .prologue
    const/4 v4, 0x2

    .line 641
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Entering state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 642
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DESTROYED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-ne p1, v1, :cond_2

    .line 643
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listening:Z

    if-eqz v1, :cond_0

    .line 644
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->stopListeners()V

    .line 646
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->broadcasting:Z

    if-eqz v1, :cond_1

    .line 647
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->stopBroadcasters()V

    .line 649
    :cond_1
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 701
    :goto_0
    return-void

    .line 652
    :cond_2
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->stateAge:J

    .line 653
    const/16 v0, 0x3e8

    .line 654
    .local v0, "timeout":I
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$4;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 699
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->removeMessages(I)V

    .line 700
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    int-to-long v2, v0

    invoke-virtual {v1, v4, v2, v3}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 657
    :pswitch_0
    const/16 v0, 0x3e8

    .line 658
    goto :goto_1

    .line 661
    :pswitch_1
    const/16 v0, 0x7530

    .line 662
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->startBroadcasters()V

    .line 663
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->startListeners()V

    .line 664
    new-instance v1, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_PAIRING:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lcom/navdy/service/library/events/connection/ConnectionStatus;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    goto :goto_1

    .line 668
    :pswitch_2
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->promiscuous:Z

    if-eqz v1, :cond_4

    .line 669
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->startBroadcasters()V

    .line 671
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->startListeners()V

    .line 672
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serverMode:Z

    if-eqz v1, :cond_3

    .line 673
    const/16 v0, 0x7530

    goto :goto_1

    .line 677
    :pswitch_3
    const/16 v0, 0x2710

    .line 678
    goto :goto_1

    .line 680
    :pswitch_4
    const/16 v0, 0x2710

    .line 681
    goto :goto_1

    .line 683
    :pswitch_5
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v1, :cond_3

    .line 684
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->disconnect()Z

    goto :goto_1

    .line 689
    :pswitch_6
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->stopListeners()V

    .line 690
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->promiscuous:Z

    if-eqz v1, :cond_5

    .line 691
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->stopBroadcasters()V

    .line 693
    :cond_5
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-direct {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->handleDeviceConnect(Lcom/navdy/service/library/device/RemoteDevice;)V

    goto :goto_1

    .line 654
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected exitState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V
    .locals 3
    .param p1, "state"    # Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .prologue
    .line 736
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exiting state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 737
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$4;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 753
    :cond_0
    :goto_0
    return-void

    .line 740
    :sswitch_0
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->startProxyService()V

    goto :goto_0

    .line 743
    :sswitch_1
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->reconnectRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 746
    :sswitch_2
    iget-boolean v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->promiscuous:Z

    if-nez v0, :cond_0

    .line 747
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->stopBroadcasters()V

    goto :goto_0

    .line 737
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x5 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method protected forgetPairedDevice(Landroid/bluetooth/BluetoothDevice;)V
    .locals 1
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 863
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {v0, p1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->removePairedConnection(Landroid/bluetooth/BluetoothDevice;)V

    .line 864
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService;->DEVICES_CHANGED_EVENT:Lcom/navdy/service/library/events/connection/ConnectionStatus;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 865
    return-void
.end method

.method protected forgetPairedDevice(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 858
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice;->getActiveConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->removePairedConnection(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 859
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService;->DEVICES_CHANGED_EVENT:Lcom/navdy/service/library/events/connection/ConnectionStatus;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 860
    return-void
.end method

.method protected forwardEventLocally(Lcom/squareup/wire/Message;)V
    .locals 2
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 383
    invoke-static {p1}, Lcom/navdy/service/library/events/NavdyEventUtil;->eventFromMessage(Lcom/squareup/wire/Message;)Lcom/navdy/service/library/events/NavdyEvent;

    move-result-object v0

    .line 384
    .local v0, "event":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-virtual {v0}, Lcom/navdy/service/library/events/NavdyEvent;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally([B)V

    .line 385
    return-void
.end method

.method protected forwardEventLocally([B)V
    .locals 2
    .param p1, "eventData"    # [B

    .prologue
    .line 390
    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mEventSource:Lcom/navdy/hud/app/IEventSource$Stub;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/IEventSource$Stub;->postEvent([B)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    :goto_0
    return-void

    .line 391
    :catch_0
    move-exception v0

    .line 392
    .local v0, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected abstract getConnectionListeners(Landroid/content/Context;)[Lcom/navdy/service/library/device/connection/ConnectionListener;
.end method

.method protected abstract getRemoteDeviceBroadcasters()[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;
.end method

.method protected handleDeviceDisconnect(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 5
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    const/4 v4, 0x0

    .line 885
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "device disconnect:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cause:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 886
    if-eqz p1, :cond_0

    .line 887
    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice;->stopLink()Z

    .line 890
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-ne v1, p1, :cond_2

    .line 891
    sget-object v1, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->ABORTED:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    if-eq p2, v1, :cond_1

    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->forceReconnect:Z

    if-eqz v1, :cond_3

    :cond_1
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serverMode:Z

    if-eqz v1, :cond_3

    .line 893
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->forceReconnect:Z

    .line 894
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-direct {v0, p0, v1, v4}, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;-><init>(Lcom/navdy/service/library/device/connection/ConnectionService;Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection;)V

    .line 895
    .local v0, "connectHandler":Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;
    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->run()V

    .line 901
    .end local v0    # "connectHandler":Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;
    :cond_2
    :goto_0
    return-void

    .line 897
    :cond_3
    invoke-virtual {p0, v4}, Lcom/navdy/service/library/device/connection/ConnectionService;->setRemoteDevice(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 898
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0
.end method

.method protected heartBeat()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 756
    iget-wide v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->stateAge:J

    const-wide/16 v4, 0xa

    rem-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 757
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Heartbeat: in state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 759
    :cond_0
    iget-wide v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->stateAge:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->stateAge:J

    .line 761
    const/16 v0, 0x3e8

    .line 762
    .local v0, "timeout":I
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$4;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State:[I

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/ConnectionService$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 811
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->removeMessages(I)V

    .line 812
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    int-to-long v2, v0

    invoke-virtual {v1, v6, v2, v3}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 813
    return-void

    .line 764
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->pendingConnectHandler:Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;

    if-eqz v1, :cond_1

    .line 765
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->pendingConnectHandler:Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->run()V

    goto :goto_0

    .line 766
    :cond_1
    invoke-direct {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->hasPaired()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serverMode:Z

    if-eqz v1, :cond_2

    .line 768
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->PAIRING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0

    .line 769
    :cond_2
    invoke-direct {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->hasPaired()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 771
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->LISTENING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0

    .line 773
    :cond_3
    const v0, 0xea60

    .line 775
    goto :goto_0

    .line 777
    :pswitch_2
    const/16 v0, 0x7530

    .line 778
    goto :goto_0

    .line 780
    :pswitch_3
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serverMode:Z

    if-eqz v1, :cond_6

    .line 781
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v1, :cond_4

    .line 782
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->RECONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 783
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->reconnectRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 785
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->needAutoSearch()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 786
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->SEARCHING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0

    .line 788
    :cond_5
    const v0, 0xea60

    goto :goto_0

    .line 793
    :cond_6
    const v0, 0xea60

    .line 795
    goto :goto_0

    .line 799
    :pswitch_4
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0

    .line 802
    :pswitch_5
    const/16 v0, 0xfa0

    .line 803
    invoke-direct {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->sendPingIfNeeded()V

    .line 804
    invoke-direct {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->checkConnection()V

    goto :goto_0

    .line 762
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 1015
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needAutoSearch()Z
    .locals 1

    .prologue
    .line 816
    const/4 v0, 0x1

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 216
    const-class v0, Lcom/navdy/hud/app/IEventSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "returning IEventSource API endpoint"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mEventSource:Lcom/navdy/hud/app/IEventSource$Stub;

    .line 221
    :goto_0
    return-object v0

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid action:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 221
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConnected(Lcom/navdy/service/library/device/connection/ConnectionListener;Lcom/navdy/service/library/device/connection/Connection;)V
    .locals 4
    .param p1, "connectionListener"    # Lcom/navdy/service/library/device/connection/ConnectionListener;
    .param p2, "connection"    # Lcom/navdy/service/library/device/connection/Connection;

    .prologue
    .line 493
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "listener connected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 494
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice;

    .line 495
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p2}, Lcom/navdy/service/library/device/connection/Connection;->getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v2

    iget-boolean v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->inProcess:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/device/RemoteDevice;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V

    .line 496
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    invoke-virtual {p0, v0, p2}, Lcom/navdy/service/library/device/connection/ConnectionService;->setActiveDevice(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection;)Z

    .line 497
    return-void
.end method

.method public onConnectionFailed(Lcom/navdy/service/library/device/connection/ConnectionListener;)V
    .locals 2
    .param p1, "connectionListener"    # Lcom/navdy/service/library/device/connection/ConnectionListener;

    .prologue
    .line 501
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onConnectionFailed:restart listeners"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 502
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 503
    return-void
.end method

.method public onCreate()V
    .locals 7

    .prologue
    .line 227
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    .line 228
    .local v0, "mainThreadId":J
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ConnectionService created: mainthread:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 229
    new-instance v3, Lcom/squareup/wire/Wire;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Lcom/navdy/service/library/events/Ext_NavdyEvent;

    aput-object v6, v4, v5

    invoke-direct {v3, v4}, Lcom/squareup/wire/Wire;-><init>([Ljava/lang/Class;)V

    iput-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mWire:Lcom/squareup/wire/Wire;

    .line 231
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "ConnectionService"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 232
    .local v2, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 234
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceLooper:Landroid/os/Looper;

    .line 235
    new-instance v3, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    iget-object v4, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceLooper:Landroid/os/Looper;

    invoke-direct {v3, p0, v4}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;-><init>(Lcom/navdy/service/library/device/connection/ConnectionService;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    .line 237
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/device/connection/ConnectionService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 240
    invoke-static {p0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->getInstance(Landroid/content/Context;)Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    .line 241
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 263
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ConnectionService destroyed.  mwhahahaha."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 265
    :try_start_0
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DESTROYED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 266
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 267
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceLooper:Landroid/os/Looper;

    invoke-virtual {v1}, Landroid/os/Looper;->quitSafely()V

    .line 268
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->stopProxyService()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :goto_0
    return-void

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onDeviceConnectFailure(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 4
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .prologue
    .line 832
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->RECONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-ne v0, v1, :cond_0

    .line 833
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDeviceConnectFailure - retrying"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 834
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->reconnectRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 844
    :goto_0
    return-void

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDeviceConnectFailure - switching to idle state"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 837
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serverMode:Z

    if-nez v0, :cond_1

    .line 838
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setRemoteDevice(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 842
    :goto_1
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0

    .line 840
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Not clearing the remote device, to attempt reconnect"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 1
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 827
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 828
    return-void
.end method

.method public onDeviceConnecting(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 823
    return-void
.end method

.method public onDeviceDisconnected(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 5
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 848
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    const/4 v2, 0x3

    invoke-virtual {p2}, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->ordinal()I

    move-result v3

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v3, v4, p1}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 849
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 850
    return-void
.end method

.method public onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 2
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 948
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->lastMessageReceivedTime:J

    .line 949
    iget-object v0, p2, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DisconnectRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-ne v0, v1, :cond_0

    .line 950
    sget-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->disconnectRequest:Lcom/squareup/wire/Extension;

    invoke-virtual {p2, v0}, Lcom/navdy/service/library/events/NavdyEvent;->getExtension(Lcom/squareup/wire/Extension;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/connection/DisconnectRequest;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->handleRemoteDisconnect(Lcom/navdy/service/library/events/connection/DisconnectRequest;)V

    .line 952
    :cond_0
    return-void
.end method

.method public onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;[B)V
    .locals 10
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "eventData"    # [B

    .prologue
    .line 905
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->lastMessageReceivedTime:J

    .line 906
    invoke-static {p2}, Lcom/navdy/service/library/events/WireUtil;->getEventType([B)Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    move-result-object v3

    .line 907
    .local v3, "messageType":Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    iget-object v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 908
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->name()Ljava/lang/String;

    move-result-object v2

    .line 909
    .local v2, "messageName":Ljava/lang/String;
    :goto_0
    iget-object v8, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serverMode:Z

    if-eqz v7, :cond_3

    const-string v7, "NAVDY-PACKET [P2H"

    :goto_1
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "-Event] "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " size:"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v9, p2

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 911
    .end local v2    # "messageName":Ljava/lang/String;
    :cond_0
    if-eqz v3, :cond_1

    sget-object v7, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->Ping:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-ne v3, v7, :cond_4

    .line 944
    :cond_1
    :goto_2
    return-void

    .line 908
    :cond_2
    const-string v2, "UNKNOWN"

    goto :goto_0

    .line 909
    .restart local v2    # "messageName":Ljava/lang/String;
    :cond_3
    const-string v7, "NAVDY-PACKET [H2P"

    goto :goto_1

    .line 913
    .end local v2    # "messageName":Ljava/lang/String;
    :cond_4
    sget-object v7, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DisconnectRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-ne v3, v7, :cond_5

    .line 915
    :try_start_0
    iget-object v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v8, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v7, p2, v8}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 916
    .local v1, "event":Lcom/navdy/service/library/events/NavdyEvent;
    sget-object v7, Lcom/navdy/service/library/events/Ext_NavdyEvent;->disconnectRequest:Lcom/squareup/wire/Extension;

    invoke-virtual {v1, v7}, Lcom/navdy/service/library/events/NavdyEvent;->getExtension(Lcom/squareup/wire/Extension;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/service/library/events/connection/DisconnectRequest;

    invoke-direct {p0, v7}, Lcom/navdy/service/library/device/connection/ConnectionService;->handleRemoteDisconnect(Lcom/navdy/service/library/events/connection/DisconnectRequest;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 917
    .end local v1    # "event":Lcom/navdy/service/library/events/NavdyEvent;
    :catch_0
    move-exception v6

    .line 918
    .local v6, "t":Ljava/lang/Throwable;
    iget-object v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Failed to parse event"

    invoke-virtual {v7, v8, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 920
    .end local v6    # "t":Ljava/lang/Throwable;
    :cond_5
    invoke-virtual {p0, p2, v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->processEvent([BLcom/navdy/service/library/events/NavdyEvent$MessageType;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 923
    sget-object v7, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DeviceInfo:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-ne v3, v7, :cond_6

    .line 925
    :try_start_1
    iget-object v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v7}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 926
    iget-object v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mWire:Lcom/squareup/wire/Wire;

    const-class v8, Lcom/navdy/service/library/events/NavdyEvent;

    invoke-virtual {v7, p2, v8}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/NavdyEvent;

    .line 928
    .local v4, "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {v4}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/DeviceInfo;

    .line 930
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    new-instance v7, Lcom/navdy/service/library/events/DeviceInfo$Builder;

    invoke-direct {v7, v0}, Lcom/navdy/service/library/events/DeviceInfo$Builder;-><init>(Lcom/navdy/service/library/events/DeviceInfo;)V

    iget-object v8, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 931
    invoke-virtual {v8}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->deviceId(Ljava/lang/String;)Lcom/navdy/service/library/events/DeviceInfo$Builder;

    move-result-object v7

    .line 932
    invoke-virtual {v7}, Lcom/navdy/service/library/events/DeviceInfo$Builder;->build()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v5

    .line 933
    .local v5, "patchedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    iget-object v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v7, v5}, Lcom/navdy/service/library/device/RemoteDevice;->setDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V

    .line 934
    iget-object v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "set remote device info"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 935
    invoke-static {v5}, Lcom/navdy/service/library/events/NavdyEventUtil;->eventFromMessage(Lcom/squareup/wire/Message;)Lcom/navdy/service/library/events/NavdyEvent;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/NavdyEvent;->toByteArray()[B

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally([B)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 937
    .end local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .end local v4    # "navdyEvent":Lcom/navdy/service/library/events/NavdyEvent;
    .end local v5    # "patchedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :catch_1
    move-exception v6

    .line 938
    .restart local v6    # "t":Ljava/lang/Throwable;
    iget-object v7, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Failed to parse deviceinfo"

    invoke-virtual {v7, v8, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 941
    .end local v6    # "t":Ljava/lang/Throwable;
    :cond_6
    invoke-virtual {p0, p2}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally([B)V

    goto/16 :goto_2
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 245
    if-eqz p1, :cond_0

    .line 248
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-ne v0, v1, :cond_1

    .line 250
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "BT enabled, disconnecting since app likely crashed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 251
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 258
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "BT enabled, switching to IDLE state"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 254
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    goto :goto_0
.end method

.method public onStartFailure(Lcom/navdy/service/library/device/connection/ConnectionListener;)V
    .locals 3
    .param p1, "connectionListener"    # Lcom/navdy/service/library/device/connection/ConnectionListener;

    .prologue
    .line 483
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to start listening:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 484
    return-void
.end method

.method public onStarted(Lcom/navdy/service/library/device/connection/ConnectionListener;)V
    .locals 2
    .param p1, "connectionListener"    # Lcom/navdy/service/library/device/connection/ConnectionListener;

    .prologue
    .line 478
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "started listening"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 479
    return-void
.end method

.method public onStopped(Lcom/navdy/service/library/device/connection/ConnectionListener;)V
    .locals 3
    .param p1, "connectionListener"    # Lcom/navdy/service/library/device/connection/ConnectionListener;

    .prologue
    .line 488
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopped listening:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 489
    return-void
.end method

.method protected processEvent([BLcom/navdy/service/library/events/NavdyEvent$MessageType;)Z
    .locals 1
    .param p1, "eventData"    # [B
    .param p2, "messageType"    # Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .prologue
    .line 958
    const/4 v0, 0x0

    return v0
.end method

.method protected processLocalEvent([BLcom/navdy/service/library/events/NavdyEvent$MessageType;)Z
    .locals 1
    .param p1, "eventData"    # [B
    .param p2, "messageType"    # Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .prologue
    .line 964
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized reconnect(Ljava/lang/String;)V
    .locals 1
    .param p1, "reconnectReason"    # Ljava/lang/String;

    .prologue
    .line 570
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    .line 571
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->forceReconnect:Z

    .line 572
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 574
    :cond_0
    monitor-exit p0

    return-void

    .line 570
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public reconnectAfterDeadConnection()Z
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x0

    return v0
.end method

.method protected rememberPairedDevice(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 853
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->deviceRegistry:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-virtual {p1}, Lcom/navdy/service/library/device/RemoteDevice;->getActiveConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->addPairedConnection(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 854
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService;->DEVICES_CHANGED_EVENT:Lcom/navdy/service/library/events/connection/ConnectionStatus;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 855
    return-void
.end method

.method protected sendEventsOnLocalConnect()V
    .locals 0

    .prologue
    .line 1020
    return-void
.end method

.method public sendMessage(Lcom/squareup/wire/Message;)V
    .locals 1
    .param p1, "event"    # Lcom/squareup/wire/Message;

    .prologue
    .line 1009
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1010
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0, p1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 1012
    :cond_0
    return-void
.end method

.method public setActiveDevice(Lcom/navdy/service/library/device/RemoteDevice;)Z
    .locals 1
    .param p1, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 564
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/service/library/device/connection/ConnectionService;->setActiveDevice(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized setActiveDevice(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection;)Z
    .locals 2
    .param p1, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "connection"    # Lcom/navdy/service/library/device/connection/Connection;

    .prologue
    .line 580
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v1, :cond_1

    .line 581
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnecting()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 582
    :cond_0
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 589
    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    .line 590
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;-><init>(Lcom/navdy/service/library/device/connection/ConnectionService;Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection;)V

    .line 591
    .local v0, "connectHandler":Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-nez v1, :cond_4

    .line 593
    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 600
    .end local v0    # "connectHandler":Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;
    :cond_2
    :goto_1
    const/4 v1, 0x1

    monitor-exit p0

    return v1

    .line 585
    :cond_3
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setRemoteDevice(Lcom/navdy/service/library/device/RemoteDevice;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 580
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 596
    .restart local v0    # "connectHandler":Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;
    :cond_4
    :try_start_2
    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->pendingConnectHandler:Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method protected setBandwidthLevel(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 1023
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 1024
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1, p1}, Lcom/navdy/service/library/device/RemoteDevice;->setLinkBandwidthLevel(I)V

    .line 1025
    new-instance v0, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;-><init>()V

    .line 1026
    .local v0, "builder":Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;->bandwidthLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;

    .line 1027
    invoke-virtual {v0}, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;->build()Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->forwardEventLocally(Lcom/squareup/wire/Message;)V

    .line 1029
    .end local v0    # "builder":Lcom/navdy/service/library/events/connection/LinkPropertiesChanged$Builder;
    :cond_1
    return-void
.end method

.method protected declared-synchronized setRemoteDevice(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 1
    .param p1, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 508
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eq p1, v0, :cond_1

    .line 509
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 512
    :cond_0
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 513
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    :cond_1
    monitor-exit p0

    return-void

    .line 508
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V
    .locals 3
    .param p1, "newState"    # Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .prologue
    const/4 v2, 0x1

    .line 626
    iget-boolean v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->quitting:Z

    if-eqz v1, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-eq v1, p1, :cond_0

    .line 630
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DESTROYED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-ne p1, v1, :cond_2

    .line 631
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->quitting:Z

    .line 634
    :cond_2
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    invoke-virtual {v1, v2, p1}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 635
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected startBroadcasters()V
    .locals 6

    .prologue
    .line 397
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->connectionLock:Ljava/lang/Object;

    monitor-enter v3

    .line 398
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->broadcasting:Z

    .line 399
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    if-nez v2, :cond_0

    .line 400
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->getRemoteDeviceBroadcasters()[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    .line 402
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 403
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "starting connection broadcaster:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    :try_start_1
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;->start()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 402
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 406
    :catch_0
    move-exception v1

    .line 408
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_2
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 411
    .end local v0    # "i":I
    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .restart local v0    # "i":I
    :cond_1
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 412
    return-void
.end method

.method protected startListeners()V
    .locals 6

    .prologue
    .line 436
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->connectionLock:Ljava/lang/Object;

    monitor-enter v3

    .line 437
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listening:Z

    .line 438
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    if-nez v2, :cond_0

    .line 439
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "initializing connector listener"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 440
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->getConnectionListeners(Landroid/content/Context;)[Lcom/navdy/service/library/device/connection/ConnectionListener;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    .line 441
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 442
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    aget-object v2, v2, v0

    invoke-virtual {v2, p0}, Lcom/navdy/service/library/device/connection/ConnectionListener;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 441
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 445
    .end local v0    # "i":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 446
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "starting connection listener:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448
    :try_start_1
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/ConnectionListener;->start()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 449
    :catch_0
    move-exception v1

    .line 451
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_2
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 454
    .end local v0    # "i":I
    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .restart local v0    # "i":I
    :cond_1
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 455
    return-void
.end method

.method protected startProxyService()V
    .locals 5

    .prologue
    .line 988
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->proxyService:Lcom/navdy/service/library/device/connection/ProxyService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->proxyService:Lcom/navdy/service/library/device/connection/ProxyService;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/ProxyService;->isAlive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 989
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 990
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    .line 989
    invoke-static {v2, v3}, Lcom/navdy/service/library/util/SystemUtils;->getProcessName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 991
    .local v1, "processName":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": start service for proxy"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 993
    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->createProxyService()Lcom/navdy/service/library/device/connection/ProxyService;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->proxyService:Lcom/navdy/service/library/device/connection/ProxyService;

    .line 994
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->proxyService:Lcom/navdy/service/library/device/connection/ProxyService;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/ProxyService;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 999
    .end local v1    # "processName":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 995
    .restart local v1    # "processName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 996
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Failed to start proxy service"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected stopBroadcasters()V
    .locals 6

    .prologue
    .line 415
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "stopping all broadcasters"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 416
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->connectionLock:Ljava/lang/Object;

    monitor-enter v3

    .line 417
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->broadcasting:Z

    .line 418
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    if-eqz v2, :cond_0

    .line 419
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 420
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopping connection broadcaster:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    :try_start_1
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDeviceBroadcasters:[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;->stop()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 419
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 423
    :catch_0
    move-exception v1

    .line 425
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_2
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 429
    .end local v0    # "i":I
    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_0
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 430
    return-void
.end method

.method protected stopListeners()V
    .locals 6

    .prologue
    .line 458
    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->connectionLock:Ljava/lang/Object;

    monitor-enter v3

    .line 459
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->listening:Z

    .line 460
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    if-eqz v2, :cond_0

    .line 461
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 462
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopping:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    :try_start_1
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->mConnectionListeners:[Lcom/navdy/service/library/device/connection/ConnectionListener;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/ConnectionListener;->stop()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 461
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 465
    :catch_0
    move-exception v1

    .line 467
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_2
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 471
    .end local v0    # "i":I
    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_0
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 472
    return-void
.end method

.method protected stopProxyService()V
    .locals 1

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->proxyService:Lcom/navdy/service/library/device/connection/ProxyService;

    if-eqz v0, :cond_0

    .line 1003
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->proxyService:Lcom/navdy/service/library/device/connection/ProxyService;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ProxyService;->cancel()V

    .line 1004
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionService;->proxyService:Lcom/navdy/service/library/device/connection/ProxyService;

    .line 1006
    :cond_0
    return-void
.end method
