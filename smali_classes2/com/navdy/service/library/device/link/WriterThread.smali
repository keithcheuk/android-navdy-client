.class public Lcom/navdy/service/library/device/link/WriterThread;
.super Lcom/navdy/service/library/device/link/IOThread;
.source "WriterThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;
    }
.end annotation


# instance fields
.field private final mEventProcessor:Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;

.field private final mEventQueue:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lcom/navdy/service/library/device/link/EventRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final mmEventWriter:Lcom/navdy/service/library/events/NavdyEventWriter;

.field private mmOutStream:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/LinkedBlockingDeque;Ljava/io/OutputStream;Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;)V
    .locals 2
    .param p2, "outputStream"    # Ljava/io/OutputStream;
    .param p3, "processor"    # Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lcom/navdy/service/library/device/link/EventRequest;",
            ">;",
            "Ljava/io/OutputStream;",
            "Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "queue":Ljava/util/concurrent/LinkedBlockingDeque;, "Ljava/util/concurrent/LinkedBlockingDeque<Lcom/navdy/service/library/device/link/EventRequest;>;"
    invoke-direct {p0}, Lcom/navdy/service/library/device/link/IOThread;-><init>()V

    .line 33
    const-string v0, "WriterThread"

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/link/WriterThread;->setName(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/navdy/service/library/device/link/WriterThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "create WriterThread"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 35
    iput-object p2, p0, Lcom/navdy/service/library/device/link/WriterThread;->mmOutStream:Ljava/io/OutputStream;

    .line 36
    new-instance v0, Lcom/navdy/service/library/events/NavdyEventWriter;

    iget-object v1, p0, Lcom/navdy/service/library/device/link/WriterThread;->mmOutStream:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/NavdyEventWriter;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/link/WriterThread;->mmEventWriter:Lcom/navdy/service/library/events/NavdyEventWriter;

    .line 37
    iput-object p1, p0, Lcom/navdy/service/library/device/link/WriterThread;->mEventQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 38
    iput-object p3, p0, Lcom/navdy/service/library/device/link/WriterThread;->mEventProcessor:Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;

    .line 39
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Lcom/navdy/service/library/device/link/IOThread;->cancel()V

    .line 97
    iget-object v0, p0, Lcom/navdy/service/library/device/link/WriterThread;->mmOutStream:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/device/link/WriterThread;->mmOutStream:Ljava/io/OutputStream;

    .line 99
    return-void
.end method

.method public run()V
    .locals 9

    .prologue
    .line 43
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "starting WriterThread loop."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 44
    :cond_0
    :goto_0
    iget-boolean v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->closing:Z

    if-nez v5, :cond_1

    .line 45
    const/4 v3, 0x0

    .line 47
    .local v3, "request":Lcom/navdy/service/library/device/link/EventRequest;
    :try_start_0
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->mEventQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v5}, Ljava/util/concurrent/LinkedBlockingDeque;->take()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/navdy/service/library/device/link/EventRequest;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_1
    iget-boolean v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->closing:Z

    if-eqz v5, :cond_2

    .line 92
    .end local v3    # "request":Lcom/navdy/service/library/device/link/EventRequest;
    :cond_1
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Writer thread ending"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 93
    return-void

    .line 48
    .restart local v3    # "request":Lcom/navdy/service/library/device/link/EventRequest;
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Ljava/lang/InterruptedException;
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "WriterThread interrupted"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 56
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_2
    if-nez v3, :cond_3

    .line 57
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "WriterThread: unable to retrieve event from queue"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_3
    iget-object v2, v3, Lcom/navdy/service/library/device/link/EventRequest;->eventData:[B

    .line 63
    .local v2, "eventData":[B
    array-length v5, v2

    const/high16 v6, 0x80000

    if-le v5, v6, :cond_4

    .line 64
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writer Max packet size exceeded ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] bytes["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x32

    .line 65
    invoke-static {v2, v7, v8}, Lcom/navdy/service/library/util/IOUtils;->bytesToHexString([BII)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 68
    :cond_4
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->mEventProcessor:Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;

    if-eqz v5, :cond_5

    .line 69
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->mEventProcessor:Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;

    invoke-interface {v5, v2}, Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;->processEvent([B)[B

    move-result-object v2

    .line 72
    :cond_5
    if-eqz v2, :cond_0

    .line 77
    const/4 v4, 0x0

    .line 79
    .local v4, "sent":Z
    :try_start_1
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->mmEventWriter:Lcom/navdy/service/library/events/NavdyEventWriter;

    iget-object v6, v3, Lcom/navdy/service/library/device/link/EventRequest;->eventData:[B

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/NavdyEventWriter;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 80
    const/4 v4, 0x1

    .line 87
    :cond_6
    :goto_2
    if-nez v4, :cond_0

    .line 88
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "send failed"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 89
    sget-object v5, Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;->SEND_FAILED:Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/device/link/EventRequest;->callCompletionHandlers(Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;)V

    goto/16 :goto_0

    .line 81
    :catch_1
    move-exception v1

    .line 82
    .local v1, "e":Ljava/io/IOException;
    iget-boolean v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->closing:Z

    if-nez v5, :cond_6

    .line 83
    iget-object v5, p0, Lcom/navdy/service/library/device/link/WriterThread;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error writing event:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_2
.end method
