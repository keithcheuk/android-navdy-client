.class public final Lcom/navdy/service/library/events/calendars/CalendarEventUpdates$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CalendarEventUpdates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;",
        ">;"
    }
.end annotation


# instance fields
.field public calendar_events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/calendars/CalendarEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 57
    if-nez p1, :cond_0

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;->calendar_events:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates$Builder;->calendar_events:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;-><init>(Lcom/navdy/service/library/events/calendars/CalendarEventUpdates$Builder;Lcom/navdy/service/library/events/calendars/CalendarEventUpdates$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates$Builder;->build()Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;

    move-result-object v0

    return-object v0
.end method

.method public calendar_events(Ljava/util/List;)Lcom/navdy/service/library/events/calendars/CalendarEventUpdates$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/calendars/CalendarEvent;",
            ">;)",
            "Lcom/navdy/service/library/events/calendars/CalendarEventUpdates$Builder;"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "calendar_events":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/calendars/CalendarEvent;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates$Builder;->calendar_events:Ljava/util/List;

    .line 66
    return-object p0
.end method
