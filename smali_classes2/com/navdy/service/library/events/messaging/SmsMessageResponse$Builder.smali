.class public final Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SmsMessageResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/messaging/SmsMessageResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/messaging/SmsMessageResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public number:Ljava/lang/String;

.field public status:Lcom/navdy/service/library/events/RequestStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/messaging/SmsMessageResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/messaging/SmsMessageResponse;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 90
    if-nez p1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 92
    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->number:Ljava/lang/String;

    .line 93
    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->name:Ljava/lang/String;

    .line 94
    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->id:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/messaging/SmsMessageResponse;
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->checkRequiredFields()V

    .line 126
    new-instance v0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;-><init>(Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;Lcom/navdy/service/library/events/messaging/SmsMessageResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->build()Lcom/navdy/service/library/events/messaging/SmsMessageResponse;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->id:Ljava/lang/String;

    .line 120
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->name:Ljava/lang/String;

    .line 112
    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->number:Ljava/lang/String;

    .line 107
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 102
    return-object p0
.end method
