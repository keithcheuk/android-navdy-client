.class public final Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationsStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public newState:Lcom/navdy/service/library/events/notification/NotificationsState;

.field public service:Lcom/navdy/service/library/events/notification/ServiceType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 76
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 80
    if-nez p1, :cond_0

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 82
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;-><init>(Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;

    move-result-object v0

    return-object v0
.end method

.method public newState(Lcom/navdy/service/library/events/notification/NotificationsState;)Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;
    .locals 0
    .param p1, "newState"    # Lcom/navdy/service/library/events/notification/NotificationsState;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 96
    return-object p0
.end method

.method public service(Lcom/navdy/service/library/events/notification/ServiceType;)Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;
    .locals 0
    .param p1, "service"    # Lcom/navdy/service/library/events/notification/ServiceType;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 104
    return-object p0
.end method
