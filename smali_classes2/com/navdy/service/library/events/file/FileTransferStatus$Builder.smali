.class public final Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FileTransferStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/file/FileTransferStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/file/FileTransferStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public chunkIndex:Ljava/lang/Integer;

.field public error:Lcom/navdy/service/library/events/file/FileTransferError;

.field public success:Ljava/lang/Boolean;

.field public totalBytesTransferred:Ljava/lang/Long;

.field public transferComplete:Ljava/lang/Boolean;

.field public transferId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 112
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 116
    if-nez p1, :cond_0

    .line 123
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferId:Ljava/lang/Integer;

    .line 118
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success:Ljava/lang/Boolean;

    .line 119
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->totalBytesTransferred:Ljava/lang/Long;

    .line 120
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 121
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->chunkIndex:Ljava/lang/Integer;

    .line 122
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/file/FileTransferStatus;
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->checkRequiredFields()V

    .line 176
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferStatus;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/file/FileTransferStatus;-><init>(Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;Lcom/navdy/service/library/events/file/FileTransferStatus$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v0

    return-object v0
.end method

.method public chunkIndex(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    .locals 0
    .param p1, "chunkIndex"    # Ljava/lang/Integer;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->chunkIndex:Ljava/lang/Integer;

    .line 162
    return-object p0
.end method

.method public error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    .locals 0
    .param p1, "error"    # Lcom/navdy/service/library/events/file/FileTransferError;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 154
    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    .locals 0
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success:Ljava/lang/Boolean;

    .line 138
    return-object p0
.end method

.method public totalBytesTransferred(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    .locals 0
    .param p1, "totalBytesTransferred"    # Ljava/lang/Long;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->totalBytesTransferred:Ljava/lang/Long;

    .line 146
    return-object p0
.end method

.method public transferComplete(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    .locals 0
    .param p1, "transferComplete"    # Ljava/lang/Boolean;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete:Ljava/lang/Boolean;

    .line 170
    return-object p0
.end method

.method public transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    .locals 0
    .param p1, "transferId"    # Ljava/lang/Integer;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferId:Ljava/lang/Integer;

    .line 130
    return-object p0
.end method
