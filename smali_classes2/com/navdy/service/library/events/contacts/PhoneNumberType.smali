.class public final enum Lcom/navdy/service/library/events/contacts/PhoneNumberType;
.super Ljava/lang/Enum;
.source "PhoneNumberType.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/contacts/PhoneNumberType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/contacts/PhoneNumberType;

.field public static final enum PHONE_NUMBER_HOME:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

.field public static final enum PHONE_NUMBER_MOBILE:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

.field public static final enum PHONE_NUMBER_OTHER:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

.field public static final enum PHONE_NUMBER_WORK:Lcom/navdy/service/library/events/contacts/PhoneNumberType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    const-string v1, "PHONE_NUMBER_HOME"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/service/library/events/contacts/PhoneNumberType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_HOME:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    const-string v1, "PHONE_NUMBER_WORK"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/contacts/PhoneNumberType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_WORK:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    const-string v1, "PHONE_NUMBER_MOBILE"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/contacts/PhoneNumberType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_MOBILE:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    const-string v1, "PHONE_NUMBER_OTHER"

    invoke-direct {v0, v1, v4, v6}, Lcom/navdy/service/library/events/contacts/PhoneNumberType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_OTHER:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 7
    new-array v0, v6, [Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    sget-object v1, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_HOME:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_WORK:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_MOBILE:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->PHONE_NUMBER_OTHER:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->$VALUES:[Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput p3, p0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->value:I

    .line 18
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/PhoneNumberType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/contacts/PhoneNumberType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->$VALUES:[Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/contacts/PhoneNumberType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->value:I

    return v0
.end method
