.class public final Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationRouteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public consideredTraffic:Ljava/lang/Boolean;

.field public destination:Lcom/navdy/service/library/events/location/Coordinate;

.field public label:Ljava/lang/String;

.field public requestId:Ljava/lang/String;

.field public results:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 126
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 130
    if-nez p1, :cond_0

    .line 138
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 132
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 133
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    .line 134
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->label:Ljava/lang/String;

    .line 135
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->results:Ljava/util/List;

    .line 136
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->consideredTraffic:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->consideredTraffic:Ljava/lang/Boolean;

    .line 137
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->requestId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->requestId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    .locals 2

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->checkRequiredFields()V

    .line 199
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    move-result-object v0

    return-object v0
.end method

.method public consideredTraffic(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
    .locals 0
    .param p1, "consideredTraffic"    # Ljava/lang/Boolean;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->consideredTraffic:Ljava/lang/Boolean;

    .line 185
    return-object p0
.end method

.method public destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
    .locals 0
    .param p1, "destination"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    .line 161
    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->label:Ljava/lang/String;

    .line 169
    return-object p0
.end method

.method public requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->requestId:Ljava/lang/String;

    .line 193
    return-object p0
.end method

.method public results(Ljava/util/List;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;)",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->results:Ljava/util/List;

    .line 177
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 145
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 153
    return-object p0
.end method
