.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationSessionRouteChange.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;",
        ">;"
    }
.end annotation


# instance fields
.field public newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

.field public oldRouteId:Ljava/lang/String;

.field public reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 76
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 80
    if-nez p1, :cond_0

    .line 84
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->oldRouteId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->oldRouteId:Ljava/lang/String;

    .line 82
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 83
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;

    move-result-object v0

    return-object v0
.end method

.method public newRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;
    .locals 0
    .param p1, "newRoute"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 99
    return-object p0
.end method

.method public oldRouteId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;
    .locals 0
    .param p1, "oldRouteId"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->oldRouteId:Ljava/lang/String;

    .line 91
    return-object p0
.end method

.method public reason(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;)Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;
    .locals 0
    .param p1, "reason"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 107
    return-object p0
.end method
