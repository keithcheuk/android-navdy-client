.class public final enum Lcom/google/maps/model/AddressType;
.super Ljava/lang/Enum;
.source "AddressType.java"

# interfaces
.implements Lcom/google/maps/internal/StringJoin$UrlValue;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/maps/model/AddressType;",
        ">;",
        "Lcom/google/maps/internal/StringJoin$UrlValue;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/maps/model/AddressType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_1:Lcom/google/maps/model/AddressType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_2:Lcom/google/maps/model/AddressType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_3:Lcom/google/maps/model/AddressType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_4:Lcom/google/maps/model/AddressType;

.field public static final enum ADMINISTRATIVE_AREA_LEVEL_5:Lcom/google/maps/model/AddressType;

.field public static final enum AIRPORT:Lcom/google/maps/model/AddressType;

.field public static final enum BUS_STATION:Lcom/google/maps/model/AddressType;

.field public static final enum COLLOQUIAL_AREA:Lcom/google/maps/model/AddressType;

.field public static final enum COUNTRY:Lcom/google/maps/model/AddressType;

.field public static final enum ESTABLISHMENT:Lcom/google/maps/model/AddressType;

.field public static final enum INTERSECTION:Lcom/google/maps/model/AddressType;

.field public static final enum LOCALITY:Lcom/google/maps/model/AddressType;

.field public static final enum NATURAL_FEATURE:Lcom/google/maps/model/AddressType;

.field public static final enum NEIGHBORHOOD:Lcom/google/maps/model/AddressType;

.field public static final enum PARK:Lcom/google/maps/model/AddressType;

.field public static final enum POINT_OF_INTEREST:Lcom/google/maps/model/AddressType;

.field public static final enum POLITICAL:Lcom/google/maps/model/AddressType;

.field public static final enum POSTAL_CODE:Lcom/google/maps/model/AddressType;

.field public static final enum PREMISE:Lcom/google/maps/model/AddressType;

.field public static final enum ROUTE:Lcom/google/maps/model/AddressType;

.field public static final enum STREET_ADDRESS:Lcom/google/maps/model/AddressType;

.field public static final enum SUBLOCALITY:Lcom/google/maps/model/AddressType;

.field public static final enum SUBLOCALITY_LEVEL_1:Lcom/google/maps/model/AddressType;

.field public static final enum SUBLOCALITY_LEVEL_2:Lcom/google/maps/model/AddressType;

.field public static final enum SUBLOCALITY_LEVEL_3:Lcom/google/maps/model/AddressType;

.field public static final enum SUBLOCALITY_LEVEL_4:Lcom/google/maps/model/AddressType;

.field public static final enum SUBLOCALITY_LEVEL_5:Lcom/google/maps/model/AddressType;

.field public static final enum SUBPREMISE:Lcom/google/maps/model/AddressType;

.field public static final enum TRAIN_STATION:Lcom/google/maps/model/AddressType;

.field public static final enum TRANSIT_STATION:Lcom/google/maps/model/AddressType;

.field public static final enum UNIVERSITY:Lcom/google/maps/model/AddressType;

.field public static final enum UNKNOWN:Lcom/google/maps/model/AddressType;

.field private static log:Ljava/util/logging/Logger;


# instance fields
.field private final addressType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 33
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "STREET_ADDRESS"

    const-string v2, "street_address"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->STREET_ADDRESS:Lcom/google/maps/model/AddressType;

    .line 38
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "ROUTE"

    const-string v2, "route"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->ROUTE:Lcom/google/maps/model/AddressType;

    .line 43
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "INTERSECTION"

    const-string v2, "intersection"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->INTERSECTION:Lcom/google/maps/model/AddressType;

    .line 49
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "POLITICAL"

    const-string v2, "political"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->POLITICAL:Lcom/google/maps/model/AddressType;

    .line 55
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "COUNTRY"

    const-string v2, "country"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->COUNTRY:Lcom/google/maps/model/AddressType;

    .line 62
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_1"

    const/4 v2, 0x5

    const-string v3, "administrative_area_level_1"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_1:Lcom/google/maps/model/AddressType;

    .line 69
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_2"

    const/4 v2, 0x6

    const-string v3, "administrative_area_level_2"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_2:Lcom/google/maps/model/AddressType;

    .line 76
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_3"

    const/4 v2, 0x7

    const-string v3, "administrative_area_level_3"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_3:Lcom/google/maps/model/AddressType;

    .line 83
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_4"

    const/16 v2, 0x8

    const-string v3, "administrative_area_level_4"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_4:Lcom/google/maps/model/AddressType;

    .line 90
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "ADMINISTRATIVE_AREA_LEVEL_5"

    const/16 v2, 0x9

    const-string v3, "administrative_area_level_5"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_5:Lcom/google/maps/model/AddressType;

    .line 95
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "COLLOQUIAL_AREA"

    const/16 v2, 0xa

    const-string v3, "colloquial_area"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->COLLOQUIAL_AREA:Lcom/google/maps/model/AddressType;

    .line 100
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "LOCALITY"

    const/16 v2, 0xb

    const-string v3, "locality"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->LOCALITY:Lcom/google/maps/model/AddressType;

    .line 107
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "SUBLOCALITY"

    const/16 v2, 0xc

    const-string v3, "sublocality"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY:Lcom/google/maps/model/AddressType;

    .line 108
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "SUBLOCALITY_LEVEL_1"

    const/16 v2, 0xd

    const-string v3, "sublocality_level_1"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_1:Lcom/google/maps/model/AddressType;

    .line 109
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "SUBLOCALITY_LEVEL_2"

    const/16 v2, 0xe

    const-string v3, "sublocality_level_2"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_2:Lcom/google/maps/model/AddressType;

    .line 110
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "SUBLOCALITY_LEVEL_3"

    const/16 v2, 0xf

    const-string v3, "sublocality_level_3"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_3:Lcom/google/maps/model/AddressType;

    .line 111
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "SUBLOCALITY_LEVEL_4"

    const/16 v2, 0x10

    const-string v3, "sublocality_level_4"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_4:Lcom/google/maps/model/AddressType;

    .line 112
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "SUBLOCALITY_LEVEL_5"

    const/16 v2, 0x11

    const-string v3, "sublocality_level_5"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_5:Lcom/google/maps/model/AddressType;

    .line 117
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "NEIGHBORHOOD"

    const/16 v2, 0x12

    const-string v3, "neighborhood"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->NEIGHBORHOOD:Lcom/google/maps/model/AddressType;

    .line 123
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "PREMISE"

    const/16 v2, 0x13

    const-string v3, "premise"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->PREMISE:Lcom/google/maps/model/AddressType;

    .line 129
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "SUBPREMISE"

    const/16 v2, 0x14

    const-string v3, "subpremise"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->SUBPREMISE:Lcom/google/maps/model/AddressType;

    .line 135
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "POSTAL_CODE"

    const/16 v2, 0x15

    const-string v3, "postal_code"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->POSTAL_CODE:Lcom/google/maps/model/AddressType;

    .line 140
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "NATURAL_FEATURE"

    const/16 v2, 0x16

    const-string v3, "natural_feature"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->NATURAL_FEATURE:Lcom/google/maps/model/AddressType;

    .line 145
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "AIRPORT"

    const/16 v2, 0x17

    const-string v3, "airport"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->AIRPORT:Lcom/google/maps/model/AddressType;

    .line 151
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "UNIVERSITY"

    const/16 v2, 0x18

    const-string v3, "university"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->UNIVERSITY:Lcom/google/maps/model/AddressType;

    .line 156
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "PARK"

    const/16 v2, 0x19

    const-string v3, "park"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->PARK:Lcom/google/maps/model/AddressType;

    .line 163
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "POINT_OF_INTEREST"

    const/16 v2, 0x1a

    const-string v3, "point_of_interest"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->POINT_OF_INTEREST:Lcom/google/maps/model/AddressType;

    .line 168
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "ESTABLISHMENT"

    const/16 v2, 0x1b

    const-string v3, "establishment"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->ESTABLISHMENT:Lcom/google/maps/model/AddressType;

    .line 173
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "BUS_STATION"

    const/16 v2, 0x1c

    const-string v3, "bus_station"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->BUS_STATION:Lcom/google/maps/model/AddressType;

    .line 178
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "TRAIN_STATION"

    const/16 v2, 0x1d

    const-string v3, "train_station"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->TRAIN_STATION:Lcom/google/maps/model/AddressType;

    .line 183
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "TRANSIT_STATION"

    const/16 v2, 0x1e

    const-string v3, "transit_station"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->TRANSIT_STATION:Lcom/google/maps/model/AddressType;

    .line 189
    new-instance v0, Lcom/google/maps/model/AddressType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x1f

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/maps/model/AddressType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/maps/model/AddressType;->UNKNOWN:Lcom/google/maps/model/AddressType;

    .line 28
    const/16 v0, 0x20

    new-array v0, v0, [Lcom/google/maps/model/AddressType;

    sget-object v1, Lcom/google/maps/model/AddressType;->STREET_ADDRESS:Lcom/google/maps/model/AddressType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/maps/model/AddressType;->ROUTE:Lcom/google/maps/model/AddressType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/maps/model/AddressType;->INTERSECTION:Lcom/google/maps/model/AddressType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/maps/model/AddressType;->POLITICAL:Lcom/google/maps/model/AddressType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/maps/model/AddressType;->COUNTRY:Lcom/google/maps/model/AddressType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_1:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_2:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_3:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_4:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_5:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/maps/model/AddressType;->COLLOQUIAL_AREA:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/maps/model/AddressType;->LOCALITY:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/maps/model/AddressType;->SUBLOCALITY:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_1:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_2:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_3:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_4:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_5:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/maps/model/AddressType;->NEIGHBORHOOD:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/maps/model/AddressType;->PREMISE:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/maps/model/AddressType;->SUBPREMISE:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/maps/model/AddressType;->POSTAL_CODE:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/maps/model/AddressType;->NATURAL_FEATURE:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/maps/model/AddressType;->AIRPORT:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/maps/model/AddressType;->UNIVERSITY:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/maps/model/AddressType;->PARK:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/maps/model/AddressType;->POINT_OF_INTEREST:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/maps/model/AddressType;->ESTABLISHMENT:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/maps/model/AddressType;->BUS_STATION:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/maps/model/AddressType;->TRAIN_STATION:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/maps/model/AddressType;->TRANSIT_STATION:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/maps/model/AddressType;->UNKNOWN:Lcom/google/maps/model/AddressType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/maps/model/AddressType;->$VALUES:[Lcom/google/maps/model/AddressType;

    .line 191
    const-class v0, Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/maps/model/AddressType;->log:Ljava/util/logging/Logger;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "addressType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 196
    iput-object p3, p0, Lcom/google/maps/model/AddressType;->addressType:Ljava/lang/String;

    .line 197
    return-void
.end method

.method public static lookup(Ljava/lang/String;)Lcom/google/maps/model/AddressType;
    .locals 3
    .param p0, "addressType"    # Ljava/lang/String;

    .prologue
    .line 213
    sget-object v0, Lcom/google/maps/model/AddressType;->STREET_ADDRESS:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    sget-object v0, Lcom/google/maps/model/AddressType;->STREET_ADDRESS:Lcom/google/maps/model/AddressType;

    .line 277
    :goto_0
    return-object v0

    .line 215
    :cond_0
    sget-object v0, Lcom/google/maps/model/AddressType;->ROUTE:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    sget-object v0, Lcom/google/maps/model/AddressType;->ROUTE:Lcom/google/maps/model/AddressType;

    goto :goto_0

    .line 217
    :cond_1
    sget-object v0, Lcom/google/maps/model/AddressType;->INTERSECTION:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    sget-object v0, Lcom/google/maps/model/AddressType;->INTERSECTION:Lcom/google/maps/model/AddressType;

    goto :goto_0

    .line 219
    :cond_2
    sget-object v0, Lcom/google/maps/model/AddressType;->POLITICAL:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220
    sget-object v0, Lcom/google/maps/model/AddressType;->POLITICAL:Lcom/google/maps/model/AddressType;

    goto :goto_0

    .line 221
    :cond_3
    sget-object v0, Lcom/google/maps/model/AddressType;->COUNTRY:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 222
    sget-object v0, Lcom/google/maps/model/AddressType;->COUNTRY:Lcom/google/maps/model/AddressType;

    goto :goto_0

    .line 223
    :cond_4
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_1:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 224
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_1:Lcom/google/maps/model/AddressType;

    goto :goto_0

    .line 225
    :cond_5
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_2:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 226
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_2:Lcom/google/maps/model/AddressType;

    goto :goto_0

    .line 227
    :cond_6
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_3:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 228
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_3:Lcom/google/maps/model/AddressType;

    goto :goto_0

    .line 229
    :cond_7
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_4:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 230
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_4:Lcom/google/maps/model/AddressType;

    goto :goto_0

    .line 231
    :cond_8
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_5:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 232
    sget-object v0, Lcom/google/maps/model/AddressType;->ADMINISTRATIVE_AREA_LEVEL_5:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 233
    :cond_9
    sget-object v0, Lcom/google/maps/model/AddressType;->COLLOQUIAL_AREA:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 234
    sget-object v0, Lcom/google/maps/model/AddressType;->COLLOQUIAL_AREA:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 235
    :cond_a
    sget-object v0, Lcom/google/maps/model/AddressType;->LOCALITY:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 236
    sget-object v0, Lcom/google/maps/model/AddressType;->LOCALITY:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 237
    :cond_b
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 238
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 239
    :cond_c
    sget-object v0, Lcom/google/maps/model/AddressType;->NEIGHBORHOOD:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 240
    sget-object v0, Lcom/google/maps/model/AddressType;->NEIGHBORHOOD:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 241
    :cond_d
    sget-object v0, Lcom/google/maps/model/AddressType;->PREMISE:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 242
    sget-object v0, Lcom/google/maps/model/AddressType;->PREMISE:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 243
    :cond_e
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBPREMISE:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 244
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBPREMISE:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 245
    :cond_f
    sget-object v0, Lcom/google/maps/model/AddressType;->POSTAL_CODE:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 246
    sget-object v0, Lcom/google/maps/model/AddressType;->POSTAL_CODE:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 247
    :cond_10
    sget-object v0, Lcom/google/maps/model/AddressType;->NATURAL_FEATURE:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 248
    sget-object v0, Lcom/google/maps/model/AddressType;->NATURAL_FEATURE:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 249
    :cond_11
    sget-object v0, Lcom/google/maps/model/AddressType;->AIRPORT:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 250
    sget-object v0, Lcom/google/maps/model/AddressType;->AIRPORT:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 251
    :cond_12
    sget-object v0, Lcom/google/maps/model/AddressType;->UNIVERSITY:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 252
    sget-object v0, Lcom/google/maps/model/AddressType;->UNIVERSITY:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 253
    :cond_13
    sget-object v0, Lcom/google/maps/model/AddressType;->PARK:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 254
    sget-object v0, Lcom/google/maps/model/AddressType;->PARK:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 255
    :cond_14
    sget-object v0, Lcom/google/maps/model/AddressType;->POINT_OF_INTEREST:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 256
    sget-object v0, Lcom/google/maps/model/AddressType;->POINT_OF_INTEREST:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 257
    :cond_15
    sget-object v0, Lcom/google/maps/model/AddressType;->ESTABLISHMENT:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 258
    sget-object v0, Lcom/google/maps/model/AddressType;->ESTABLISHMENT:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 259
    :cond_16
    sget-object v0, Lcom/google/maps/model/AddressType;->BUS_STATION:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 260
    sget-object v0, Lcom/google/maps/model/AddressType;->BUS_STATION:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 261
    :cond_17
    sget-object v0, Lcom/google/maps/model/AddressType;->TRAIN_STATION:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 262
    sget-object v0, Lcom/google/maps/model/AddressType;->TRAIN_STATION:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 263
    :cond_18
    sget-object v0, Lcom/google/maps/model/AddressType;->TRANSIT_STATION:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 264
    sget-object v0, Lcom/google/maps/model/AddressType;->TRANSIT_STATION:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 265
    :cond_19
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_1:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 266
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_1:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 267
    :cond_1a
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_2:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 268
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_2:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 269
    :cond_1b
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_3:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 270
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_3:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 271
    :cond_1c
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_4:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 272
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_4:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 273
    :cond_1d
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_5:Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, Lcom/google/maps/model/AddressType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 274
    sget-object v0, Lcom/google/maps/model/AddressType;->SUBLOCALITY_LEVEL_5:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0

    .line 276
    :cond_1e
    sget-object v0, Lcom/google/maps/model/AddressType;->log:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Unknown address type \'%s\'"

    invoke-virtual {v0, v1, v2, p0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    .line 277
    sget-object v0, Lcom/google/maps/model/AddressType;->UNKNOWN:Lcom/google/maps/model/AddressType;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/maps/model/AddressType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lcom/google/maps/model/AddressType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/maps/model/AddressType;

    return-object v0
.end method

.method public static values()[Lcom/google/maps/model/AddressType;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/maps/model/AddressType;->$VALUES:[Lcom/google/maps/model/AddressType;

    invoke-virtual {v0}, [Lcom/google/maps/model/AddressType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/model/AddressType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/maps/model/AddressType;->addressType:Ljava/lang/String;

    return-object v0
.end method

.method public toUrlValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 206
    sget-object v0, Lcom/google/maps/model/AddressType;->UNKNOWN:Lcom/google/maps/model/AddressType;

    if-ne p0, v0, :cond_0

    .line 207
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Shouldn\'t use AddressType.UNKNOWN in a request."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/maps/model/AddressType;->addressType:Ljava/lang/String;

    return-object v0
.end method
